<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Session;

Route::group(['middleware' => ['web','global'], 'prefix' => ''], function()
{
    
    Route::GET('/', 'HomeController@index');
    Route::GET('pmo/cat_project/dt', 'Pmo\CatProjectController@dt');
    Route::POST('pmo/cat_project/insert', 'Pmo\CatProjectController@insert');
    Route::POST('pmo/cat_project/edit', 'Pmo\CatProjectController@edit');
    Route::POST('pmo/cat_project/update', 'Pmo\CatProjectController@update');
    Route::DELETE('pmo/cat_project/delete', 'Pmo\CatProjectController@delete');
    Route::resource('pmo/cat_project', 'Pmo\CatProjectController');


    // Okupansi 
    Route::GET('ga/okupansi/dt', 'Ga\OkupansiController@dt');
    Route::POST('ga/okupansi/insert', 'Ga\OkupansiController@insert');
    Route::POST('ga/okupansi/edit', 'Ga\OkupansiController@edit');
    Route::POST('ga/okupansi/update', 'Ga\OkupansiController@update');
    Route::DELETE('ga/okupansi/delete', 'Ga\OkupansiController@delete');
    Route::resource('ga/okupansi', 'Ga\OkupansiController');
    Route::GET('soltek', 'soltek\soltekController@index');
    Route::GET('soltek/dt', 'soltek\soltekController@dt');

    Route::GET('ic/dat_expert/dt', 'inkubasi\inkubasiController@dt');
    Route::POST('ic/dat_expert/insert', 'inkubasi\inkubasiController@insert');
    Route::POST('ic/dat_expert/edit', 'inkubasi\inkubasiController@edit');
    Route::POST('ic/dat_expert/update', 'inkubasi\inkubasiController@update');
    Route::DELETE('ic/dat_expert/delete', 'inkubasi\inkubasiController@delete');
    Route::resource('ic/dat_expert', 'inkubasi\inkubasiController');

    Route::GET('km', 'Km\DataKmController@index');
    Route::GET('km/{year}/{tw}', 'Km\DataKmController@index');

    Route::GET('data/hki', 'Data\HkiController@index');
    Route::GET('data/hki/dt', 'Data\HkiController@dt');
    Route::GET('data/inkubasi/dt', 'Data\InkubasiController@dt');
    Route::GET('data/inkubasi', 'Data\InkubasiController@index');

    Route::GET('data/inovasi/dt', 'Data\InovasiController@dt');
    Route::GET('data/inovasi', 'Data\InovasiController@index');

    Route::GET('data/okupansi/dt', 'Data\OkupansiController@dt');
    Route::GET('data/okupansi', 'Data\OkupansiController@index');
    Route::GET('data/segmen/dt', 'Data\SegmenController@dt');
    Route::GET('data/segmen', 'Data\SegmenController@index');
    Route::GET('data/project','Data\ProjectController@index');
    Route::GET('data/project/dt', 'Data\ProjectController@dt');
    Route::GET('data/docs_legal','Data\DocumentLegalController@index');
    Route::GET('data/docs_legal/dt','Data\DocumentLegalController@dt');

});

Route::group(['middleware' => ['web'], 'prefix' => ''], function()
{
    Route::POST('login/exe', 'LoginController@exe');
    Route::GET('login/logout', 'LoginController@logout');

    Route::resource('login', 'LoginController');

});

//Inkubasi Ramdan
    Route::GET('ic/inkubasi/data' , 'inkubasi\inkubasiCOntroller@show');
    Route::GET('ic/inkubasi/insert' , 'inkubasi\inkubasiCOntroller@create');
    Route::GET('ic/inkubasi/detail' , 'inkubasi\inkubasiCOntroller@detail');
    
    //expert 
    Route::get('em/expert/index', 'Em\ExpertController@index' );
    Route::get('em/expert/insert', 'Em\ExpertController@create' );
    Route::get('em/expert/detail', 'Em\ExpertController@show' );
    Route::resource('em/expert', 'Em\ExpertController');

    //form inkubas
    Route::GET('ic/startup/form/identity/{id}', 'Ic\FormStartupController@identity');
    Route::POST('ic/startup/form/identity_update', 'Ic\FormStartupController@identityUpdate');
    
    //CRUD Ramdan
    Route::GET('ic/startup/dt' , 'Ic\StartupController@dt');
    //Route::POST('ic/inkubasi/insert', 'Ic\StartupController@insert');
    //Route::POST('ic/inkubasi/edit', 'Ic\StartupController@edit');
    //Route::POST('ic/inkubasi/update', 'Ic\StartupController@update');
    //Route::DELETE('ic/inkubasi/delete', 'Ic\StartupController@delete');
    Route::resource('ic/startup', 'Ic\StartupController');


    // Route::GET('ic/inkubasi/{page}', 'Ic\StartupController@index');

    Route::get('em/client/index', 'Em\ClientController@index' );
    Route::GET('em/client/dt', 'Em\ClientController@dt');
    Route::POST('em/client/insert', 'Em\ClientController@insert');
    Route::POST('em/client/edit', 'Em\ClientController@edit');
    Route::POST('em/client/update', 'Em\ClientController@update');
    Route::DELETE('em/client/delete', 'Em\ClientController@delete');
    Route::resource('em/client', 'Em\ClientController');

    Route::get('em/problem_pasar/index', 'Em\ProblemPasarController@index' );
    Route::GET('em/problem_pasar/dt', 'Em\ProblemPasarController@dt');
    Route::POST('em/problem_pasar/insert', 'Em\ProblemPasarController@insert');
    Route::POST('em/problem_pasar/edit', 'Em\ProblemPasarController@edit');
    Route::POST('em/problem_pasar/update', 'Em\ProblemPasarController@update');
    Route::DELETE('em/problem_pasar/delete', 'Em\ProblemPasarController@delete');
    Route::resource('em/problem_pasar', 'Em\ProblemPasarController');


    Route::GET('hki/', 'Hki\DataHkiController@index');
   
    Route::GET('hki/dt', 'Hki\DataHkiController@dt');
    Route::GET('hki/dtmember', 'Hki\DataHkiController@dtmember');
    Route::POST('hki/insertmember', 'Hki\DataHkiController@insertmember');
    Route::POST('hki/edit', 'Hki\DataHkiController@edit');
    Route::POST('hki/editmember', 'Hki\DataHkiController@editmember');
    Route::POST('hki/update', 'Hki\DataHkiController@update');
    
    Route::POST('sso/login', 'Login\IgraciasLogin@PostLogin');
    Route::resource('sso', 'Login\IgraciasLogin');
