@extends('tpl.limitless.master_layout4')

@section('css_section')

@endsection

@section('page_title')
Form Data Kekayaan Intelektual (KI)
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <form id="frm" class="form-validate">
            <input type="hidden" name="id" id="id">
            {{csrf_field()}}
            <div class="modal-body">
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Tahun KI</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="inp[ki_year]" value="{{ $data->ki_year }}" id="ki_year" data-rule-required="true">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Jenis KI</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="inp[ki_type]" value="{{ $data->ki_type }}" id="ki_type" data-rule-required="true">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Jenis Ciptaan</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="inp[ki_status]" value="{{ $data->ki_status }}" id="ki_status" data-rule-required="true">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Judul Ciptaan</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="inp[ki_title]" value="{{ $data->ki_title }}" id="ki_title" data-rule-required="true">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Uraian Ciptaan</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="inp[ki_desc]" value="{{ $data->ki_desc }}" id="ki_desc" data-rule-required="true">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Tanggal Pertama Kali Diumumkan</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="inp[ki_published_date]" value="{{ $data->ki_published_date }}" id="ki_published_date" data-rule-required="true">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Negara Pertama Kali Diumumkan</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="inp[ki_published_country]" value="{{ $data->ki_published_country }}" id="ki_published_country" data-rule-required="true">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Kota Pertama Kali Diumumkan</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="inp[ki_person_city]" value="{{ $data->ki_person_city }}" id="ki_person_city" data-rule-required="true">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Lampiran Contoh Ciptaan</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="inp[ki_file_attachment]" value="{{ $data->ki_file_attachment }}" id="ki_file_attachment" data-rule-required="true">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Lampiran Surat Pernyataan</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="inp[ki_file_statement]" value="{{ $data->ki_file_statement }}" id="ki_file_statement" data-rule-required="true">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Lampiran Surat Pengalihan Hak</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="inp[ki_file_transfer]" value="{{ $data->ki_file_transfer }}" id="ki_file_transfer" data-rule-required="true">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Lampiran KTP Pengusul KI dan Seluruh Anggota Pengusul KI</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="inp[ki_file_ktp]" value="{{ $data->ki_file_ktp }}" id="ki_file_ktp" data-rule-required="true">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Pembiayaan Pendaftaran Hak Cipta</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="inp[ki_finance]" value="{{ $data->ki_finance }}" id="ki_finance" data-rule-required="true">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Pembiayaan Pendaftaran Hak Cipta (lain lain)</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="inp[ki_finance_other]" value="{{ $data->ki_finance_other }}" id="ki_finance_other" data-rule-required="true">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Tanggal Submit Hak Cipta I-Gracias</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="inp[ki_submit_date]" value="{{ $data->ki_submit_date }}" id="ki_submit_date" data-rule-required="true">
                    </div>
                </div>
            </div>
            <div class="modal-footer bg-grey-100">
                <button type="button" class="btn btn-danger btn-labeled btn-labeled-left btn-xs pull-left" data-dismiss="modal">
                    <b><i class="icon-switch"></i></b> Cancel
                </button>
                <button type="button" class="btn btn-success btn-labeled btn-labeled-left btn-xs" id="act-save" onclick="save('insert')">
                    <b><i class="icon-floppy-disk"></i></b> Simpan
                </button>
                <button type="button" class="btn btn-success btn-labeled btn-labeled-left btn-xs" id="act-update" onclick="save('update')">
                    <b><i class="icon-floppy-disk"></i></b> Update
                </button>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="frmbox" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger-700" style="background-image:url({{url('/images/backgrounds/bg.png')}})">
                <h5 class="modal-title"><i class="fa fa-navicon"></i> &nbsp;Form</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
    <div class="modal fade" id="frmboxmember" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-danger-700" style="background-image:url({{url('/images/backgrounds/bg.png')}})">
                    <h5 class="modal-title"><i class="fa fa-navicon"></i> &nbsp;Form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <form id="frmmember" class="form-validate">
                    <input type="hidden" name="id" id="id">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-form-label col-lg-3">Status</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="inp_member[kim_status]" id="kim_status" data-rule-required="true">
                            </div>
                            <label class="col-form-label col-lg-3">Nama</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="inp_member[kim_person]" id="kim_person" data-rule-required="true">
                            </div>
                            <label class="col-form-label col-lg-3">Prodi</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="inp_member[kim_prodi]" id="kim_prodi" data-rule-required="true">
                            </div>
                            <label class="col-form-label col-lg-3">Fakultas</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="inp_member[kim_faculty]" id="kim_faculty" data-rule-required="true">
                            </div>
                            <label class="col-form-label col-lg-3">No. Telp</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="inp_member[kim_notelp]" id="kim_notelp" data-rule-required="true">
                            </div>
                            <label class="col-form-label col-lg-3">E-mail</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="inp_member[kim_email]" id="kim_email" data-rule-required="true">
                            </div>
                            <label class="col-form-label col-lg-3">Alamat</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="inp_member[kim_address]" id="kim_address" data-rule-required="true">
                            </div>
                            <label class="col-form-label col-lg-3">Kode Pos</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="inp_member[kim_kodepos]" id="kim_kodepos" data-rule-required="true">
                            </div>
                         
                        </div>
                    </div>
                    <div class="modal-footer bg-grey-100">
                        <button type="button" class="btn btn-danger btn-labeled btn-labeled-left btn-xs pull-left" data-dismiss="modal">
                            <b><i class="icon-switch"></i></b> Cancel
                        </button>
                        <button type="button" class="btn btn-success btn-labeled btn-labeled-left btn-xs" id="act-save" onclick="savemember('insertmember')">
                            <b><i class="icon-floppy-disk"></i></b> Simpan
                        </button>
                        <button type="button" class="btn btn-success btn-labeled btn-labeled-left btn-xs" id="act-update" onclick="save('updatemember')">
                            <b><i class="icon-floppy-disk"></i></b> Update
                        </button>
                    </div>
                </form>
                </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
</div>
@endsection

@section('js_section')
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/interactions.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/widgets.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/effects.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/mousewheel.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/globalize/globalize.js')}}"></script>
<script>
    $(function(){
        dTable =  $('#table').DataTable({
            ajax: {
                url: '{{ url("hki/dt") }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: [
                { data: 'ki_year', name: 'ki_year', orderable: true },
                { data: 'ki_type', name: 'ki_type', orderable: true},
                { data: 'ki_status', name: 'ki_status', orderable: true},
                { data: 'ki_title', name: 'ki_title', orderable: true},
                { data: 'ki_desc', name: 'ki_desc', orderable: true},
                { data: 'ki_published_date', name: 'ki_published_date', orderable: true},
                { data: 'ki_published_country', name: 'ki_published_country', orderable: true},
                { data: 'ki_person_city', name: 'ki_person_city', orderable: true},
                { data: 'ki_file_attachment', name: 'ki_file_attachment', orderable: true},
                { data: 'ki_file_statement', name: 'ki_file_statement', orderable: true},
                { data: 'ki_file_transfer', name: 'ki_file_transfer', orderable: true},
                { data: 'ki_file_ktp', name: 'ki_file_ktp', orderable: true},
                { data: 'ki_finance', name: 'ki_finance', orderable: true},
                { data: 'ki_finance_other', name: 'ki_finance_other', orderable: true},
                { data: 'ki_submit_date', name: 'ki_submit_date', orderable: true},
                { data: 'kim_person', name: 'kim_person', orderable: true},
                { data: 'kim_person', name: 'kim_person', orderable: true},
                { data: 'action', name: 'action', orderable: false, searchable: false, className: 'center'}
            ],
            "order": [['0', 'asc']]
        });

        $('.select2').select2();

        $('.datepicker').datepicker({
            dateFormat : 'yy-mm-dd'
        });

        $('.form-check-input-styled').uniform();

        $('.dataTables_filter input[type=search]').attr('placeholder','Pencarian');

        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });

       // $('.dt-buttons').append('<button class="btn btn-light bg-white" onclick="add()"><i class="icon-add"></i> <span class="d-none d-lg-inline-block ml-2">Tambah Data</span></button>');
    });

    $(function(){
        dTable =  $('#table_member').DataTable({
            ajax: {
                url: '{{ url("hki/dtmember") }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: [
                { data: 'kim_status', name: 'kim_status', orderable: true },
                { data: 'kim_person', name: 'ki_type', orderable: true,searchable:true},
                { data: 'kim_prodi', name: 'kim_prodi', orderable: true},
                { data: 'kim_faculty', name: 'kim_faculty', orderable: true},
                { data: 'kim_kk', name: 'kim_kk', orderable: true},
                { data: 'kim_notelp', name: 'kim_notelp', orderable: true},
                { data: 'kim_email', name: 'kim_email', orderable: true},
                { data: 'kim_address', name: 'kim_address', orderable: true},
                { data: 'action', name: 'action', orderable: false, searchable: false, className: 'center'}
            ],
            "order": [['0', 'asc']]
        });

        $('.select2').select2();

        $('.datepicker').datepicker({
            dateFormat : 'yy-mm-dd'
        });

        $('.form-check-input-styled').uniform();

        $('.dataTables_filter input[type=search]').attr('placeholder','Pencarian');

        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });

        $('.dt-buttons').append('<button class="btn btn-light bg-white" onclick="addmember()"><i class="icon-add"></i> <span class="d-none d-lg-inline-block ml-2">Tambah Data</span></button>');
    });

    function add()
    {
        _reset();
        $('.form-check-input-styled').prop('checked',false).uniform('refresh');
        $('#act-save').show();
        $('#act-update').hide();
        $('#frmbox').modal({keyboard: false, backdrop: 'static'});
    }

    function addmember()
    {
        _reset();
        $('.form-check-input-styled').prop('checked',false).uniform('refresh');
        $('#act-save').show();
        $('#act-update').hide();
        $('#frmboxmember').modal({keyboard: false, backdrop: 'static'});
    }

    function save(url)
    {
        if($("#frm").valid())
        {
            var formData = new FormData($('#frm')[0]);

            $.ajax({
                url:'{{ url("hki") }}'+'/'+url,
                type:'post',
                data: formData,
                contentType: false,//untuk upload image
                processData: false,//untuk upload image
                timeout: 300000, // sets timeout to 3 seconds
                dataType:'json',
                success : function(e) {
                    if(e.status == 'ok;') {
                        new Noty({
                            text: 'Berhasil Menyimpan Data',
                            type: 'success'
                        }).show();

                        dTable.draw();
                        $("#frmbox").modal('hide');
                    } else {
                        alert(e.text);
                    }
                }
            });
        }
    }

    function savemember(url)
    {
        if($("#frmmember").valid())
        {
            var formData = new FormData($('#frmmember')[0]);

            $.ajax({
                url:'{{ url("hki") }}'+'/'+url,
                type:'post',
                data: formData,
                contentType: false,//untuk upload image
                processData: false,//untuk upload image
                timeout: 300000, // sets timeout to 3 seconds
                dataType:'json',
                success : function(e) {
                    if(e.status == 'ok;') {
                        new Noty({
                            text: 'Berhasil Menyimpan Data',
                            type: 'success'
                        }).show();

                        dTable.draw();
                        $("#frmmember").modal('hide');
                    } else {
                        alert(e.text);
                    }
                }
            });
        }
    }

    function edit(id)
    {
        $.ajax({
            url:'{{ url("hki/edit") }}',
            type:'post',
            dataType:'json',
            data: ({ id : id }),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(e) {
                _reset();
                $('#act-save').hide();
                $('#act-update').show();
                $('#id').val(id);
                $.each(e, function(key, value) {
                    if($('#'+key).hasClass("select2")) {
                        $('#'+key).val(value).trigger('change');
                    }
                    else if($('input[type=radio]').hasClass(key)){
                        if(value!=""){
                            $("input[name='inp["+key+"]'][value='" + value + "']").prop('checked', true);
                            $.uniform.update();
                        }
                    }
                    else if($('input[type=checkbox]').hasClass(key)){
                        if(value!=null){
                            var temp = value.split('; ');
                            for (var i = 0; i < temp.length; i++) {
                                $("input[name='inp["+key+"][]'][value='" + temp[i] + "']").prop('checked', true);
                            }
                            $.uniform.update();
                        }
                    }
                    else  $('#'+key).val(value);
                });
                $('#frmbox').modal({keyboard: false, backdrop: 'static'});
            }
        });
    }

    function editmember(id)
    {
        $.ajax({
            url:'{{ url("hki/editmember") }}',
            type:'post',
            dataType:'json',
            data: ({ id : id }),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(e) {
                _reset();
                $('#act-save').hide();
                $('#act-update').show();
                $('#id').val(id);
                $.each(e, function(key, value) {
                    if($('#'+key).hasClass("select2")) {
                        $('#'+key).val(value).trigger('change');
                    }
                    else if($('input[type=radio]').hasClass(key)){
                        if(value!=""){
                            $("input[name='inp["+key+"]'][value='" + value + "']").prop('checked', true);
                            $.uniform.update();
                        }
                    }
                    else if($('input[type=checkbox]').hasClass(key)){
                        if(value!=null){
                            var temp = value.split('; ');
                            for (var i = 0; i < temp.length; i++) {
                                $("input[name='inp["+key+"][]'][value='" + temp[i] + "']").prop('checked', true);
                            }
                            $.uniform.update();
                        }
                    }
                    else  $('#'+key).val(value);
                });
                $('#frmboxmember').modal({keyboard: false, backdrop: 'static'});
            }
        });
    }
</script>
@endsection