@extends('errors.master_error')
@section('code', "500")
@section('text')
    something went wrong. Brace yourself till we get the error fixed.<br>(Internal Server Error)
@endsection