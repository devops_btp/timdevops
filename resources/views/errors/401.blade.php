@extends('errors.master_error')
@section('code', "401")
@section('text')
    looks like you are not allowed to access this page.<br>(Unauthorized)
@endsection