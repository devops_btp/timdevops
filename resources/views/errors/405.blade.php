@extends('errors.master_error')
@section('code', "405")
@section('text')
    this url can only handle the following request methods.<br>(Method Not Allowed)
@endsection