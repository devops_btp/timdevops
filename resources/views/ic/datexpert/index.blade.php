@extends('tpl.limitless.master_layout4')

@section('css_section')

@endsection

@section('page_title')
Master Data Expert
@endsection

@section('content')
<div class="card">
    <table class="table table-striped" id="table">
        <thead>
        <tr>
            <th>Tahun</th>
            <th>Product</th>
            <th>Team</th>
            <th>Deskripsi</th>
            <th>Tim Fakultas</th>
            <th>Status</th>
            <th>Kategori</th>
            <th>Founding</th>
            <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>

<!-- <div class="modal fade" id="frmbox" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger-700" style="background-image:url({{url('/images/backgrounds/bg.png')}})">
                <h5 class="modal-title"><i class="fa fa-navicon"></i> &nbsp;Form</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form id="frm" class="form-validate">
                <input type="hidden" name="id" id="id">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Tahun</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="inp[ri_year]" id="ri_year" data-rule-required="true">
                        </div>
                        <label class="col-form-label col-lg-3">Product</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="inp[ri_product]" id="ri_product" data-rule-required="true">
                        </div>
                        <label class="col-form-label col-lg-3">Team</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="inp[ri_team]" id="ri_team" data-rule-required="true">
                        </div>
                        <label class="col-form-label col-lg-3">Deskripsi</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="inp[ri_desc]" id="ri_desc" data-rule-required="true">
                        </div>
                        <label class="col-form-label col-lg-3">Tim Fakultas</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="inp[ri_team_faculty]" id="ri_team_faculty" data-rule-required="true">
                        </div>
                        <label class="col-form-label col-lg-3">Status</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="inp[ri_status]" id="ri_status" data-rule-required="true">
                        </div>
                        <label class="col-form-label col-lg-3">Kategori</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="inp[ri_category]" id="ri_category" data-rule-required="true">
                        </div>
                        <label class="col-form-label col-lg-3">Founding</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="inp[ri_founding]" id="ri_founding" data-rule-required="true">
                        </div>
                    </div>
                </div>
                <div class="modal-footer bg-grey-100">
                    <button type="button" class="btn btn-danger btn-labeled btn-labeled-left btn-xs pull-left" data-dismiss="modal">
                        <b><i class="icon-switch"></i></b> Cancel
                    </button>
                    <button type="button" class="btn btn-success btn-labeled btn-labeled-left btn-xs" id="act-save" onclick="save('insert')">
                        <b><i class="icon-floppy-disk"></i></b> Simpan
                    </button>
                    <button type="button" class="btn btn-success btn-labeled btn-labeled-left btn-xs" id="act-update" onclick="save('update')">
                        <b><i class="icon-floppy-disk"></i></b> Update
                    </button>
                </div>
            </form> -->
        <!-- </div> /.modal-content -->
    <!-- </div>/.modal-dialog -->
<!-- </div>/.modal -->
@endsection

@section('js_section')
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/interactions.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/widgets.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/effects.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/mousewheel.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/globalize/globalize.js')}}"></script>
<script>
    $(function(){
        dTable =  $('#table').DataTable({
            ajax: {
                url: '{{ url("ic/dat_expert/dt") }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: [
                { data: 'ri_year', name: 'ri_year', orderable: true },
                { data: 'ri_product', name: 'ri_product', orderable: true},
                { data: 'ri_team', name: 'ri_team', orderable: true},
                { data: 'ri_desc', name: 'ri_desc', orderable: true},
                { data: 'ri_team_faculty', name: 'ri_team_faculty', orderable: true},
                { data: 'ri_status', name: 'ri_status', orderable: true},
                { data: 'ri_category', name: 'ri_category', orderable: true},
                { data: 'ri_founding', name: 'ri_founding', orderable: true},
                { data: 'action', name: 'action', orderable: false, searchable: false, className: 'center'}
            ],
            "order": [['0', 'asc']]
        });

        $('.select2').select2();

        $('.datepicker').datepicker({
            dateFormat : 'yy-mm-dd'
        });

        $('.form-check-input-styled').uniform();

        $('.dataTables_filter input[type=search]').attr('placeholder','Pencarian');

        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });

       // $('.dt-buttons').append('<button class="btn btn-light bg-white" onclick="add()"><i class="icon-add"></i> <span class="d-none d-lg-inline-block ml-2">Tambah Data</span></button>');
    });

    // function add()
    // {
    //     _reset();
    //     $('.form-check-input-styled').prop('checked',false).uniform('refresh');
    //     $('#act-save').show();
    //     $('#act-update').hide();
    //     $('#frmbox').modal({keyboard: false, backdrop: 'static'});
    // }

    // function save(url)
    // {
    //     if($("#frm").valid())
    //     {
    //         var formData = new FormData($('#frm')[0]);

    //         $.ajax({
    //             url:'{{ url("ic/dat_expert") }}'+'/'+url,
    //             type:'post',
    //             data: formData,
    //             contentType: false,//untuk upload image
    //             processData: false,//untuk upload image
    //             timeout: 300000, // sets timeout to 3 seconds
    //             dataType:'json',
    //             success : function(e) {
    //                 if(e.status == 'ok;') {
    //                     new Noty({
    //                         text: 'Berhasil Menyimpan Data',
    //                         type: 'success'
    //                     }).show();

    //                     dTable.draw();
    //                     $("#frmbox").modal('hide');
    //                 } else {
    //                     alert(e.text);
    //                 }
    //             }
    //         });
    //     }
    // }

    // function edit(id)
    // {
    //     $.ajax({
    //         url:'{{ url("ic/dat_expert/edit") }}',
    //         type:'post',
    //         dataType:'json',
    //         data: ({ id : id }),
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         },
    //         success: function(e) {
    //             _reset();
    //             $('#act-save').hide();
    //             $('#act-update').show();
    //             $('#id').val(id);
    //             $.each(e, function(key, value) {
    //                 if($('#'+key).hasClass("select2")) {
    //                     $('#'+key).val(value).trigger('change');
    //                 }
    //                 else if($('input[type=radio]').hasClass(key)){
    //                     if(value!=""){
    //                         $("input[name='inp["+key+"]'][value='" + value + "']").prop('checked', true);
    //                         $.uniform.update();
    //                     }
    //                 }
    //                 else if($('input[type=checkbox]').hasClass(key)){
    //                     if(value!=null){
    //                         var temp = value.split('; ');
    //                         for (var i = 0; i < temp.length; i++) {
    //                             $("input[name='inp["+key+"][]'][value='" + temp[i] + "']").prop('checked', true);
    //                         }
    //                         $.uniform.update();
    //                     }
    //                 }
    //                 else  $('#'+key).val(value);
    //             });
    //             $('#frmbox').modal({keyboard: false, backdrop: 'static'});
    //         }
    //     });
    // }

    // function del(id, txt)
    // {
    //     if(confirm('Data: '+txt+'\n'+'Apakah anda yakin akan menghapus data ini?')) {
    //     $.ajax({
    //         url:'{{ url("ic/dat_expert/delete") }}',
    //         type:'delete',
    //         dataType:'json',
    //         data: ({id : id }),
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         },
    //         success: function(e) {
    //             if(e.status == 'ok;') {
    //                 new Noty({
    //                     text: 'Berhasil Menghapus Data',
    //                     type: 'success'
    //                 }).show();

    //                 dTable.draw();
    //             } else {
    //                 alert(e.text);
    //             }
    //         }
    //     });
    // }
    // }
</script>
@endsection