<div class="navbar navbar-expand-lg navbar-light navbar-component rounded">
    <div class="navbar-collapse collapse" id="navbar-navigation">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="{{ url('ic/startup') }}" class="navbar-nav-link">
                    <i class="icon-arrow-left7 mr-2"></i> Kembali ke Data Startup
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="navbar-nav-link">
                    <i class="icon-brain mr-2"></i> Identitas Startup
                </a>
            </li>
            <li class="nav-item dropdown">
                <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-rocket mr-2"></i> Produk
                </a>

                <div class="dropdown-menu">
                    <a href="{{ url('ic/startup/form/product/'.$id) }}" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Data Produk</a>
                    <a href="{{ url('ic/dat_expert') }}" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Pengujian</a>
                    <a href="{{ url('ic/dat_expert') }}" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Sertifikasi Produk</a>
                    <a href="{{ url('ic/dat_expert') }}" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Izin Produk</a>
                    <a href="{{ url('ic/dat_expert') }}" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Foto & Video Produk</a>
                    <a href="{{ url('ic/dat_expert') }}" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Aspek Bisnis</a>
                </div>
            </li>
            <li class="nav-item ">
                <a href="#" class="navbar-nav-link">
                    <i class="icon-clipboard6 mr-2"></i> Tim
                </a>
            </li>
            <li class="nav-item dropdown">
                <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-stats-growth mr-2"></i> Legalitas
                </a>

                <div class="dropdown-menu">
                    <a href="#" class="dropdown-item"><i class="icon-align-center-horizontal"></i> HKI</a>
                    <a href="#" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Badan Hukum</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-cube mr-2"></i> Coaching/Mentoring
                </a>

                <div class="dropdown-menu">
                    <a href="/ga/okupansi" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Mentor/Coach</a>
                    <a href="/ga/okupansi" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Jadwal</a>
                    <a href="/ga/okupansi" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Mentoring/Coahing</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a href="#" class="navbar-nav-link">
                    <i class="icon-cube mr-2"></i> Monitoring/Evaluasi
                </a>
            </li>
        </ul>
    </div>
</div>