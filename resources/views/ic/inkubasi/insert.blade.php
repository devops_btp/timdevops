@extends('tpl.limitless.master_layout4')

@section('css_section')

@endsection

@section('page_title')
Insert Data Startup
@endsection

@section('content')

<div class="content-wrapper">
	<!-- Data Startup -->
	    <div class="card">
			<div class="card-header header-elements-inline">
				<legend class="font-weight-semibold"><i class="icon-vcard mr-2"></i>Data Startup</legend>
				<div class="header-elements">
					<div class="list-icons">
		                <a class="list-icons-item" data-action="collapse"></a>
		                <a class="list-icons-item" data-action="reload"></a>
		        		<a class="list-icons-item" data-action="remove"></a>
		            </div>
	            </div>
			</div>
			<div class="card-body">
				<form id="frm" class="form-validate">
					<input type="hidden" name="id" id="id">
                {{csrf_field()}}
					<div class="row">
						<div class="col-md-6">
							<fieldset>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Unggah Logo Startup</label>
										<div class="col-lg-9">
											<input type="file" class="file-input-custom" name="inp[ic_logo_startup]" id="ic_logo_startup" data-show-caption="true" data-show-upload="true" accept="image/*" data-fouc>
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Nama Startup</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" name="inp[ic_nama_startup]" id="ic_nama_startup">
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Alamat</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" name="inp[ic_alamat]" id="ic_alamat">
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Visi</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" name="inp[ic_visi]" id="ic_visi">
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Misi</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" name="inp[ic_misi]" id="ic_misi">
										</div>
								</div>
							</fieldset>
						</div>
						<div class="col-md-6">
							<fieldset>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Data Media Sosial(FB/IG/Twitter)</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" name="inp[ic_data_medsos]" id="ic_data_medsos">
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Website</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" name="inp[ic_website]" id="ic_website">
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Jumlah Pegawai</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="inp[ic_jml_pegawai]" id="ic_jml_pegawai">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">File Struktur Organisasi</label>
											<div class="col-lg-9">
												<input name="inp[ic_file_so]" id="ic_file_so" type="file" class="form-input-styled" data-fouc>
												<span class="form-text text-muted">Accepted formats: pdf, doc. Max file size 2Mb</span>
											</div>
								</div>
							</fieldset>
						</div>
					</div>
					<div class="text-right">
						<button type="submit" class="btn btn-primary" id="act-save" onclick="save('insert')">Submit</button>
					</div>
				</form>
			</div>
		</div>
	<!-- /Data Startup -->

	<!-- Data Produk -->
	    <div class="card" id="frmbox">
			<div class="card-header header-elements-inline">
				<legend class="font-weight-semibold"><i class="icon-cart mr-2"></i>Data Produk</legend>
				<div class="header-elements">
					<div class="list-icons">
		                <a class="list-icons-item" data-action="collapse"></a>
		                <a class="list-icons-item" data-action="reload"></a>
		        		<a class="list-icons-item" data-action="remove"></a>
		            </div>
	            </div>
			</div>
			<div class="card-body">
				<form action="#">
					<div class="row">
						<div class="col-md-6">
							<fieldset>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Nama Produk</label>
										<div class="col-lg-9">
											<input type="text" class="form-control">
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Deskripsi Produk</label>
										<div class="col-lg-9">
											<textarea class="form-control"></textarea>
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Keunikan Produk</label>
										<div class="col-lg-9">
											<textarea class="form-control"></textarea>
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Spesifikasi Produk</label>
										<div class="col-lg-9">
											<input type="text" class="form-control">
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Masalah yang ingin diselesaikan</label>
										<div class="col-lg-9">
											<textarea class="form-control" ></textarea>
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Solusi</label>
										<div class="col-lg-9">
											<textarea class="form-control"></textarea>
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Link Video Produk</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" value="http://">
										</div>
								</div>
							</fieldset>
						</div>
						<div class="col-md-6">
							<fieldset>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Logo Produk   (Bukan Logo Startup)</label>
										<div class="col-lg-9">
											<input type="file" class="file-input-custom" data-show-caption="true" data-show-upload="true" accept="image/*" data-fouc>
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Unggah foto Produk</label>
										<div class="col-lg-9">
											<input type="file" class="file-input-custom" data-show-caption="true" data-show-upload="true" accept="image/*" data-fouc>
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Unggah Marketing (Brosur/Flyer/Proposal)</label>
										<div class="col-lg-9">
											<input type="file" class="file-input-custom" data-show-caption="true" data-show-upload="true" accept="image/*" data-fouc>
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">File Struktur Organisasi</label>
											<div class="col-lg-9">
												<input name="recommendations" type="file" class="form-input-styled" data-fouc>
												<span class="form-text text-muted">Accepted formats: pdf, doc. Max file size 2Mb</span>
											</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">File Business Model Canvas</label>
										<div class="col-lg-9">
											<input name="recommendations" type="file" class="form-input-styled" data-fouc>
											<span class="form-text text-muted">Accepted formats: pdf, doc. Max file size 2Mb</span>
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">File Data HKI</label>
										<div class="col-lg-9">
											<input name="recommendations" type="file" class="form-input-styled" data-fouc>
											<span class="form-text text-muted">Accepted formats: pdf, doc. Max file size 2Mb</span>
										</div>
								</div>
							</fieldset>
						</div>
					</div>
					<div class="text-right">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
	<!-- /Data Produk -->

	<!-- Data Tim -->
	    <div class="card">
			<div class="card-header header-elements-inline">
				<legend class="font-weight-semibold"><i class="icon-users4 mr-2"></i>Data Tim</legend>
				<div class="header-elements">
					<div class="list-icons">
		                <a class="list-icons-item" data-action="collapse"></a>
		                <a class="list-icons-item" data-action="reload"></a>
		        		<a class="list-icons-item" data-action="remove"></a>
		            </div>
	            </div>
			</div>
			<div class="card-body">
				<form action="#">
					<div class="row">
						<div class="col-md-6">
							<fieldset>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Nama</label>
										<div class="col-lg-9">
											<input type="text" class="form-control">
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Tanggal Lahir</label>
									<div class="col-lg-9 input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="date" class="form-control daterange-single" value="03/18/2013">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Nomor HP/WA</label>
										<div class="col-lg-9">
											<input type="text" class="form-control">
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">NIK sesuai KTP</label>
										<div class="col-lg-9">
											<input type="text" class="form-control">
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Jenis Kelasmin:</label>
									<div class="col-lg-9">
										<select name="university-country" data-placeholder="Pilih Jenis Kelamin" class="form-control form-control-select2" data-fouc>
											<option></option> 
											<option value="1">Pria</option> 
											<option value="2">Wanita</option> 
										</select>
									</div>
									</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Email</label>
										<div class="col-lg-9">
											<input type="text" class="form-control">
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Jabatan/Peran distartup</label>
										<div class="col-lg-9">
											<input type="text" class="form-control">
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Apakah Anda berasal dari Civitas TelU</label>
									<div class="col-lg-9">
										<select name="university-country" data-placeholder="Pilih Jika berasal dari civitas TelU" class="form-control form-control-select2" data-fouc>
											<option></option> 
											<option value="1">Mahasiswa</option> 
											<option value="2">Alumni</option> 
											<option value="3">Dosen</option> 
											<option value="4">Pegawai</option> 
										</select>
									</div>
								</div>
							</fieldset>
						</div>
						<div class="col-md-6">
							<fieldset>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Unggah Foto</label>
										<div class="col-lg-9">
											<input type="file" class="file-input-custom" data-show-caption="true" data-show-upload="true" accept="image/*" data-fouc>
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Unggah KTP</label>
										<div class="col-lg-9">
											<input type="file" class="file-input-custom" data-show-caption="true" data-show-upload="true" accept="image/*" data-fouc>
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">File CV</label>
										<div class="col-lg-9">
											<input name="recommendations" type="file" class="form-input-styled" data-fouc>
											<span class="form-text text-muted">Accepted formats: pdf, doc. Max file size 2Mb</span>
										</div>
								</div>
								<div class="form-group row">

								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label"></label>
									<div class="col-lg-9">
										<button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-left"><b><i class="icon-plus3"></i></b> 
											Tambah Data Tim
										</button>
									</div>	
								</div>
								</div>
							</fieldset>
						</div>
						<div class="text-right">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	<!-- /Data Tim -->
</div>

@endsection

@section('js_section')
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/forms/wizards/steps.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/forms/inputs/inputmask.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/extensions/cookie.js')}}"></script>

<script type="text/javascript" src="{{url('/themes/limitless/global/js/demo_pages/form_wizard.js')}}"></script>

<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/uploaders/fileinput/plugins/purify.min.js"')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/uploaders/fileinput/plugins/sortable.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/uploaders/fileinput/fileinput.min.js')}}"></script>

<script type="text/javascript" src="{{url('/themes/limitless/global/js/demo_pages/uploader_bootstrap.js')}}"></script>
<script>
    $(function(){
        dTable =  $('#table').DataTable({
            ajax: {
                url: '{{ url("ic/inkubasi/dt") }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: [
                { data: 'ic_logo', name: 'ic_logo', orderable: true },
                { data: 'ic_nama_startup', name: 'ic_nama_startup', orderable: true },
                { data: 'ic_alamat', name: 'ic_alamat', orderable: true },
                { data: 'ic_visi', name: 'ic_visi', orderable: true },
                { data: 'ic_misi', name: 'ic_misi', orderable: true },
                { data: 'ic_data_medsos', name: 'ic_data_medsos', orderable: true },
                { data: 'ic_website', name: 'ic_website', orderable: true },
                { data: 'ic_jml_pegawai', name: 'ic_jml_pegawai', orderable: true },
                { data: 'ic_file_so', name: 'ic_file_so', orderable: true },
                { data: 'action', name: 'action', orderable: false, searchable: false, className: 'center'}
            ],
            "order": [['0', 'asc']]
        });

        $('.select2').select2();

        $('.datepicker').datepicker({
            dateFormat : 'yy-mm-dd'
        });

        $('.form-check-input-styled').uniform();

        $('.dataTables_filter input[type=search]').attr('placeholder','Pencarian');

        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });

        $('.dt-buttons').append('<button class="btn btn-light bg-white" onclick="add()"><i class="icon-add"></i> <span class="d-none d-lg-inline-block ml-2">Tambah Data</span></button>');
    });

    function add()
    {
        _reset();
        $('.form-check-input-styled').prop('checked',false).uniform('refresh');
        $('#act-save').show();
        $('#act-update').hide();
        $('#frmbox').modal({keyboard: false, backdrop: 'static'});
    }

    function save(url)
    {
        if($("#frm").valid())
        {
            var formData = new FormData($('#frm')[0]);

            $.ajax({
                url:'{{ url("ic/inkubasi") }}'+'/'+url,
                type:'post',
                data: formData,
                contentType: false,//untuk upload image
                processData: false,//untuk upload image
                timeout: 300000, // sets timeout to 3 seconds
                dataType:'json',
                success : function(e) {
                    if(e.status == 'ok;') {
                        new Noty({
                            text: 'Berhasil Menyimpan Data',
                            type: 'success'
                        }).show();

                        dTable.draw();
                        $("#frmbox").modal('hide');
                    } else {
                        alert(e.text);
                    }
                }
            });
        }
    }

    function edit(id)
    {
        $.ajax({
            url:'{{ url("ic/inkubasi/edit") }}',
            type:'post',
            dataType:'json',
            data: ({ id : id }),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(e) {
                _reset();
                $('#act-save').hide();
                $('#act-update').show();
                $('#id').val(id);
                $.each(e, function(key, value) {
                    if($('#'+key).hasClass("select2")) {
                        $('#'+key).val(value).trigger('change');
                    }
                    else if($('input[type=radio]').hasClass(key)){
                        if(value!=""){
                            $("input[name='inp["+key+"]'][value='" + value + "']").prop('checked', true);
                            $.uniform.update();
                        }
                    }
                    else if($('input[type=checkbox]').hasClass(key)){
                        if(value!=null){
                            var temp = value.split('; ');
                            for (var i = 0; i < temp.length; i++) {
                                $("input[name='inp["+key+"][]'][value='" + temp[i] + "']").prop('checked', true);
                            }
                            $.uniform.update();
                        }
                    }
                    else  $('#'+key).val(value);
                });
                $('#frmbox').modal({keyboard: false, backdrop: 'static'});
            }
        });
    }

    function del(id, txt)
    {
        if(confirm('Data: '+txt+'\n'+'Apakah anda yakin akan menghapus data ini?')) {
        $.ajax({
            url:'{{ url("ic/inkubasi/delete") }}',
            type:'delete',
            dataType:'json',
            data: ({id : id }),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(e) {
                if(e.status == 'ok;') {
                    new Noty({
                        text: 'Berhasil Menghapus Data',
                        type: 'success'
                    }).show();

                    dTable.draw();
                } else {
                    alert(e.text);
                }
            }
        });
    }
    }
</script>
@endsection