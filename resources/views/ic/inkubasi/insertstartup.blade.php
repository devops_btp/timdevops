@extends('tpl.limitless.master_layout4')

@section('css_section')

@endsection

@section('page_title')
Insert Data Startup
@endsection

@section('content')
<div class="navbar navbar-expand-md navbar-light">
	<div class="text-center d-md-none w-100">
		<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-navigation">
			<i class="icon-unfold mr-2"></i>
			Navigation
		</button>
	</div>

	<div class="navbar-collapse collapse" id="navbar-navigation">
		<ul class="navbar-nav">
			<li class="nav-item">
				<a href="#" class="navbar-nav-link">
					<i class="icon-brain mr-2"></i> Data Startup
				</a>
			</li>
			<li class="nav-item dropdown">
				<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
					<i class="icon-rocket mr-2"></i> Produk
				</a>

				<div class="dropdown-menu">
                    <a href="{{ url('ic/dat_expert') }}" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Data Produk</a>
                    <a href="{{ url('ic/dat_expert') }}" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Pengujian</a>
                    <a href="{{ url('ic/dat_expert') }}" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Sertifikasi Produk</a>
                    <a href="{{ url('ic/dat_expert') }}" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Izin Produk</a>
                    <a href="{{ url('ic/dat_expert') }}" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Foto & Video Produk</a>
                    <a href="{{ url('ic/dat_expert') }}" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Aspek Bisnis</a>
				</div>
			</li>
			<li class="nav-item ">
				<a href="#" class="navbar-nav-link">
					<i class="icon-clipboard6 mr-2"></i> Tim
				</a>
			</li>
			<li class="nav-item dropdown">
				<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
					<i class="icon-stats-growth mr-2"></i> Legalitas
				</a>

				<div class="dropdown-menu">
					<a href="#" class="dropdown-item"><i class="icon-align-center-horizontal"></i> HKI</a>
					<a href="#" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Badan Hukum</a>
				</div>
			</li>

			<li class="nav-item dropdown">
				<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
					<i class="icon-cube mr-2"></i> Coaching/Mentoring
				</a>

				<div class="dropdown-menu">
					<a href="/ga/okupansi" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Mentor/Coach</a>
					<a href="/ga/okupansi" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Jadwal</a>
					<a href="/ga/okupansi" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Mentoring/Coahing</a>
				</div>
			</li>

            <li class="nav-item dropdown">
				<a href="#" class="navbar-nav-link">
					<i class="icon-cube mr-2"></i> Monitoring/Evaluasi
				</a>
			</li>
		</ul>
	</div>
</div>

<p>&nbsp;</p>

<div class="content-wrapper" id="frmbox" role="dialog">
    <!-- Data Startup -->
	    <div class="card">
			<div class="card-header header-elements-inline">
				<legend class="font-weight-semibold"><i class="icon-vcard mr-2"></i>Data Startup</legend>
				<div class="header-elements">
					<div class="list-icons">
		                <a class="list-icons-item" data-action="collapse"></a>
		                <a class="list-icons-item" data-action="reload"></a>
		        		<a class="list-icons-item" data-action="remove"></a>
		            </div>
	            </div>
			</div>
			<div class="card-body">
				<form id="frm" class="form-validate" enctype="multipart/form-data">
					<input type="hidden" name="id" id="id">
                    {{csrf_field()}}
					<div class="row">
						<div class="col-md-6">
							<fieldset>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Unggah Logo Startup</label>
										<div class="col-lg-9">
											<input type="file" class="file-input-custom" name="inp[ic_logo]" id="ic_logo" data-show-caption="true" data-show-upload="true" accept="image/*" data-fouc>
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Nama Startup</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" name="inp[ic_nama_startup]" id="ic_nama_startup" value="{{ $startup[0]->ic_nama_startup }}">
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Alamat</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" name="inp[ic_alamat]" id="ic_alamat" value="{{ $startup[0]->ic_alamat }}">
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Visi</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" name="inp[ic_visi]" id="ic_visi" value="{{ $startup[0]->ic_visi }}">
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Misi</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" name="inp[ic_misi]" id="ic_misi" value="{{ $startup[0]->ic_misi }}">
										</div>
								</div>
							</fieldset>
						</div>
						<div class="col-md-6">
							<fieldset>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Data Media Sosial(FB/IG/Twitter)</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" name="inp[ic_data_medsos]" id="ic_data_medsos" value="{{ $startup[0]->ic_data_medsos }}">
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Website</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" name="inp[ic_website]" id="ic_website" value="{{ $startup[0]->ic_website }}">
										</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Jumlah Pegawai</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="inp[ic_jml_pegawai]" id="ic_jml_pegawai" value="{{ $startup[0]->ic_jml_pegawai }}">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">File Struktur Organisasi</label>
											<div class="col-lg-9">
												<input name="inp[ic_file_so]" id="ic_file_so" type="file" class="form-input-styled" data-fouc>
												<span class="form-text text-muted">Accepted formats: pdf, doc. Max file size 2Mb</span>
											</div>
								</div>
							</fieldset>
						</div>
					</div>
					<div class="text-right">
						<button type="submit" class="btn btn-primary" id="act-save" onclick="save('insert')">Submit</button>
					</div>
				</form>
			</div>
		</div>
	<!-- /Data Startup -->
</div>

@endsection

@section('js_section')
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/interactions.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/widgets.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/effects.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/mousewheel.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/globalize/globalize.js')}}"></script>

<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/forms/wizards/steps.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/forms/inputs/inputmask.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/extensions/cookie.js')}}"></script>

<script type="text/javascript" src="{{url('/themes/limitless/global/js/demo_pages/form_wizard.js')}}"></script>

<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/uploaders/fileinput/plugins/purify.min.js"')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/uploaders/fileinput/plugins/sortable.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/uploaders/fileinput/fileinput.min.js')}}"></script>

<script type="text/javascript" src="{{url('/themes/limitless/global/js/demo_pages/uploader_bootstrap.js')}}"></script>

<script>
    $(function(){
        dTable =  $('#table').DataTable({
            ajax: {
                url: '{{ url("ic/inkubasi/dt") }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: [
                { data: 'ic_logo', name: 'ic_logo', orderable: true },
                { data: 'ic_nama_startup', name: 'ic_nama_startup', orderable: true },
                { data: 'ic_alamat', name: 'ic_alamat', orderable: true },
                { data: 'ic_visi', name: 'ic_visi', orderable: true },
                { data: 'ic_misi', name: 'ic_misi', orderable: true },
                { data: 'ic_data_medsos', name: 'ic_data_medsos', orderable: true },
                { data: 'ic_website', name: 'ic_website', orderable: true },
                { data: 'ic_jml_pegawai', name: 'ic_jml_pegawai', orderable: true },
                { data: 'ic_file_so', name: 'ic_file_so', orderable: true },
                { data: 'action', name: 'action', orderable: false, searchable: false, className: 'center'}
            ],
            "order": [['0', 'asc']]
        });

        $('.select2').select2();

        $('.datepicker').datepicker({
            dateFormat : 'yy-mm-dd'
        });

        $('.form-check-input-styled').uniform();

        $('.dataTables_filter input[type=search]').attr('placeholder','Pencarian');

        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });

        $('.dt-buttons').append('<button class="btn btn-light bg-white" onclick="add()"><i class="icon-add"></i> <span class="d-none d-lg-inline-block ml-2">Tambah Data</span></button>');
    });

    function add()
    {
        _reset();
        $('.form-check-input-styled').prop('checked',false).uniform('refresh');
        $('#act-save').show();
        $('#act-update').hide();
        $('#frmbox').modal({keyboard: false, backdrop: 'static'});
    }

    function save(url)
    {
        if($("#frm").valid())
        {
            var formData = new FormData($('#frm')[0]);

            $.ajax({
                url:'{{ url("ic/inkubasi") }}'+'/'+url,
                type:'post',
                data: formData,
                contentType: false,//untuk upload image
                processData: false,//untuk upload image
                timeout: 300000, // sets timeout to 3 seconds
                dataType:'json',
                success : function(e) {
                    if(e.status == 'ok;') {
                        new Noty({
                            text: 'Berhasil Menyimpan Data',
                            type: 'success'
                        }).show();

                        dTable.draw();
                        $("#frmbox").modal('hide');
                    } else {
                        alert(e.text);
                    }
                }
            });
        }
    }

    function edit(id)
    {
        $.ajax({
            url:'{{ url("ic/inkubasi/edit") }}',
            type:'post',
            dataType:'json',
            data: ({ id : id }),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(e) {
                _reset();
                $('#act-save').hide();
                $('#act-update').show();
                $('#id').val(id);
                $.each(e, function(key, value) {
                    if($('#'+key).hasClass("select2")) {
                        $('#'+key).val(value).trigger('change');
                    }
                    else if($('input[type=radio]').hasClass(key)){
                        if(value!=""){
                            $("input[name='inp["+key+"]'][value='" + value + "']").prop('checked', true);
                            $.uniform.update();
                        }
                    }
                    else if($('input[type=checkbox]').hasClass(key)){
                        if(value!=null){
                            var temp = value.split('; ');
                            for (var i = 0; i < temp.length; i++) {
                                $("input[name='inp["+key+"][]'][value='" + temp[i] + "']").prop('checked', true);
                            }
                            $.uniform.update();
                        }
                    }
                    else  $('#'+key).val(value);
                });
                $('#frmbox').modal({keyboard: false, backdrop: 'static'});
            }
        });
    }

    function del(id, txt)
    {
        if(confirm('Data: '+txt+'\n'+'Apakah anda yakin akan menghapus data ini?')) {
        $.ajax({
            url:'{{ url("ic/inkubasi/delete") }}',
            type:'delete',
            dataType:'json',
            data: ({id : id }),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(e) {
                if(e.status == 'ok;') {
                    new Noty({
                        text: 'Berhasil Menghapus Data',
                        type: 'success'
                    }).show();

                    dTable.draw();
                } else {
                    alert(e.text);
                }
            }
        });
    }
    }
</script>
@endsection