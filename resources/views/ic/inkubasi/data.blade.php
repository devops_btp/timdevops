@extends('tpl.limitless.master_layout4')

@section('css_section')

@endsection

@section('page_title')
Master Data Inkubasi Center
@endsection

@section('content')
<div class="content-wrapper">

    <!-- Basic datatable -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Data Startup</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <table class="table datatable-basic">
            <thead>
                <tr>
                    <th>Nama Startup</th>
                    <th>Nama Produk</th>
                    <th>Nama Founder</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @for ($i=0 ;$i<5 ;$i++)   
                    <tr>
                        <td><a href="#">BTP</a></td>
                        <td>Inkubator</a></td>
                        <td>Ramdan</td>
                        <td >
                            <div>
                                <a href="#" class="btn btn-primary">Edit</a>
                                <a href="#" class="btn btn-danger">Hapus</a>
                            </div>
                        </td>
                    </tr>
                @endfor
            </tbody>
        </table>
    </div>
    <!-- /basic datatable -->

</div>
@endsection

@section('js_section')
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/interactions.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/widgets.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/effects.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/mousewheel.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/globalize/globalize.js')}}"></script>
@endsection
