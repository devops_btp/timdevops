@extends('tpl.limitless.master_layout4')

	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_1/css/bootstrap.min.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_1/css/bootstrap_limitless.min.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_1/css/layout.min.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_1/css/components.min.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_1/css/colors.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/custom.css')}}">
	<style>
		.stretched-link::after {
	position: absolute;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	z-index: 1;
	pointer-events: auto;
	content: "";
	background-color: rgba(0,0,0,0);
	}
		</style>

@section('page_title')
Detail Data Startup
@endsection

@section('content')

<!-- Content area -->
			<div class="content">

				<!-- Inner container -->
				<div class="d-md-flex align-items-md-start">

					<!-- Left sidebar component -->
					<div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-left wmin-300 border-0 shadow-0 sidebar-expand-md">

						<!-- Sidebar content -->
						<div class="sidebar-content">

							<!-- Navigation -->
							<div class="card">
								<div class="card-body bg-indigo-400 text-center card-img-top" style="background-image: {{url('/themes/limitless/global/images/panel_bg.png')}}; background-size: contain;">
									<div class="card-img-actions d-inline-block mb-3">
										<img class="img-fluid rounded-circle" src="{{url('/themes/limitless/global/images/logobtp.png')}}" width="170" height="170" alt="">
										<div class="card-img-actions-overlay rounded-circle">
											<a href="#" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round">
												<i class="icon-plus3"></i>
											</a>
											<a href="user_pages_profile.html" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round ml-2">
												<i class="icon-link"></i>
											</a>
										</div>
									</div>

						    		<h6 class="font-weight-semibold mb-0">Bandung Techno Park</h6>
						    		<span class="d-block opacity-75">Inkubasi Bisnis</span>

					    			<div class="list-icons list-icons-extended mt-3">
				                    	<a href="#" class="list-icons-item text-white" data-popup="tooltip" title="" data-container="body" data-original-title="Google Drive"><i class="icon-google-drive"></i></a>
				                    	<a href="#" class="list-icons-item text-white" data-popup="tooltip" title="" data-container="body" data-original-title="Twitter"><i class="icon-twitter"></i></a>
				                    	<a href="#" class="list-icons-item text-white" data-popup="tooltip" title="" data-container="body" data-original-title="Github"><i class="icon-github"></i></a>
			                    	</div>
						    	</div>

								<div class="card-body p-0">
									<ul class="nav nav-sidebar mb-2">
										<li class="nav-item-header">Navigation</li>
										<li class="nav-item">
											<a href="#profile" class="nav-link active" data-toggle="tab">
												<i class="icon-user"></i>
												 Profile Startup
											</a>
										</li>
										<li class="nav-item">
											<a href="#schedule" class="nav-link" data-toggle="tab">
												<i class="icon-calendar3"></i>
												Data Produk
												{{-- <span class="font-size-sm font-weight-normal opacity-75 ml-auto">02:56pm</span> --}}
											</a>
										</li>
										<li class="nav-item">
											<a href="#inbox" class="nav-link" data-toggle="tab">
												<i class="icon-envelop2"></i>
												Data Tim
												{{-- <span class="badge bg-danger badge-pill ml-auto">29</span> --}}
											</a>
										</li>

									</ul>
								</div>
							</div>
							<!-- /navigation -->

						</div>
						<!-- /sidebar content -->

					</div>
					<!-- /left sidebar component -->


					<!-- Right content -->
					<div class="tab-content w-100 overflow-auto">
						<div class="tab-pane fade active show" id="profile">



							<!-- Profile info -->
							<div class="card">
								<div class="card-header header-elements-inline">
									<h5 class="card-title">Informasi Startup</h5>
									<div class="header-elements">
										<div class="list-icons">
					                		<a class="list-icons-item" data-action="collapse"></a>
					                		<a class="list-icons-item" data-action="reload"></a>
					                		<a class="list-icons-item" data-action="remove"></a>
					                	</div>
				                	</div>
								</div>

								<div class="card-body">
									<form action="#">
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>Nama Startup</label>
													<input type="text" readonly="readonly" value="Bandung Techno Park" class="form-control">
												</div>
												<div class="col-md-6">
													<label>Alamat</label>
													<input type="text" readonly="readonly" value="Jl. Telekomunikasi, Sukapura, Kec. Dayeuhkolot, Bandung, Jawa Barat 40257" class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>Visi</label>
													<textarea rows="3" cols="3" class="form-control" readonly="readonly" placeholder="“MENJADI MOTOR PENGGERAK DALAM MEWUJUDKAN MASYARAKAT INFORMASI INDONESIA DAN PENDORONG TUMBUHNYA INDUSTRI ICT DAN TECHNOPRENEUR DI INDONESIA”"></textarea>
												</div>
												
												<div class="col-md-6">
													<label>Visi</label>
													<textarea rows="3" cols="3" class="form-control" readonly="readonly" placeholder="1. MENINGKATKAN KERJASAMA ANTARA ACADEMIC – BUSINESS – GOVERMENT DALAM PENGEMBANGAN ICT YANG MELIPUTI: INFRASTRUKTUR, APLIKASI, CONTENT, KONTEKS, DAN REGULASI. 2. MENDORONG PERKEMBANGAN EKONOMI DAN BUDAYA BERBASIS PENGETAHUAN DAN TEKNOLOGI. 3. MENCIPTAKAN TENAGA ICT YANG MANDIRI DAN BERDAYA SAING TINGGI. 4. MENUMBUHKEMBANGKAN MASYARAKAT YANG MAMPU MEMANFAATKAN ICT DALAM PENINGKATAN KESEJAHTERAAN. 5. MENCIPTAKAN TECHNOPREUNERSHIP DI MASYARAKAT."></textarea>
												</div>
												
											</div>
										</div>

										{{-- <div class="form-group">
											<div class="row">
												<div class="col-md-4">
													<label>City</label>
													<input type="text" value="Munich" class="form-control">
												</div>
												<div class="col-md-4">
													<label>State/Province</label>
													<input type="text" value="Bayern" class="form-control">
												</div>
												<div class="col-md-4">
													<label>ZIP code</label>
													<input type="text" value="1031" class="form-control">
												</div>
											</div>
										</div> --}}

										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>Data Media Sosial</label>
													<input type="text" readonly="readonly" value="MyBTP" class="form-control">
												</div>
												<div class="col-md-6">
													<label>Website</label>
													<input type="text" readonly="readonly" value="btp.or.id" class="form-control">
												</div>
											</div>
										</div>

				                        <div class="form-group">
				                        	<div class="row">
				                        		<div class="col-md-6">
													<label>Jumlah Pegawai</label>
													<input readonly= type="text" value="70" class="form-control">
													{{-- <span class="form-text text-muted">+99-99-9999-9999</span> --}}
				                        		</div>

											
				                        	</div>
				                        </div>

				                        {{-- <div class="text-right">
				                        	<button type="submit" class="btn btn-primary">Save changes</button>
				                        </div> --}}
									</form>
								</div>
							</div>
							<!-- /profile info -->

					    </div>

					    <div class="tab-pane fade" id="schedule">

							<div class="card">
								<div class="card-header header-elements-inline">
									<h5 class="card-title">Data Produk</h5>
									<div class="header-elements">
										<div class="list-icons">
					                		<a class="list-icons-item" data-action="collapse"></a>
					                		<a class="list-icons-item" data-action="reload"></a>
					                		<a class="list-icons-item" data-action="remove"></a>
					                	</div>
				                	</div>
								</div>

								<div class="card-body">
									<form action="#">
										<div class="form-group">
											<div class="row">
												{{-- <div class="card">
													<div class="card-img-actions m-1">
														<img class="card-img img-fluid" src="../../../../global_assets/images/placeholders/placeholder.jpg" alt="">
														<div class="card-img-actions-overlay card-img">
															<a href="../../../../global_assets/images/placeholders/placeholder.jpg" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round" data-popup="lightbox" rel="group">
																<i class="icon-plus3"></i>
															</a>
						
															<a href="#" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round ml-2">
																<i class="icon-link"></i>
															</a>
														</div>
													</div>
												</div> --}}
												<div class="col-md-6">
													<label>Nama Produk</label>
													<input type="text" readonly="readonly" value="Jl. Telekomunikasi, Sukapura, Kec. Dayeuhkolot, Bandung, Jawa Barat 40257" class="form-control">
												</div>
												<div class="col-md-6">
													<label>Alamat</label>
													<input type="text" readonly="readonly" value="Jl. Telekomunikasi, Sukapura, Kec. Dayeuhkolot, Bandung, Jawa Barat 40257" class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>Visi</label>
													<textarea rows="3" cols="3" class="form-control" readonly="readonly" placeholder="“MENJADI MOTOR PENGGERAK DALAM MEWUJUDKAN MASYARAKAT INFORMASI INDONESIA DAN PENDORONG TUMBUHNYA INDUSTRI ICT DAN TECHNOPRENEUR DI INDONESIA”"></textarea>
												</div>
												
												<div class="col-md-6">
													<label>Visi</label>
													<textarea rows="3" cols="3" class="form-control" readonly="readonly" placeholder="1. MENINGKATKAN KERJASAMA ANTARA ACADEMIC – BUSINESS – GOVERMENT DALAM PENGEMBANGAN ICT YANG MELIPUTI: INFRASTRUKTUR, APLIKASI, CONTENT, KONTEKS, DAN REGULASI. 2. MENDORONG PERKEMBANGAN EKONOMI DAN BUDAYA BERBASIS PENGETAHUAN DAN TEKNOLOGI. 3. MENCIPTAKAN TENAGA ICT YANG MANDIRI DAN BERDAYA SAING TINGGI. 4. MENUMBUHKEMBANGKAN MASYARAKAT YANG MAMPU MEMANFAATKAN ICT DALAM PENINGKATAN KESEJAHTERAAN. 5. MENCIPTAKAN TECHNOPREUNERSHIP DI MASYARAKAT."></textarea>
												</div>
												
											</div>
										</div>

										{{-- <div class="form-group">
											<div class="row">
												<div class="col-md-4">
													<label>City</label>
													<input type="text" value="Munich" class="form-control">
												</div>
												<div class="col-md-4">
													<label>State/Province</label>
													<input type="text" value="Bayern" class="form-control">
												</div>
												<div class="col-md-4">
													<label>ZIP code</label>
													<input type="text" value="1031" class="form-control">
												</div>
											</div>
										</div> --}}

										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>Data Media Sosial</label>
													<input type="text" readonly="readonly" value="MyBTP" class="form-control">
												</div>
												<div class="col-md-6">
													<label>Website</label>
													<input type="text" readonly="readonly" value="btp.or.id" class="form-control">
												</div>
											</div>
										</div>

				                        <div class="form-group">
				                        	<div class="row">
				                        		<div class="col-md-6">
													<label>Jumlah Pegawai</label>
													<input readonly= type="text" value="70" class="form-control">
													{{-- <span class="form-text text-muted">+99-99-9999-9999</span> --}}
				                        		</div>

											
				                        	</div>
				                        </div>

				                        {{-- <div class="text-right">
				                        	<button type="submit" class="btn btn-primary">Save changes</button>
				                        </div> --}}
									</form>
								</div>
							</div>
				    		<!-- Available hours -->
							{{-- <div class="card">
								<div class="card-header header-elements-inline">
									<h6 class="card-title">Available hours</h6>
									<div class="header-elements">
										<div class="list-icons">
					                		<a class="list-icons-item" data-action="collapse"></a>
					                		<a class="list-icons-item" data-action="reload"></a>
					                		<a class="list-icons-item" data-action="remove"></a>
					                	</div>
				                	</div>
								</div>

								<div class="card-body">
									<div class="chart-container">
										<div class="chart has-fixed-height" id="available_hours"></div>
									</div>
								</div>
							</div> --}}
							<!-- /available hours -->


							<!-- Schedule -->
							{{-- <div class="card">
								<div class="card-header header-elements-inline">
									<h5 class="card-title">My schedule</h5>
									<div class="header-elements">
										<div class="list-icons">
					                		<a class="list-icons-item" data-action="collapse"></a>
					                		<a class="list-icons-item" data-action="reload"></a>
					                		<a class="list-icons-item" data-action="remove"></a>
					                	</div>
				                	</div>
								</div>

								<div class="card-body">
									<div class="my-schedule"></div>
								</div>
							</div> --}}
							<!-- /schedule -->

				    	</div>

					    <div class="tab-pane fade" id="inbox">

				    	</div>

				    	<div class="tab-pane fade" id="orders">


				    	</div>
					</div>
					<!-- /right content -->

				</div>
				<!-- /inner container -->

			</div>
			<!-- /content area -->

			<!-- Core JS files -->
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/main/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/main/bootstrap.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/loaders/blockui.min.js')}}"></script>
	<!-- /core JS files -->

	<script type="text/javascript" src="/themes/limitless/layout_1/js/app.js"></script>

	<!-- Theme JS files -->
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/extensions/rowlink.js')}}"></script>
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/visualization/echarts/echarts.min.js')}}"></script>
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/forms/selects/select2.min.js')}}"></script>
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/ui/fullcalendar/core/main.min.js')}}"></script>
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/ui/fullcalendar/daygrid/main.min.js')}}"></script>
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/ui/fullcalendar/timegrid/main.min.js')}}"></script>
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/ui/fullcalendar/interaction/main.min.js')}}"></script>

	<script type="text/javascript" src="{{url('themes/limitless/global/js/plugins/media/fancybox.min.js')}}"></script>

	<script type="text/javascript" src="{{url('/themes/limitless/global/js/demo_pages/user_pages_profile_tabbed.js')}}"></script>
	<!-- /theme JS files -->
@endsection

	

