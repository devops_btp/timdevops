@extends('tpl.limitless.master')
@section('icon', "icon-cube3")
@section('title', "Application")

<!-- contents -->
@section('content')
    <style>
        .panel.widget {
            padding: 30px;
            border: 0;
            border-radius: 3px;
            flex: 1;
            margin: 10px
        }

        .panel.widget.center {
            text-align: center
        }

        .panel.widget .avatar {
            width: 100px;
            height: auto;
            border: 0;
            border-radius: 50%
        }

        .panel.widget i {
            font-size: 48px;
            background: rgba(0, 0, 0, .3);
            border-radius: 50%;
            width: 100px;
            height: 100px;
            display: block;
            margin: 0 auto;
            color: #eee;
            line-height: 110px
        }

        .panel.widget h4 {
            color: #fff;
            font-weight: 300;
            margin-top: 20px;
            font-size: 20px
        }

        .panel.widget p {
            font-size: 20px;
            font-size: 14px;
            line-height: 1.57142857;
            color: #ddd;
            display: block;
            max-height: 65px;
            margin: 30px 0 0
        }

        .panel.widget .readm-link {
            text-decoration: underline
        }

        .panel.widget .btn-primary, .panel.widget .readm-link {
            margin-top: 20px;
            color: #fff
        }

        .btn {
            padding: 6px 15px;
            font-size: 14px;
            line-height: 1.57142857;
            border-radius: 3px;
            transition: border .2s linear, color .2s linear, width .2s linear, background-color .2s linear;
            -webkit-font-smoothing: subpixel-antialiased
        }

        .panel.widget.bgimage {
            background-size: cover;
            background-position: 50%;
            position: relative
        }

        .panel.widget .panel-content {
            z-index: 9;
            position: relative
        }

        .panel.widget .dimmer {
            background: rgba(45, 53, 61, .5);
            background: linear-gradient(135deg, rgba(45, 53, 61, .79), rgba(45, 53, 61, .5));
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#c92d353d", endColorstr="#802d353d", GradientType=1);
            opacity: 1;
            transition: all .3s ease;
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            border-radius: 0
        }

        .side-menu .panel.widget .dimmer {
            border-radius: 0
        }

        .expanded .side-menu .panel.widget .dimmer, .side-menu:hover .panel.widget .dimmer {
            background: linear-gradient(275deg, #242c32, rgba(36, 44, 50, .95) 34%, rgba(36, 44, 50, .7));
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#242c32", endColorstr="#00242c32", GradientType=1)
        }

        .side-menu .navbar-header, .voyager .side-menu .navbar-header {
            float: none;
            margin-bottom: 0
        }

        .side-menu .panel.widget {
            padding: 12px 0;
            margin: 0;
            border-radius: 0
        }

        .side-menu-container .side-menu .panel.widget .dimmer {
            border-radius: 0
        }

        .app-container .side-menu .panel.widget a, .app-container .side-menu .panel.widget h4, .app-container .side-menu .panel.widget p {
            display: none
        }

        .app-container .side-menu .panel.widget p {
            font-size: 12px;
            margin-top: 0
        }

        .app-container .side-menu .panel.widget .avatar {
            width: 36px;
            height: 36px;
            float: left;
            margin-left: 12px;
            border: 0;
            position: relative;
            top: 3px
        }

        .app-container .side-menu .panel.widget h4 {
            float: left;
            display: block;
            position: absolute;
            left: 60px;
            top: -7px;
            width: 190px;
            text-align: left;
            opacity: 0;
            transition: opacity .3s ease;
            margin-top: 17px;
            left: 56px;
            overflow: hidden;
            height: 26px
        }

        .app-container.expanded .panel.widget h4, .app-container .side-menu:hover .panel.widget h4 {
            opacity: 1
        }

        .app-container .side-menu:hover .panel.widget .avatar {
            float: left;
            margin-left: 12px;
            transition: none
        }
    </style>
    <div class="clearfix container-fluid row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="panel widget center bgimage" style="margin-bottom:0;overflow:hidden;">
                <div class="dimmer"></div>
                <div class="panel-content">
                    <i class="icon-cube3"></i> 
                    <p>Ini adalah calon untuk dashboard</p> 
                </div>
            </div>
        </div> 
    </div>
@endsection
<!-- /contents -->

<!-- javascript page -->
@section('js_section') 
@endsection
<!-- /javascript page -->