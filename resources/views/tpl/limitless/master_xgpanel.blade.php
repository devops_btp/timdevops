<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- end CSRF Token -->
	
	<title>@yield('title')</title>
    
	<!--<link rel="shortcut icon" href="cdn/environment/favicon.ico" type="image/x-icon">
	<link rel="icon" href="assets/limitless/layout_1/frontend/images/favicon.ico" type="image/x-icon">-->

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/icons/icomoon/styles.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/icons/flag-icon/css/flag-icon.min.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/icons/fontawesome/styles.min.css')}}">
	
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_1/css/bootstrap.min.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_1/css/bootstrap_limitless.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_1/css/layout.min.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_1/css/components.min.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_1/css/colors.css')}}">
	<!-- /global stylesheets -->
    <link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/custom.css')}}">

    @yield('css_section')
</head>

<body class="@yield('bodyClass')">
	
    <!-- Loading -->
    <div class="yloading" id="loading-img">
    <div class="loader-container">
        <div class="theme_xbox theme_xbox_with_text">
            <div class="pace_progress" data-progress-text="60%" data-progress="60"></div>
            <div class="pace_activity"></div> <span>LOADING...</span>
        </div>
    </div>
    </div>
    <!-- Loading -->
	
    <!-- Main navbar -->
	<div class="navbar navbar-expand-md navbar-dark bg-danger-800" style="background-image:url({{url('/images/backgrounds/bg.png')}})" >
		<div class="navbar-brand">
			<a href="index.html" class="d-inline-block">
				<img src="{{url('/themes/limitless/global/images/gpanel_light.png')}}" alt="">
			</a>
		</div>

		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-tree5"></i>
			</button>
			<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
				<i class="icon-paragraph-justify3"></i>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="navbar-mobile">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
						<i class="icon-paragraph-justify3"></i>
					</a>
				</li>
			</ul>

			<span class="badge bg-success ml-md-3 mr-md-auto">Online</span>

			<ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a href="#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown">
                        <i class="icon-bell3"></i>
                        <span class="d-md-none ml-2">Notification</span>
                        <span class="badge badge-pill bg-warning-400 ml-auto ml-md-0">9</span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
                        <div class="dropdown-content-header">
                            <span class="font-weight-semibold">Git updates</span>
                            <a href="#" class="text-default"><i class="icon-sync"></i></a>
                        </div>

                        <div class="dropdown-content-body dropdown-scrollable">
                            <ul class="media-list">
                                <li class="media">
                                    <div class="mr-3">
                                        <a href="#" class="btn bg-transparent border-primary text-primary rounded-round border-2 btn-icon"><i class="icon-git-pull-request"></i></a>
                                    </div>

                                    <div class="media-body">
                                        Drop the IE <a href="#">specific hacks</a> for temporal inputs
                                        <div class="text-muted font-size-sm">4 minutes ago</div>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="mr-3">
                                        <a href="#" class="btn bg-transparent border-warning text-warning rounded-round border-2 btn-icon"><i class="icon-git-commit"></i></a>
                                    </div>

                                    <div class="media-body">
                                        Add full font overrides for popovers and tooltips
                                        <div class="text-muted font-size-sm">36 minutes ago</div>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="mr-3">
                                        <a href="#" class="btn bg-transparent border-info text-info rounded-round border-2 btn-icon"><i class="icon-git-branch"></i></a>
                                    </div>

                                    <div class="media-body">
                                        <a href="#">Chris Arney</a> created a new <span class="font-weight-semibold">Design</span> branch
                                        <div class="text-muted font-size-sm">2 hours ago</div>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="mr-3">
                                        <a href="#" class="btn bg-transparent border-success text-success rounded-round border-2 btn-icon"><i class="icon-git-merge"></i></a>
                                    </div>

                                    <div class="media-body">
                                        <a href="#">Eugene Kopyov</a> merged <span class="font-weight-semibold">Master</span> and <span class="font-weight-semibold">Dev</span> branches
                                        <div class="text-muted font-size-sm">Dec 18, 18:36</div>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="mr-3">
                                        <a href="#" class="btn bg-transparent border-primary text-primary rounded-round border-2 btn-icon"><i class="icon-git-pull-request"></i></a>
                                    </div>

                                    <div class="media-body">
                                        Have Carousel ignore keyboard events
                                        <div class="text-muted font-size-sm">Dec 12, 05:46</div>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div class="dropdown-content-footer bg-light">
                            <a href="#" class="text-grey mr-auto">All updates</a>
                            <div>
                                <a href="#" class="text-grey" data-popup="tooltip" title="Mark all as read"><i class="icon-radio-unchecked"></i></a>
                                <a href="#" class="text-grey ml-2" data-popup="tooltip" title="Bug tracker"><i class="icon-bug2"></i></a>
                            </div>
                        </div>
                    </div>
                </li>
				<li class="nav-item dropdown dropdown-user">
					<a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
						<img src="{{url('/images/az9bZ6b_700b.jpg')}}" class="rounded-circle mr-2" height="34" alt="">
						<span>Victoria</span>
					</a>

					<div class="dropdown-menu dropdown-menu-right">
						<a href="{{url('xgpanel/users')}}" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
						<a href="{{url('xgpanel/login/logout')}}" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-content">

		<!-- Main sidebar -->
		<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">
			<!-- Sidebar mobile toggler -->
			<div class="sidebar-mobile-toggler text-center">
				<a href="#" class="sidebar-mobile-main-toggle">
					<i class="icon-arrow-left8"></i>
				</a>
				Navigation
				<a href="#" class="sidebar-mobile-expand">
					<i class="icon-screen-full"></i>
					<i class="icon-screen-normal"></i>
				</a>
			</div>
			<!-- /sidebar mobile toggler -->
			
			<!-- Sidebar content -->
			<div class="sidebar-content">

				<!-- User menu -->
				<div class="sidebar-user">
					<div class="card-body">
						<div class="media">
							<div class="mr-3">
								<a href="#"><img src="{{url('/images/az9bZ6b_700b.jpg')}}" width="38" height="38" class="rounded-circle" alt=""></a>
							</div>

							<div class="media-body">
								<div class="media-title font-weight-semibold">Victoria</div>
								<div class="font-size-xs opacity-50">
									<i class="icon-pin font-size-sm"></i> &nbsp;Administrator
								</div>
							</div>

							<div class="ml-3 align-self-center">
								<a href="#" class="text-white"><i class="icon-cog3"></i></a>
							</div>
						</div>
					</div>
				</div>
				<!-- /user menu -->
			
				<!-- Main navigation -->
				<div class="card card-sidebar-mobile">
					<ul class="nav nav-sidebar" data-nav-type="accordion">

						<!-- Main -->
						<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
						<li class="nav-item">
							<a href="{{ url('gpanel') }}" class="nav-link">
								<i class="icon-home4"></i> <span>Dashboard</span>
							</a>
						</li>
                        <li class="nav-item">
                            <a href="{{ url('xgpanel/language') }}" class="nav-link">
                                <i class="icon-bubbles6"></i> <span>Language</span>
                            </a>
                        </li>
                        <li class="nav-item nav-item-submenu">
                            <a href="#" class="nav-link"><i class="icon-markup"></i> <span>Build</span></a>

                            <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                                <li class="nav-item">
                                    <a href="{{ url('xgpanel/apps') }}" class="nav-link">
                                        <i class="icon-cube3"></i> <span>Application</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('xgpanel/module') }}" class="nav-link">
                                        <i class="icon-puzzle"></i> <span>Module</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('xgpanel/navigation') }}" class="nav-link">
                                        <i class="icon-tree7"></i> <span>Navigation</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item nav-item-submenu">
                            <a href="#" class="nav-link"><i class="icon-user-lock"></i> <span>Manajemen User</span></a>

                            <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                                <li class="nav-item">
                                    <a href="{{ url('xgpanel/usergroups') }}" class="nav-link">
                                        <i class="icon-users4"></i> <span>User Group</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('xgpanel/users') }}" class="nav-link">
                                        <i class="icon-user-lock"></i> <span>User</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item nav-item-submenu">
                            <a href="#" class="nav-link"><i class="icon-hammer-wrench"></i> <span>Tools</span></a>

                            <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                                <li class="nav-item">
                                    <a href="{{ url('xgpanel/settings') }}" class="nav-link">
                                        <i class="icon-cog52"></i> <span>Setting</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('xgpanel/documentation/type/api') }}" class="nav-link">
                                        <i class="icon-bookmark"></i> <span>Documentation</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('xgpanel/logs') }}" class="nav-link">
                                        <i class="icon-folder-search"></i> <span>Tracking Log</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
					</ul>
				</div>
				<!-- /main navigation -->
				
			</div>
			<!-- Sidebar content -->
			
		</div>
		<!-- /main sidebar -->

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4><i class="@yield('icon') mr-2"></i> <span class="font-weight-semibold">@yield('title')</span></h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					<div class="header-elements d-none">
                        <div class="daterange-custom" id="reportrange">
                            <div class="daterange-custom-display">
                                <i>{{ date('d') }}</i> <b><i>{{ \Carbon\Carbon::now()->format('F') }}</i> <i>{{ date('Y') }}</i></b>
                            </div>
                        </div>

                        <div class="list-icons ml-3">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown"><i class="icon-grid3"></i></a>
                                <div class="dropdown-menu">
                                    <a href="xgpanel/modules" class="dropdown-item"><i class="icon-puzzle"></i> Module</a>
                                </div>
                            </div>
                        </div>

                        <!--<div class="breadcrumb">
                            <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <span class="breadcrumb-item active">Dashboard</span>
                        </div>
						<!--<div class="d-flex justify-content-center">
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
						</div>-->
					</div>
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
						<div class="breadcrumb">
							<a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
							<span class="breadcrumb-item active">@yield('title')</span>
						</div>

						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					<!--<div class="header-elements d-none">
						<div class="breadcrumb justify-content-center">
							<a href="#" class="breadcrumb-elements-item">
								<i class="icon-comment-discussion mr-2"></i>
								Support
							</a>

							<div class="breadcrumb-elements-item dropdown p-0">
								<a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear mr-2"></i>
									Settings
								</a>

								<div class="dropdown-menu dropdown-menu-right">
									<a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
									<a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
									<a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
									<div class="dropdown-divider"></div>
									<a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
								</div>
							</div>
						</div>
					</div>-->
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content">@yield('content')</div>
			<!-- /content area -->


			<!-- Footer -->
			<div class="navbar navbar-expand-lg navbar-light">
				<div class="text-center d-lg-none w-100">
					<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
						<i class="icon-unfold mr-2"></i>
						Footer
					</button>
				</div>

				<div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; 2019 - {{ date('Y') }}. <span class="text-primary font-weight-semibold">iGracias Application</span> by Digital Transformation YPTelkom
					</span>

					<ul class="navbar-nav ml-lg-auto">
						<li class="nav-item"><a href="" class="navbar-nav-link"><i class="icon-lifebuoy mr-2"></i> Support</a></li>
					</ul>
				</div>
			</div>
			<!-- /footer -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page container -->

    <script>
        var dTable;
    </script>

	<!-- Core JS files -->
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/main/jquery.min.js')}}"></script>	
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/main/bootstrap.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/loaders/blockui.min.js')}}"></script>
	<!-- /core JS files -->
	 
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/ui/moment/moment.min.js')}}"></script>
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/pickers/daterangepicker.js')}}"></script>

	<script type="text/javascript" src="{{url('/themes/limitless/layout_1/js/app.js')}}"></script>
	<!-- /theme JS files -->
	
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/tables/datatables/extensions/jszip/jszip.min.js')}}"></script>
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/tables/datatables/extensions/buttons/dataTables.buttons.min.js')}}"></script>
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/tables/datatables/extensions/buttons/buttons.html5.min.js')}}"></script>
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/forms/selects/select2.min.js')}}"></script>
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/forms/validation/validate.min.js')}}"></script> 
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/forms/validation/additional_methods.min.js')}}"></script>
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/notifications/noty.min.js')}}"></script>
	<script type="text/javascript" src="{{url('/themes/limitless/global/js/setting.js')}}"></script>  
	
	@yield('js_section')
</body>
</html>