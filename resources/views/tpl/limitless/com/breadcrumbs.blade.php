@if($items->hasParent())
    @include('tpl.limitless.com.breadcrumbs', ['items' => $items->parent()])
@endif

<span class="breadcrumb-item">{!! $items->title !!}</span>

