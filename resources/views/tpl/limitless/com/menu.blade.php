@foreach($items as $item)
<li class="nav-item @if($item->hasChildren()) nav-item-submenu @endif">
    @if(str_contains($item->alias, 'apps') == true and $item->hasChildren() == false)

    @else
        <a href="@if($item->hasChildren())#@else{{ url($item->url()) }}@endif"
           class="nav-link {{ 'mod'.session()->get('cname') == $item->alias ? 'active' : '' }}">
            {!! $item->icon !!} <span>{!! $item->title !!}</span>
        </a>
    @endif

    @if($item->hasChildren())
        <ul class="nav nav-group-sub">
            @include('tpl.limitless.com.menu', ['items' => $item->children()])
        </ul>
    @endif
</li>
@endforeach