@if($element == 'a')
	<a class="btn btn-{{ empty($color) ? 'primary' : $color }} {{ empty($class) ? '' : $class }}" id="{{ empty($id) ? '' : $id }}" href="{{ empty($action) ? '' : $action }}">{{ $slot }}</a>
@else
	<button type="{{ empty($type) ? '' : $type }}" class="btn btn-{{ empty($color) ? 'primary' : $color }} {{ empty($class) ? '' : $class }}" id="{{ empty($id) ? '' : $id }}" onclick="{{ empty($action) ? '' : $action }}">{{ $slot }}</button>
@endif