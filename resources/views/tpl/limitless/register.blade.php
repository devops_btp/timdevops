<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- end CSRF Token -->
	
	<title>XGracias</title>

    <link rel="icon" type="image/png" href="{{url('/images/xgracias-favicon.png')}}" />

	<!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/icons/icomoon/styles.css')}}">
	
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/login.css').'?v='.\Carbon::now()->timestamp}}">



    <style>
        body {
            background-image:url('{{url('/images/bg-login-2.jpg')}}');
            background-color: #FFFFFF;
        }
        body.login .login-container { 
            top :30% !important;
        }
        body.login .login-sidebar {
            border-top:5px solid #C62828;
        }
        @media (max-width: 767px) {
            body.login .login-sidebar {
                border-top:0px !important;
                border-left:5px solid #C62828;
            }
        }
        body.login .form-group-default.focused{
            border-color:#C62828;
        }
        .login-button, .bar:before, .bar:after{
            background:#C62828;
        }
        
        .validation-invalid-label{
            font-weight:bold !important;
        }

        body.login .alert-green {
            background: #2E7D32;
            border-left: 5px solid rgba(0,0,0,.1);
            font-size: 12px;
        }
        body.login .alert-black, body.login .alert-green {
            color: #fff;
            position: relative;
            z-index: 10;
            margin-top: 20px;
        }
    </style>
</head>

<body class="login">
<div class="container-fluid">
    <div class="row">
        <div class="faded-bg animated"></div>
        <div class="hidden-xs col-sm-7 col-md-8">
            <div class="clearfix">
                <div class="col-sm-12 col-md-10 col-md-offset-2">
                    <div class="logo-title-container">
                        <img class="img-responsive pull-left flip logo hidden-xs animated fadeIn" src="{{url('/images/xgracias-logo-only-512.png')}}" alt="XGracias System" style="padding-top: 65px">
                        <div class="copy animated fadeIn">
                            <h1>XGracias {{$login}}</h1>
                            <p>Welcome to XGracias. by Digital Transformation YPTelkom</p>
                        </div>
                    </div> <!-- .logo-title-container -->
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-5 col-md-4 login-sidebar">

            <div class="login-container">

                <p>Register Below:</p>

                <form id="frm_username" class="form-validates" method="POST" class="login-form" method="post" action="{{url($action)}}">
                    {{csrf_field()}}
                    
                    <div class="form-group form-group-default" id="nameGroup">
                        <label>Nama Lengkap</label>
                        <div class="controls">
                            <input type="text" name="inp[ref_fullname]" id="fullname" value="" placeholder="Nama Lengkap" class="form-control" required>
                        </div>
                    </div>
                    
                    <div class="form-group form-group-default" id="emailGroup">
                        <label>Email</label>
                        <div class="controls">
                            <input type="email" name="inp[ref_email]" id="ref_email" value="" placeholder="Email" class="form-control" required>
                        </div>
                    </div>
                    
                    <div class="form-group form-group-default" id="phoneGroup">
                        <label>No. Telp</label>
                        <div class="controls">
                            <input type="text" name="inp[ref_phone]" id="ref_phone" value="" placeholder="No. Telp" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group form-group-default" id="usernameGroup">
                        <label>Username</label>
                        <div class="controls">
                            <input type="text" name="inp[ref_username]" id="ref_username" value="" placeholder="Username" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group form-group-default" id="passwordGroup">
                        <label>Password</label>
                        <div class="controls">
                            <input type="password" name="password" id="password" placeholder="Password" class="form-control" required>
                        </div>
                    </div>

                    <button type="button" onclick="save()" class="btn btn-block login-button">
                        <span class="signingin hidden"><span class="voyager-refresh"></span> Register...</span>
                        <span class="signin">Register &nbsp; <i class="icon-circle-right2"></i></span>
                    </button>

                </form>

                <div style="clear:both"></div>

                @if (session()->has('alert'))
                <div class="alert alert-green">
                    <ul class="list-unstyled">
                        <li><span class="text-semibold">{{ session()->get('alert') }}</span></li>
                    </ul>
                </div>
                @endif
 

            </div> <!-- .login-container -->

        </div> <!-- .login-sidebar -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

<!-- Core JS files -->
<script type="text/javascript" src="{{url('/themes/limitless/global/js/main/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/main/bootstrap.bundle.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/loaders/blockui.min.js')}}"></script>
<!-- /core JS files -->
 
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/forms/validation/validate.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/forms/validation/additional_methods.min.js')}}"></script> 
<script> 
    var form = document.forms[0];
    var names = document.querySelector('[name="inp[ref_fullname]"]');
    var email = document.querySelector('[name="inp[ref_email]"]');
    var phone = document.querySelector('[name="inp[ref_phone]"]');
    var username = document.querySelector('[name="inp[ref_username]"]');
    var password = document.querySelector('[name="password"]');
  
    names.focus();
    document.getElementById('nameGroup').classList.add("focused");

    names.addEventListener('focusin', function(e){
        document.getElementById('nameGroup').classList.add("focused");
    });
    names.addEventListener('focusout', function(e){
        document.getElementById('nameGroup').classList.remove("focused");
    });

    email.addEventListener('focusin', function(e){
        document.getElementById('emailGroup').classList.add("focused");
    });
    email.addEventListener('focusout', function(e){
        document.getElementById('emailGroup').classList.remove("focused");
    });
 

    phone.addEventListener('focusin', function(e){
        document.getElementById('phoneGroup').classList.add("focused");
    });
    phone.addEventListener('focusout', function(e){
        document.getElementById('phoneGroup').classList.remove("focused");
    });

    $('div.alert').delay(5000).slideUp(300);


    // Focus events for email and password fields
    username.addEventListener('focusin', function(e){
        document.getElementById('usernameGroup').classList.add("focused");
    });
    username.addEventListener('focusout', function(e){
        document.getElementById('usernameGroup').classList.remove("focused");
    });

    password.addEventListener('focusin', function(e){
        document.getElementById('passwordGroup').classList.add("focused");
    });
    password.addEventListener('focusout', function(e){
        document.getElementById('passwordGroup').classList.remove("focused");
    });


var validator = "";
$(function(){    

    $('#frm_username').validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-invalid-label',
        successClass: 'validation-valid-label',
        validClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        /*success: function(label) {
            label.addClass('validation-valid-label').text('Success.'); // remove to hide Success message
        },*/

        // Different components require proper error label placement
        errorPlacement: function(error, element) {

            // Unstyled checkboxes, radios
            if (element.parents().hasClass('form-check')) {
                error.appendTo( element.parents('.form-check').parent() );
            }

            // Input with icons and Select2
            else if (element.parents().hasClass('form-group-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }

            // Input group, styled file input
            else if (element.parent().is('.uniform-uploader, .uniform-select') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }

            // Other elements
            else {
                error.insertAfter(element);
            }
        },
        rules: {
            password: {
                minlength: 8
            },
            repeat_password: {
                equalTo: '#password'
            },
            email: {
                email: true
            },
            repeat_email: {
                equalTo: '#email'
            },
            minimum_characters: {
                minlength: 10
            },
            maximum_characters: {
                maxlength: 10
            },
            minimum_number: {
                min: 10
            },
            maximum_number: {
                max: 10
            },
            number_range: {
                range: [10, 20]
            },
            url: {
                url: true
            },
            date: {
                date: true
            },
            date_iso: {
                dateISO: true
            },
            numbers: {
                number: true
            },
            digits: {
                digits: true
            },
            creditcard: {
                creditcard: true
            },
            basic_checkbox: {
                minlength: 2
            },
            styled_checkbox: {
                minlength: 2
            },
            switchery_group: {
                minlength: 2
            },
            switch_group: {
                minlength: 2
            },
            'inp[ref_username]' : { 
                remote: {
                    url:'{{url("register/check_username") }}',
                    type: "post",
                    headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend : function() {
                        $('#loading-img').show();
                    },
                    complete : function() {
                        $('#loading-img').hide();
                    },
                    error: function (request, status, error) { 
                    }
                },
                noSpace : true,
                lettersonly: true, 
                minlength: 6
            }        
        },
        messages: {
            custom: {
                required: 'This is a custom error message'
            },
            basic_checkbox: {
                minlength: 'Please select at least {0} checkboxes'
            },
            styled_checkbox: {
                minlength: 'Please select at least {0} checkboxes'
            },
            switchery_group: {
                minlength: 'Please select at least {0} switches'
            },
            switch_group: {
                minlength: 'Please select at least {0} switches'
            },
            agree: 'Please accept our policy',
            'inp[ref_username]':{
                remote: jQuery.validator.format("Your username already taken or use")
            }   
        },
        onkeyup: false
    });
}); 

function save()
{
    // alert($('.passtrengthMeter .tooltip').html());
    if($("#frm_username").valid())
    {
        $("#frm_username").submit(); 
    }
}
</script>
</body>
</html>