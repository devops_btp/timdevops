<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>My BTP Application Center</title>
    <base href="{{ url('') }}" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="cdn/environment/favicon.png" type="image/x-icon">

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/icons/icomoon/styles.css')}}">
    <link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/icons/fontawesome/styles.min.css')}}">

    <link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_1/css/bootstrap.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_1/css/bootstrap_limitless.css')}}">
    <link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_1/css/layout.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_1/css/components.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_1/css/colors.css')}}">
    <!-- /global stylesheets -->

    <link rel="shortcut icon" href="cdn/environment/favicon.ico" type="image/x-icon">
    <link rel="icon" href="cdn/environment/favicon.ico" type="image/x-icon">

    <style>
        @media (min-width:769px) {
            .login-container .page-container .login-form {
                margin:0px 20px;
                -webkit-box-shadow: 1px 2px 14px 1px rgba(38,38,38,.5);
                -moz-box-shadow: 1px 2px 14px 1px rgba(38,38,38,.5);
                box-shadow: 1px 2px 14px 1px rgba(38,38,38,.5);
            }
            .login-container .content {
                padding:20px;
            }
            .login-container .page-container {
                padding-top: 20px;
            }
        }
        .form-control {
            border: 1px solid #ccc;
        }
    </style>
</head>

<body class="login-container login-cover" style="background-image:url({{ url('images/login_cover_user.jpg') }});background-position: 0px -55px; background-repeat: no-repeat;">

<!-- Page content -->
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Content area -->
        <div class="content d-flex justify-content-center align-items-center">
            <!-- Advanced login -->
            <form method="post" action="{{ url('sso/login') }}">

                {{csrf_field()}}
                
                <div class="card" style="background-color: rgba(255,255,255,.75);">
                    <div class="card-body mb-0">
                        <div class="text-center mb-3 mx-5">
                            <img src="https://btp.telkomuniversity.ac.id/my/cdn/environment/logo.png" class="mb-3" style="height: 100px">
                            <h5 class="mb-0">My BTP Application Center</h5>
                            <span class="d-block text-muted">Silahkan Menggunakan Akun iGracias Anda</span>
                            @if ($errors = Session::get('errors'))
                          
                             <h3>{{$errors}}</H3>
                         
                          @endif
                        </div>

                        <div class="form-group form-group-feedback form-group-feedback-left">
                            <input type="text" name="username" id="username" class="form-control" placeholder="Username">
                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group form-group-feedback form-group-feedback-left">
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn bg-danger-800 btn-block">Login <i class="icon-circle-right2 ml-1"></i></button>
                            
                        </div>
        
                    </div>
                    
                </div>
                
            </form>
            
            <!-- /advanced login -->
        </div>
        <!-- /content area -->

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

<script type="text/javascript" src="{{ url('themes/limitless/global/js/main/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ url('themes/limitless/global/js/main/bootstrap.bundle.min.js') }}"></script>
<script type="text/javascript" src="{{ url('themes/limitless/global/js/plugins/loaders/blockui.min.js') }}"></script>
<script>
    $(document).ready(function() {

    });
</script>
</body>
</html>