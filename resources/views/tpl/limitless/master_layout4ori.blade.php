<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>@yield('page_title')</title>

	<link rel="icon" type="image/png" href="{{url('/images/xgracias-favicon.png')}}" />

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/icons/icomoon/styles.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/icons/flag-icon/css/flag-icon.min.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/icons/fontawesome/styles.min.css')}}">

	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_4/css/bootstrap.min.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_4/css/bootstrap_limitless.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_4/css/layout.min.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_4/css/components.min.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_4/css/colors.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/custom.css')}}">
	<!-- /global stylesheets -->

	@yield('css_section')
</head>
<body class="">

<!-- Loading -->
<div class="yloading" id="loading-img">
	<div class="loader-container">
		<div class="theme_xbox theme_xbox_with_text">
			<div class="pace_progress" data-progress-text="60%" data-progress="60"></div>
			<div class="pace_activity"></div> <span>LOADING...</span>
		</div>
	</div>
</div>
<!-- Loading -->

<!-- Main navbar -->
<div class="navbar navbar-expand-md navbar-dark bg-danger-800" style="background-image:url('{{ url('/themes/limitless/global/images/bg.png') }}')">
	<div class="navbar-brand wmin-200" style="padding-top: 6px; padding-bottom: 0px">
		<a href="#" class="d-inline-block" style="font-size: 22px; color: #ffffff">
			<strong>MyBTP</strong> Application Center
		</a>
	</div>

	<div class="d-md-none">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
			<i class="icon-tree5"></i>
		</button>
	</div>

	<div class="collapse navbar-collapse" id="navbar-mobile">
		<ul class="navbar-nav">
			<!--<li class="nav-item dropdown">
				<a href="#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown">
					<i class="icon-people"></i>
					<span class="d-md-none ml-2">Users</span>
					<span class="badge badge-mark border-orange-400 ml-auto ml-md-0"></span>
				</a>

				<div class="dropdown-menu dropdown-content wmin-md-300">
					<div class="dropdown-content-header">
						<span class="font-weight-semibold">Users online</span>
						<a href="#" class="text-default"><i class="icon-search4 font-size-base"></i></a>
					</div>

					<div class="dropdown-content-body dropdown-scrollable">
						<ul class="media-list">
							<li class="media">
								<div class="mr-3">
									<img src="../../../../global_assets/images/demo/users/face18.jpg" width="36" height="36" class="rounded-circle" alt="">
								</div>
								<div class="media-body">
									<a href="#" class="media-title font-weight-semibold">Jordana Ansley</a>
									<span class="d-block text-muted font-size-sm">Lead web developer</span>
								</div>
								<div class="ml-3 align-self-center"><span class="badge badge-mark border-success"></span></div>
							</li>

							<li class="media">
								<div class="mr-3">
									<img src="../../../../global_assets/images/demo/users/face24.jpg" width="36" height="36" class="rounded-circle" alt="">
								</div>
								<div class="media-body">
									<a href="#" class="media-title font-weight-semibold">Will Brason</a>
									<span class="d-block text-muted font-size-sm">Marketing manager</span>
								</div>
								<div class="ml-3 align-self-center"><span class="badge badge-mark border-danger"></span></div>
							</li>

							<li class="media">
								<div class="mr-3">
									<img src="../../../../global_assets/images/demo/users/face17.jpg" width="36" height="36" class="rounded-circle" alt="">
								</div>
								<div class="media-body">
									<a href="#" class="media-title font-weight-semibold">Hanna Walden</a>
									<span class="d-block text-muted font-size-sm">Project manager</span>
								</div>
								<div class="ml-3 align-self-center"><span class="badge badge-mark border-success"></span></div>
							</li>

							<li class="media">
								<div class="mr-3">
									<img src="../../../../global_assets/images/demo/users/face19.jpg" width="36" height="36" class="rounded-circle" alt="">
								</div>
								<div class="media-body">
									<a href="#" class="media-title font-weight-semibold">Dori Laperriere</a>
									<span class="d-block text-muted font-size-sm">Business developer</span>
								</div>
								<div class="ml-3 align-self-center"><span class="badge badge-mark border-warning-300"></span></div>
							</li>

							<li class="media">
								<div class="mr-3">
									<img src="../../../../global_assets/images/demo/users/face16.jpg" width="36" height="36" class="rounded-circle" alt="">
								</div>
								<div class="media-body">
									<a href="#" class="media-title font-weight-semibold">Vanessa Aurelius</a>
									<span class="d-block text-muted font-size-sm">UX expert</span>
								</div>
								<div class="ml-3 align-self-center"><span class="badge badge-mark border-grey-400"></span></div>
							</li>
						</ul>
					</div>

					<div class="dropdown-content-footer bg-light">
						<a href="#" class="text-grey mr-auto">All users</a>
						<a href="#" class="text-grey"><i class="icon-gear"></i></a>
					</div>
				</div>
			</li>-->

			<!--<li class="nav-item dropdown">
				<a href="#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown">
					<i class="icon-pulse2"></i>
					<span class="d-md-none ml-2">Activity</span>
					<span class="badge badge-mark border-orange-400 ml-auto ml-md-0"></span>
				</a>

				<div class="dropdown-menu dropdown-content wmin-md-350">
					<div class="dropdown-content-header">
						<span class="font-weight-semibold">Latest activity</span>
						<a href="#" class="text-default"><i class="icon-search4 font-size-base"></i></a>
					</div>

					<div class="dropdown-content-body dropdown-scrollable">
						<ul class="media-list">
							<li class="media">
								<div class="mr-3">
									<a href="#" class="btn bg-success-400 rounded-round btn-icon"><i class="icon-mention"></i></a>
								</div>

								<div class="media-body">
									<a href="#">Taylor Swift</a> mentioned you in a post "Angular JS. Tips and tricks"
									<div class="font-size-sm text-muted mt-1">4 minutes ago</div>
								</div>
							</li>

							<li class="media">
								<div class="mr-3">
									<a href="#" class="btn bg-pink-400 rounded-round btn-icon"><i class="icon-paperplane"></i></a>
								</div>

								<div class="media-body">
									Special offers have been sent to subscribed users by <a href="#">Donna Gordon</a>
									<div class="font-size-sm text-muted mt-1">36 minutes ago</div>
								</div>
							</li>

							<li class="media">
								<div class="mr-3">
									<a href="#" class="btn bg-blue rounded-round btn-icon"><i class="icon-plus3"></i></a>
								</div>

								<div class="media-body">
									<a href="#">Chris Arney</a> created a new <span class="font-weight-semibold">Design</span> branch in <span class="font-weight-semibold">Limitless</span> repository
									<div class="font-size-sm text-muted mt-1">2 hours ago</div>
								</div>
							</li>

							<li class="media">
								<div class="mr-3">
									<a href="#" class="btn bg-purple-300 rounded-round btn-icon"><i class="icon-truck"></i></a>
								</div>

								<div class="media-body">
									Shipping cost to the Netherlands has been reduced, database updated
									<div class="font-size-sm text-muted mt-1">Feb 8, 11:30</div>
								</div>
							</li>

							<li class="media">
								<div class="mr-3">
									<a href="#" class="btn bg-warning-400 rounded-round btn-icon"><i class="icon-comment"></i></a>
								</div>

								<div class="media-body">
									New review received on <a href="#">Server side integration</a> services
									<div class="font-size-sm text-muted mt-1">Feb 2, 10:20</div>
								</div>
							</li>

							<li class="media">
								<div class="mr-3">
									<a href="#" class="btn bg-teal-400 rounded-round btn-icon"><i class="icon-spinner11"></i></a>
								</div>

								<div class="media-body">
									<strong>January, 2018</strong> - 1320 new users, 3284 orders, $49,390 revenue
									<div class="font-size-sm text-muted mt-1">Feb 1, 05:46</div>
								</div>
							</li>
						</ul>
					</div>

					<div class="dropdown-content-footer bg-light">
						<a href="#" class="text-grey mr-auto">All activity</a>
						<div>
							<a href="#" class="text-grey" data-popup="tooltip" title="Clear list"><i class="icon-checkmark3"></i></a>
							<a href="#" class="text-grey ml-2" data-popup="tooltip" title="Settings"><i class="icon-gear"></i></a>
						</div>
					</div>
				</div>
			</li>-->
		</ul>

		<span class="badge bg-success-400 badge-pill ml-md-3 mr-md-auto">Online</span>

		<ul class="navbar-nav">
			<li class="nav-item dropdown">
				<a href="#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown">
					<i class="icon-bubbles5"></i>
					<span class="d-md-none ml-2">Messages</span>
					<span class="badge badge-pill badge-mark bg-orange-400 border-orange-400 ml-auto ml-md-0"></span>
				</a>

				<div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
					<div class="dropdown-content-header">
						<span class="font-weight-semibold">Messages</span>
						<a href="#" class="text-default"><i class="icon-compose"></i></a>
					</div>

					<div class="dropdown-content-body dropdown-scrollable">
						<ul class="media-list">
							<li class="media">
								<div class="mr-3 position-relative">
									<img src="../../../../global_assets/images/placeholders/placeholder.jpg" width="36" height="36" class="rounded-circle" alt="">
								</div>

								<div class="media-body">
									<div class="media-title">
										<a href="#">
											<span class="font-weight-semibold">James Alexander</span>
											<span class="text-muted float-right font-size-sm">04:58</span>
										</a>
									</div>

									<span class="text-muted">who knows, maybe that would be the best thing for me...</span>
								</div>
							</li>

							<li class="media">
								<div class="mr-3 position-relative">
									<img src="../../../../global_assets/images/placeholders/placeholder.jpg" width="36" height="36" class="rounded-circle" alt="">
								</div>

								<div class="media-body">
									<div class="media-title">
										<a href="#">
											<span class="font-weight-semibold">Margo Baker</span>
											<span class="text-muted float-right font-size-sm">12:16</span>
										</a>
									</div>

									<span class="text-muted">That was something he was unable to do because...</span>
								</div>
							</li>

							<li class="media">
								<div class="mr-3">
									<img src="../../../../global_assets/images/placeholders/placeholder.jpg" width="36" height="36" class="rounded-circle" alt="">
								</div>
								<div class="media-body">
									<div class="media-title">
										<a href="#">
											<span class="font-weight-semibold">Jeremy Victorino</span>
											<span class="text-muted float-right font-size-sm">22:48</span>
										</a>
									</div>

									<span class="text-muted">But that would be extremely strained and suspicious...</span>
								</div>
							</li>

							<li class="media">
								<div class="mr-3">
									<img src="../../../../global_assets/images/placeholders/placeholder.jpg" width="36" height="36" class="rounded-circle" alt="">
								</div>
								<div class="media-body">
									<div class="media-title">
										<a href="#">
											<span class="font-weight-semibold">Beatrix Diaz</span>
											<span class="text-muted float-right font-size-sm">Tue</span>
										</a>
									</div>

									<span class="text-muted">What a strenuous career it is that I've chosen...</span>
								</div>
							</li>

							<li class="media">
								<div class="mr-3">
									<img src="../../../../global_assets/images/placeholders/placeholder.jpg" width="36" height="36" class="rounded-circle" alt="">
								</div>
								<div class="media-body">
									<div class="media-title">
										<a href="#">
											<span class="font-weight-semibold">Richard Vango</span>
											<span class="text-muted float-right font-size-sm">Mon</span>
										</a>
									</div>

									<span class="text-muted">Other travelling salesmen live a life of luxury...</span>
								</div>
							</li>
						</ul>
					</div>

					<div class="dropdown-content-footer justify-content-center p-0">
						<a href="#" class="bg-light text-grey w-100 py-2" data-popup="tooltip" title="Load more"><i class="icon-menu7 d-block top-0"></i></a>
					</div>
				</div>
			</li>

			<li class="nav-item dropdown">
				<a href="#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown">
					<i class="icon-bell2"></i>
					<span class="d-md-none ml-2">Activity</span>
					<span class="badge badge-pill badge-mark bg-orange-400 border-orange-400 ml-auto ml-md-0"></span>
				</a>

				<div class="dropdown-menu dropdown-content wmin-md-350">
					<div class="dropdown-content-header">
						<span class="font-weight-semibold">Latest activity</span>
						<a href="#" class="text-default"><i class="icon-search4 font-size-base"></i></a>
					</div>

					<div class="dropdown-content-body dropdown-scrollable">
						<ul class="media-list">
							<li class="media">
								<div class="mr-3">
									<a href="#" class="btn bg-success-400 rounded-round btn-icon"><i class="icon-mention"></i></a>
								</div>

								<div class="media-body">
									<a href="#">Taylor Swift</a> mentioned you in a post "Angular JS. Tips and tricks"
									<div class="font-size-sm text-muted mt-1">4 minutes ago</div>
								</div>
							</li>

							<li class="media">
								<div class="mr-3">
									<a href="#" class="btn bg-pink-400 rounded-round btn-icon"><i class="icon-paperplane"></i></a>
								</div>

								<div class="media-body">
									Special offers have been sent to subscribed users by <a href="#">Donna Gordon</a>
									<div class="font-size-sm text-muted mt-1">36 minutes ago</div>
								</div>
							</li>

							<li class="media">
								<div class="mr-3">
									<a href="#" class="btn bg-blue rounded-round btn-icon"><i class="icon-plus3"></i></a>
								</div>

								<div class="media-body">
									<a href="#">Chris Arney</a> created a new <span class="font-weight-semibold">Design</span> branch in <span class="font-weight-semibold">Limitless</span> repository
									<div class="font-size-sm text-muted mt-1">2 hours ago</div>
								</div>
							</li>

							<li class="media">
								<div class="mr-3">
									<a href="#" class="btn bg-purple-300 rounded-round btn-icon"><i class="icon-truck"></i></a>
								</div>

								<div class="media-body">
									Shipping cost to the Netherlands has been reduced, database updated
									<div class="font-size-sm text-muted mt-1">Feb 8, 11:30</div>
								</div>
							</li>

							<li class="media">
								<div class="mr-3">
									<a href="#" class="btn bg-warning-400 rounded-round btn-icon"><i class="icon-comment"></i></a>
								</div>

								<div class="media-body">
									New review received on <a href="#">Server side integration</a> services
									<div class="font-size-sm text-muted mt-1">Feb 2, 10:20</div>
								</div>
							</li>

							<li class="media">
								<div class="mr-3">
									<a href="#" class="btn bg-teal-400 rounded-round btn-icon"><i class="icon-spinner11"></i></a>
								</div>

								<div class="media-body">
									<strong>January, 2018</strong> - 1320 new users, 3284 orders, $49,390 revenue
									<div class="font-size-sm text-muted mt-1">Feb 1, 05:46</div>
								</div>
							</li>
						</ul>
					</div>

					<div class="dropdown-content-footer bg-light">
						<a href="#" class="text-grey mr-auto">All activity</a>
						<div>
							<a href="#" class="text-grey" data-popup="tooltip" title="Clear list"><i class="icon-checkmark3"></i></a>
							<a href="#" class="text-grey ml-2" data-popup="tooltip" title="Settings"><i class="icon-gear"></i></a>
						</div>
					</div>
				</div>
			</li>

			<li class="nav-item dropdown dropdown-user">
				<a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
					<img src="http://demo.interface.club/limitless/demo/Template/global_assets/images/demo/users/face11.jpg" class="rounded-circle mr-2" height="34" alt="">
					<span>Victoria</span>
				</a>

				<div class="dropdown-menu dropdown-menu-right">
					<a href="#" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
					<a href="#" class="dropdown-item"><i class="icon-coins"></i> My balance</a>
					<a href="#" class="dropdown-item"><i class="icon-comment-discussion"></i> Messages <span class="badge badge-pill bg-blue ml-auto">58</span></a>
					<div class="dropdown-divider"></div>
					<a href="#" class="dropdown-item"><i class="icon-cog5"></i> Account settings</a>
					<a href="{{ url('login/logout') }}" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
				</div>
			</li>
		</ul>
	</div>
</div>
<!-- /main navbar -->

<!-- Secondary navbar -->
<div class="navbar navbar-expand-md navbar-light">
	<div class="text-center d-md-none w-100">
		<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-navigation">
			<i class="icon-unfold mr-2"></i>
			Navigation
		</button>
	</div>

	<div class="navbar-collapse collapse" id="navbar-navigation">
		<ul class="navbar-nav">
			<li class="nav-item">
				<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
					<i class="icon-home4 mr-2"></i> Dashboard
				</a>

				<div class="dropdown-menu">
					<a href="{{ url('data/hki') }}" class="dropdown-item"><i class="icon-file-spreadsheet"></i> Data HKI</a>
					<a href="{{ url('data/inkubasi') }}" class="dropdown-item"><i class="icon-file-spreadsheet"></i> Data Inkubasi</a>
					<a href="{{ url('data/okupansi') }}" class="dropdown-item"><i class="icon-file-spreadsheet"></i> Data Tenant</a>
					<a href="#" class="dropdown-item"><i class="icon-file-spreadsheet"></i> Data Segment Pasar</a>
					<a href="#" class="dropdown-item"><i class="icon-file-spreadsheet"></i> Data Project</a>
				</div>
			</li>
			<li class="nav-item dropdown">
				<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
					<i class="icon-brain mr-2"></i> Expert Management
				</a>

				<div class="dropdown-menu">
					<a href="pmo/cat_project" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Master Kategori Project</a>
					<a href="{{ url('em/expert') }}" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Master Data Expert</a>
				</div>
			</li>
			<li class="nav-item dropdown">
				<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
					<i class="icon-rocket mr-2"></i> Inkubasi Center
				</a>

				<div class="dropdown-menu">
					<a href="{{ url('ic/dat_expert') }}" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Master Data Expert</a>
				</div>
			</li>
			<li class="nav-item dropdown">
				<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
					<i class="icon-clipboard6 mr-2"></i> Project Management
				</a>

				<div class="dropdown-menu">
					<a href="#" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Master Data Expert</a>
				</div>
			</li>
			<li class="nav-item dropdown">
				<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
					<i class="icon-stats-growth mr-2"></i> KM
				</a>

				<div class="dropdown-menu">
					<a href="#" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Master Data Expert</a>
				</div>
			</li>

			<li class="nav-item dropdown">
				<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
					<i class="icon-cube mr-2"></i> GA
				</a>

				<div class="dropdown-menu">
					<a href="/ga/okupansi" class="dropdown-item"><i class="icon-align-center-horizontal"></i> Okupansi</a>
				</div>
			</li>
		</ul>
	</div>
</div>
<!-- /Secondary navbar -->


<!-- Page container -->
<div class="page-header">
	<div class="page-header-content header-elements-md-inline">
		<div class="page-title d-flex">
			<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">@yield('page_title')</span></h4>
			<a href="" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

		<div class="header-elements d-none py-0 mb-3 mb-md-0">
			<div class="breadcrumb">
				<a href="" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
				<span class="breadcrumb-item active">@yield('page_title')</span>
			</div>
		</div>
	</div>
</div>

<div class="page-content pt-0">

	<!-- Main content -->
	<div class="content-wrapper">

		<!-- Content area -->
		<div class="content">

			@yield('content')

		</div>
	</div>
</div>
<!-- /page container -->

<!-- Footer -->
<div class="navbar navbar-expand-lg navbar-light">
	<div class="text-center d-lg-none w-100">
		<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
			<i class="icon-unfold mr-2"></i>
			Footer
		</button>
	</div>

	<div class="navbar-collapse collapse" id="navbar-footer">
		<span class="navbar-text">
			&copy; {{ date('Y')-1 }} - {{ date('Y') }}. <span class="text-primary font-weight-semibold">MyBTP Application Center</span> by BTP
		</span>

		<ul class="navbar-nav ml-lg-auto">
			<li class="nav-item"><a href="" class="navbar-nav-link"><i class="icon-lifebuoy mr-2"></i> Support</a></li>
		</ul>
	</div>
</div>
<!-- /footer -->

<!-- Core JS files -->
<script type="text/javascript" src="{{url('/themes/limitless/global/js/main/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/main/bootstrap.bundle.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/loaders/blockui.min.js')}}"></script>
<!-- Core JS files -->

<script type="text/javascript" src="{{url('/themes/limitless/layout_4/js/app.js')}}"></script>

<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/tables/datatables/extensions/jszip/jszip.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/tables/datatables/extensions/buttons/dataTables.buttons.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/tables/datatables/extensions/buttons/buttons.html5.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/forms/validation/validate.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/forms/validation/additional_methods.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/notifications/noty.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/setting.js')}}"></script>

@yield('js_section')

</body>
</html>