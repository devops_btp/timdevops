<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- end CSRF Token -->
	
	<title>XGracias</title>

    <link rel="icon" type="image/png" href="{{url('/images/xgracias-favicon.png')}}" />

	<!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/icons/icomoon/styles.css')}}">
	
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/login.css').'?v='.\Carbon::now()->timestamp}}">



    <style>
        body {
            background-image:url('{{url('/images/bg-login-2.jpg')}}');
            background-color: #FFFFFF;
        }
        body.login .login-sidebar {
            border-top:5px solid #C62828;
        }
        @media (max-width: 767px) {
            body.login .login-sidebar {
                border-top:0px !important;
                border-left:5px solid #C62828;
            }
        }
        body.login .form-group-default.focused{
            border-color:#C62828;
        }
        .login-button, .bar:before, .bar:after{
            background:#C62828;
            margin-right: 20px;
        }  

        .register{
            background-color : #1976D2;
        }
    </style>
</head>

<body class="login">
<div class="container-fluid">
    <div class="row"> 
    home
    </div> <!-- .row -->
</div> <!-- .container-fluid -->
<script>
     

</script>
</body>
</html>