<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- end CSRF Token -->
	<?php $locale = App::getLocale(); $title = session()->has('cpart') ? session()->get('cpart')->$locale : ''; ?>
	<title>{{ $title }}</title>

	<link rel="icon" type="image/png" href="{{url('/images/xgracias-favicon.png')}}" />

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/paper.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/print.css')}}">

	<style>@page { size: A4 landscape }</style>
</head>

<body class="A4 landscape">
	@yield('content')
</body>
</html>