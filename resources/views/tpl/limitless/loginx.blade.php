






<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- end CSRF Token -->

	<title>XGracias Best Unit Award - Yayasan Pendidikan Telkom</title>

    <link rel="icon" type="image/png" href="{{url('/images/xgracias-favicon.png')}}" />

	<!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/icons/icomoon/styles.css')}}">

	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/login.css').'?v='.\Carbon::now()->timestamp}}">



    <style>
        body {
            background-image:url('https://igracias.ittelkom-sby.ac.id/environment/itts/images/background.jpg');
            background-color: #FFFFFF;
        }
        body.login .login-sidebar {
            border-top:5px solid #C62828;
        }
        @media (max-width: 767px) {
            body.login .login-sidebar {
                border-top:0px !important;
                border-left:5px solid #C62828;
            }
        }
        body.login .form-group-default.focused{
            border-color:#C62828;
        }
        .login-button, .bar:before, .bar:after{
            background:#C62828;
            margin-right: 20px;
        }

        .register{
            background-color : #1976D2;
        }
    </style>
</head>

<body class="login">
<div class="container-fluid">
    <div class="row">
        <div class="faded-bg animated"></div>
        <div class="hidden-xs col-sm-7 col-md-8">
            <div class="clearfix">
                <div class="col-sm-12 col-md-10 col-md-offset-2">
                    <div class="logo-title-container">
                        <img class="img-responsive pull-left flip logo hidden-xs animated fadeIn" src="{{url('icons/itts.jpg')}}" alt="XGracias System">
                        <div class="copy animated fadeIn">
                            <h1>XGracias Best Unit Award YPT</h1>
                            <p>XGracias Best Unit Award - Yayasan Pendidikan Telkom</p>
                        </div>
                    </div> <!-- .logo-title-container -->
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-5 col-md-4 login-sidebar">

            <div class="login-container">

                <p>Sign In Below:</p>

                <form class="login-form" method="post" action="{{url($action)}}">
                    {{csrf_field()}}
                    <div class="form-group form-group-default" id="usernameGroup">
                        <label>Username</label>
                        <div class="controls">
                            <input type="text" name="username" id="username" value="" placeholder="Username" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group form-group-default" id="passwordGroup">
                        <label>Password</label>
                        <div class="controls">
                            <input type="password" name="password" placeholder="Password" class="form-control" required>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-block login-button">
                        <span class="signingin hidden"><span class="voyager-refresh"></span> Logging in...</span>
                        <span class="signin">Login &nbsp; <i class="icon-circle-right2"></i></span>
                    </button>

                    <a type="button" class="btn btn-block login-button register" href="register">
                        <span class="signin">Register &nbsp; <i class="icon-circle-right2"></i></span>
                    </a>

                </form>

                <div style="clear:both"></div>

                @if (session()->has('alert'))
                <div class="alert alert-red">
                    <ul class="list-unstyled">
                        <li><span class="text-semibold">{{ session()->get('alert') }}</span></li>
                    </ul>
                </div>
                @endif

            </div> <!-- .login-container -->

        </div> <!-- .login-sidebar -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->
<script>
    var btn = document.querySelector('button[type="submit"]');
    var form = document.forms[0];
    var email = document.querySelector('[name="username"]');
    var password = document.querySelector('[name="password"]');
    btn.addEventListener('click', function(ev){
        if (form.checkValidity()) {
            btn.querySelector('.signingin').className = 'signingin';
            btn.querySelector('.signin').className = 'signin hidden';
        } else {
            ev.preventDefault();
        }
    });
    email.focus();
    document.getElementById('usernameGroup').classList.add("focused");

    // Focus events for email and password fields
    email.addEventListener('focusin', function(e){
        document.getElementById('usernameGroup').classList.add("focused");
    });
    email.addEventListener('focusout', function(e){
        document.getElementById('usernameGroup').classList.remove("focused");
    });

    password.addEventListener('focusin', function(e){
        document.getElementById('passwordGroup').classList.add("focused");
    });
    password.addEventListener('focusout', function(e){
        document.getElementById('passwordGroup').classList.remove("focused");
    });

</script>
</body>
</html>
