@extends('tpl.limitless.master_layout4')

@section('css_section')

@endsection

@section('page_title')

@endsection

@section('content')
    <div class="mb-3 pt-2">
        <h6 class="mb-0 font-weight-semibold">Data Dashboard</h6>
    </div>

    <div class="row">
        <div class="col-md-2">
            <div class="card">
                <a href="{{ url('data/hki') }}" style="color: inherit">
                    <div class="card-body text-center">
                        <i class="icon-certificate icon-3x text-primary mt-1 mb-3"></i>
                        <h6 class="font-weight-semibold">Dashboard HKI</h6>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-2">
            <div class="card">
                <a href="{{ url('data/inkubasi') }}" style="color: inherit">
                    <div class="card-body text-center">
                        <i class="icon-stats-growth icon-3x text-indigo mt-1 mb-3"></i>
                        <h6 class="font-weight-semibold">Dashboard Inkubasi</h6>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-2">
            <div class="card">
                <a href="{{ url('data/okupansi') }}" style="color: inherit">
                    <div class="card-body text-center">
                        <i class="icon-city icon-3x text-danger-800 mt-1 mb-3"></i>
                        <h6 class="font-weight-semibold">Dashboard Tenant</h6>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-2">
            <div class="card">
                <a href="{{ url('data/segmen') }}" style="color: inherit">
                    <div class="card-body text-center">
                        <i class="icon-cart5 icon-3x text-success mt-1 mb-3"></i>
                        <h6 class="font-weight-semibold">Dashboard Segment Pasar</h6>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-2">
            <div class="card">
                <a href="{{ url('data/project') }}" style="color: inherit">
                    <div class="card-body text-center">
                        <i class="icon-stats-dots icon-3x text-purple-600 mt-1 mb-3"></i>
                        <h6 class="font-weight-semibold">Dashboard Project</h6>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-2">
            <div class="card">
                <a href="{{ url('data/docs_legal') }}" style="color: inherit">
                    <div class="card-body text-center">
                        <i class="icon-file-pdf icon-3x text-danger mt-1 mb-3"></i>
                        <h6 class="font-weight-semibold text-semibold">Dashboard Documents</h6>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <div class="mb-3 pt-2">
        <h6 class="mb-0 font-weight-semibold">Application</h6>
    </div>

    <div class="row">
        <div class="col-md-2">
            <div class="card">
                <a href="{{ url('km') }}" style="color: inherit">
                    <div class="card-body text-center">
                        <i class="icon-stats-bars icon-3x text-primary mt-1 mb-3"></i>
                        <h6 class="font-weight-semibold">Monitoring KM</h6>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-2">
            <div class="card">
                <a href="#" style="color: inherit">
                    <div class="card-body text-center">
                        <i class="icon-cart5 icon-3x text-muted mt-1 mb-3"></i>
                        <h6 class="font-weight-semibold text-muted">Data Projects</h6>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-2">
            <div class="card">
                <a href="#" style="color: inherit">
                    <div class="card-body text-center">
                        <i class="icon-cart-add2 icon-3x text-muted mt-1 mb-3"></i>
                        <h6 class="font-weight-semibold text-muted">Data Problem Pasar</h6>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-2">
            <div class="card">
                <a href="#" style="color: inherit">
                    <div class="card-body text-center">
                        <i class="icon-user-tie icon-3x text-muted mt-1 mb-3"></i>
                        <h6 class="font-weight-semibold text-muted">Data Expert</h6>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-2">
            <div class="card">
                <a href="#" style="color: inherit">
                    <div class="card-body text-center">
                        <i class="icon-users icon-3x text-muted mt-1 mb-3"></i>
                        <h6 class="font-weight-semibold text-muted">Data Client</h6>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-2">
            <div class="card">
                <a href="#" style="color: inherit">
                    <div class="card-body text-center">
                        <i class="icon-rocket icon-3x text-muted mt-1 mb-3"></i>
                        <h6 class="font-weight-semibold text-muted">Data Startup</h6>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-2">
            <div class="card">
                <a href="#" style="color: inherit">
                    <div class="card-body text-center">
                        <i class="icon-bubbles4 icon-3x text-muted mt-1 mb-3"></i>
                        <h6 class="font-weight-semibold text-muted">Helpdesk</h6>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-2">
            <div class="card">
                <a href="#" style="color: inherit">
                    <div class="card-body text-center">
                        <i class="icon-stars icon-3x text-muted mt-1 mb-3"></i>
                        <h6 class="font-weight-semibold text-muted">Feedback</h6>
                    </div>
                </a>
            </div>
        </div>
    </div>
@endsection

@section('js_section')

@endsection