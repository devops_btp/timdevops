@extends('tpl.limitless.master_layout4')

@section('css_section')

@endsection

@section('page_title')
Kinerja Manajerial (KM)
@endsection

@section('content')
<div class="card">
    <div class="card-header bg-white">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        {{ Form::select('group-filter-select', $year, $var_year, ['class' => 'form-control select2 form-control-sm', 'id' => 'years']) }}
                    </div>
                    <div class="col-md-8">
                        @for($t=1; $t<=4; $t++)
                        <button class="btn {{ $t == $var_tw ? 'btn-success' : 'btn-light' }} btn-md" onclick="link_tw({{ $t }})">TW {{ $t }}</button>
                        @endfor
                    </div>
                </div>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ $km->km_url_edit ?? '#' }}" class="btn bg-pink btn-sm" target="_blank"><i class="icon-pen2 mr-1"></i> Edit KM</a>
                <a href="{{ $km->km_url_evidence ?? '#' }}" class="btn bg-indigo btn-sm" target="_blank"><i class="icon-file-text mr-1"></i> Evidence KM</a>

                <a href="{{ $km->km_url_doc ?? '#' }}" class="btn {{ empty($km->km_url_doc) ? 'bg-grey' : 'bg-primary' }} btn-sm" target="_blank"><i class="icon-file-pdf mr-1"></i> Document KM</a>
            </div>
        </div>
    </div>

    @if(empty($km->km_url_view))
        <div class="card-body">
            <div class="alert alert-danger">Template Dokumen KM Belum Tersedia</div>
        </div>
    @else
        <iframe src="{{ $km->km_url_view }}" style="width: 100%; height: 450px" frameborder="0"></iframe>
    @endif
</div>

@endsection

@section('js_section')
<script>
    /*var iframe = document.getElementsByTagName('iframe')[0];
    iframe.addEventListener("load", function() {
        var node = document.createElement('style');
        node.appendChild(document.createTextNode('#top-bar {display:none;}'));
        window.frames[0].document.head.appendChild(node);

        // the cleaner and simpler way
        //window.frames[0].document.body.style.backgroundColor = "#fff";
    });*/

    $(function() {
        $('.select2').select2({
            minimumResultsForSearch: Infinity
        });

        $('#years').change(function() {
            window.location.href = '{{ url('km') }}'+'/'+$(this).val()+'/'+'{{ $var_tw }}';
        });
    });

    function link_tw (tw)
    {
        window.location.href = '{{ url('km/'.$var_year) }}'+'/'+tw;
    }
    /*$('iframe').load( function() {
        $('iframe').contents().find("head")
            .append($("<style type='text/css'>  #top-bar {display:none;}  </style>"));
    });

    $(function(){
        $('iframe').load( function() {
            $('iframe').contents().find("head")
                .append($("<style type='text/css'>  #top-bar {display:none;}  </style>"));
        });
        dTable =  $('#table').DataTable({
            ajax: {
                url: '{{ url("pmo/cat_project/dt") }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: [
                { data: 'cp_name', name: 'cp_name', orderable: true },
                { data: 'action', name: 'action', orderable: false, searchable: false, className: 'center'}
            ],
            "order": [['0', 'asc']]
        });

        $('.select2').select2();

        $('.datepicker').datepicker({
            dateFormat : 'yy-mm-dd'
        });

        $('.form-check-input-styled').uniform();

        $('.dataTables_filter input[type=search]').attr('placeholder','Pencarian');

        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });

        $('.dt-buttons').append('<button class="btn btn-light bg-white" onclick="add()"><i class="icon-add"></i> <span class="d-none d-lg-inline-block ml-2">Tambah Data</span></button>');
    });

    function add()
    {
        _reset();
        $('.form-check-input-styled').prop('checked',false).uniform('refresh');
        $('#act-save').show();
        $('#act-update').hide();
        $('#frmbox').modal({keyboard: false, backdrop: 'static'});
    }

    function save(url)
    {
        if($("#frm").valid())
        {
            var formData = new FormData($('#frm')[0]);

            $.ajax({
                url:'{{ url("pmo/cat_project") }}'+'/'+url,
                type:'post',
                data: formData,
                contentType: false,//untuk upload image
                processData: false,//untuk upload image
                timeout: 300000, // sets timeout to 3 seconds
                dataType:'json',
                success : function(e) {
                    if(e.status == 'ok;') {
                        new Noty({
                            text: 'Berhasil Menyimpan Data',
                            type: 'success'
                        }).show();

                        dTable.draw();
                        $("#frmbox").modal('hide');
                    } else {
                        alert(e.text);
                    }
                }
            });
        }
    }

    function edit(id)
    {
        $.ajax({
            url:'{{ url("pmo/cat_project/edit") }}',
            type:'post',
            dataType:'json',
            data: ({ id : id }),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(e) {
                _reset();
                $('#act-save').hide();
                $('#act-update').show();
                $('#id').val(id);
                $.each(e, function(key, value) {
                    if($('#'+key).hasClass("select2")) {
                        $('#'+key).val(value).trigger('change');
                    }
                    else if($('input[type=radio]').hasClass(key)){
                        if(value!=""){
                            $("input[name='inp["+key+"]'][value='" + value + "']").prop('checked', true);
                            $.uniform.update();
                        }
                    }
                    else if($('input[type=checkbox]').hasClass(key)){
                        if(value!=null){
                            var temp = value.split('; ');
                            for (var i = 0; i < temp.length; i++) {
                                $("input[name='inp["+key+"][]'][value='" + temp[i] + "']").prop('checked', true);
                            }
                            $.uniform.update();
                        }
                    }
                    else  $('#'+key).val(value);
                });
                $('#frmbox').modal({keyboard: false, backdrop: 'static'});
            }
        });
    }

    function del(id, txt)
    {
        if(confirm('Data: '+txt+'\n'+'Apakah anda yakin akan menghapus data ini?')) {
        $.ajax({
            url:'{{ url("pmo/cat_project/delete") }}',
            type:'delete',
            dataType:'json',
            data: ({id : id }),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(e) {
                if(e.status == 'ok;') {
                    new Noty({
                        text: 'Berhasil Menghapus Data',
                        type: 'success'
                    }).show();

                    dTable.draw();
                } else {
                    alert(e.text);
                }
            }
        });
    }
    }*/
</script>
@endsection