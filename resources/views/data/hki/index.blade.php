@extends('tpl.limitless.master_layout4')

@section('css_section')

@endsection

@section('page_title')
    Data Hak Kekayaan Intelektual (HKI)
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div id="grafik"></div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div id="grafik2"></div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div id="grafik3"></div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-md-6 offset-3">
                <div class="row">
                @foreach($rj_unit as $ru)
                <div class="col-md-6"><strong>{{ $ru }}</strong>{{ ' : '.config('ref.jns_ki.'.$ru)  }}</div>
                @endforeach
                </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h4 class="text-center">Jumlah Perolehan Kekayaan Intelektual (KI) per Tahun - Berdasarkan Fakultas dan Unit</h4>
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Fakultas</th>
                @foreach($rekap_tahun as $th)
                    <th class="text-center">Tahun {{ $th }}</th>
                @endforeach
            </tr>
            </thead>
            <tbody>
            @foreach($rekap_unit as $u)
                <tr>
                    <td style="white-space:nowrap"><strong>{{ strtoupper($u) }}</strong></td>
                    @foreach($rekap_tahun as $th)
                        <td class="text-center">{{ !empty($rekap[$th][$u]) ? array_sum($rekap[$th][$u]) : '0' }}</td>
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="card">
        <div class="card-header">
            <h4 class="text-center">Jumlah Perolehan Kekayaan Intelektual (KI) per Tahun - Berdasarkan Jenis KI</h4>
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Jenis KI</th>
                @foreach($rj_tahun as $thn)
                    <th class="font-weight-bold text-center">Tahun {{ $thn }}</th>
                @endforeach
            </tr>
            </thead>
            <tbody>

            @foreach($rj_unit as $unit)
                <tr>
                    <td class="font-weight-bold">{{ config('ref.jns_ki.'.$unit) }} ({{ $unit }})</td>
                    @foreach($rj_tahun as $thn)
                        <td class="text-center">{{$rekap_jenis[$thn][$unit] }}</td>
                    @endforeach
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>

    <div class="card">
        <div class="card-header">
            <h4 class="text-center">Rekapitulasi Service Level Agreement (SLA) Kekayaan Intelektual (KI) Per Tahun</h4>
        </div>
        <table class="table table-striped table-responsive">
            <thead>
            <tr>
                <th rowspan="2">Fakultas</th>
                @foreach($rekap_tahun as $th)
                <th colspan="3" class="text-center">Tahun {{ $th }}</th>
                @endforeach
            </tr>
            <tr>
                @foreach($rekap_tahun as $th)
                <th class="text-center">Usulan</th>
                <th class="text-center">Proses</th>
                <th class="text-center" >Selesai</th>
                @endforeach
            </tr>
            </thead>
            <tbody>
            @foreach($rekap_unit as $u)
                <tr>
                    <td style="white-space:nowrap"><strong>{{ strtoupper($u) }}</strong></td>
                    @foreach($rekap_tahun as $th)
                        <td class="text-center">{{ $rekap[$th][$u]['usulan'] ?? '0' }}</td>
                        <td class="text-center">{{ $rekap[$th][$u]['proses'] ?? '0' }}</td>
                        <td class="text-center">{{ $rekap[$th][$u]['selesai'] ?? '0' }}</td>
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('js_section')
    <script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/uploaders/dropzone.min.js')}}"></script>
    <script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/interactions.min.js')}}"></script>
    <script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/widgets.min.js')}}"></script>
    <script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/effects.min.js')}}"></script>
    <script src="{{url('/themes/limitless/global/js/plugins/extensions/mousewheel.min.js')}}"></script>
    <script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/globalize/globalize.js')}}"></script>
    <script src="{{url('/highcharts.js')}}"></script>

    <script>
       // console.log({!! json_encode($rekap_tahun) !!});
        Highcharts.chart('grafik',{
            chart: {
                type: 'column'
            },
            xAxis: [{
                categories : {!! json_encode($rekap_tahun) !!},
                crosshair :true
            }],
            title: {
                text: 'Jumlah Perolehan KI per Tahun'
            },
            plotOptions: {
                column: {
                    dataLabels: {
                        enabled: true,
                        style: {
                            textOutline: 0
                        }
                    },
                }
            },
            yAxis: [
                {
                    title: {
                        text: 'Jumlah'
                    }
                }],
            series: [
                {
                    name: 'Jumlah HKI',
                    showInLegend: false,
                    color: '#3f51b5',
                    data : {!! $chart_year !!}
                }]
        });
    </script>
    <script>

        Highcharts.chart('grafik2',{
            chart: {
                type: 'column'
            },
            xAxis: [{
                categories : {!! json_encode($rekap_unit) !!},
                crosshair :true
            }],
            title: {
                text: 'Jumlah Perolehan KI per Tahun Berdasarkan Fakultas dan Unit'
            },
            plotOptions: {
                column: {
                    dataLabels: {
                        enabled: true,
                        style: {
                            textOutline: 0
                        }
                    },
                }
            },
            yAxis: [
                {
                    title: {
                        text: 'Jumlah'
                    }
                }],
            series: {!! $chart_unit !!}
        });

    </script>
     <script>

        Highcharts.chart('grafik3',{
            chart: {
                type: 'column'
            },
            xAxis: [{
                categories : {!! json_encode($rj_unit) !!},
                crosshair :true
            }],
            title: {
                text: 'Jumlah Perolehan KI per Tahun Berdasarkan Jenis KI'
            },
            plotOptions: {
                column: {
                    dataLabels: {
                        enabled: true,
                        style: {
                            textOutline: 0
                        }
                    },
                }
            },
            yAxis: [
                {
                    title: {
                        text: 'Jumlah'
                    }
                }],
            series: {!! $chart_jenis !!}
        });

    </script>
@endsection