@extends('tpl.limitless.master_layout4')

@section('css_section')

@endsection

@section('page_title')
Data Project
@endsection

@section('content')
<div class="card">
    <div class="card-body">
    <div id="grafik"></div>
</div>
</div>
<div class="card">
    <div class="card-body">
    <div id="grafik2"></div>
</div>
</div>
<div class="card">
    <table class="table table-striped" id="table">
        <thead>
        <tr>
            <th>Year</th>
            <th>Number</th>
            <th>Client</th>
            <th>Contract</th>
            <th>Project</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Price</th>
            <th>PPN</th>
            <th>Price Total</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>


@endsection

@section('js_section')
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/uploaders/dropzone.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/interactions.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/widgets.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/effects.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/mousewheel.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/globalize/globalize.js')}}"></script>
<script src="{{url('/highcharts.js')}}"></script>

        

<script>

    Highcharts.chart('grafik',{
        title: {
            text: 'Jumlah Proyek (Soltek) per Triwulan'
        },
        chart: {
            type: 'spline',
            renderTo: 'linear'
        },

    yAxis: {
        title: {
            text: 'Number of Project'
        }
    },

    xAxis:[{
                    categories : ['TW1','TW2','TW3', 'TW4'], //harusnya nama tahun
                    crosshair :true
           }],
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },



    series: {!! $chart_count !!},

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

    });

    Highcharts.chart('grafik2',{
        title: {
        text: 'Pendapatan Harga (Soltek) per Triwulan, 2019 - 2020'
    },

    yAxis: {
        title: {
            text: 'Number of Project'
        }
    },

        xAxis:[{
            categories : ['TW1','TW2','TW3', 'TW4'], //harusnya nama tahun
            crosshair :true
        }],

    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

        series: {!! $chart_price !!},

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

    });

    $(function(){
        dTable =  $('#table').DataTable({
            ajax: {
                url: '{{ url("data/project/dt") }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: [
                { data: 'rp_year', name: 'rp_year', orderable: true },
                { data: 'rp_number', name: 'rp_number', orderable: true},
                { data: 'rp_client', name: 'rp_client', orderable: true},
                { data: 'rp_contract', name: 'rp_contract', orderable: true},
                { data: 'rp_project', name: 'rp_project', orderable: true},
                { data: 'rp_date_start', name: 'rp_date_start', orderable: true},
                { data: 'rp_date_end', name: 'rp_date_end', orderable: true},
                { data: 'rp_price', name: 'rp_price', orderable: true},
                { data: 'rp_ppn', name: 'rp_ppn', orderable: true},
                { data: 'rp_price_total', name: 'rp_price_total', orderable: true},
                //{ data: 'action', name: 'action', orderable: false, searchable: false, className: 'center'}
            ],
            "order": [['0', 'asc']]
        });

        $('.select2').select2();

        $('.datepicker').datepicker({
            dateFormat : 'yy-mm-dd'
        });

        $('.form-check-input-styled').uniform();

        $('.dataTables_filter input[type=search]').attr('placeholder','Pencarian');

        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });

        //$('.dt-buttons').append('<button class="btn btn-light bg-white" onclick="add()"><i class="icon-add"></i> <span class="d-none d-lg-inline-block ml-2">Tambah Data</span></button>');
    });
</script>
@endsection