@extends('tpl.limitless.master_layout4')

@section('css_section')

@endsection

@section('page_title')
Data marketing (Segmen Pasar)
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div id="grafik"></div>
    </div>
</div>


<div class="card">
    <table class="table table-striped" id="table">
        <thead>
            <tr>
                <th>Year</th>
                <th>Target</th>
                <th>Client</th>
                <th>Job</th>
                <th>Price</th>
                <th>Status</th>
                <th>Product</th>
                <th>Trigger</th>
                <th>AM</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>


@endsection

@section('js_section')
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/uploaders/dropzone.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/interactions.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/widgets.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/effects.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/mousewheel.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/globalize/globalize.js')}}"></script>
<script src="{{url('/highcharts.js')}}"></script>

<script>
    Highcharts.setOptions({
    lang: {
        decimalPoint: ','
    }
});
    Highcharts.chart('grafik', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Segmen Pasar'
    },
    tooltip: {
        pointFormat: '{series.name}: {point.y} Miliyar <b>({point.percentage:.1f}%)</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: ({point.percentage:.1f} %)',
                formatter: function() {
    
                    return Highcharts.numberFormat(this.y, '2',',') +'%'; 

                }        
            }
        }
    },
    series: [{
        name: 'Total',
        colorByPoint: true,
        data: [
            @foreach($grafik as $data)
  {
    name: "{{$data->name}}",
    y: {{$data->y}}
  },
  @endforeach
  
]
    }]
});
    $(function(){
        dTable =  $('#table').DataTable({
            ajax: {
                url: '{{ url("data/segmen/dt") }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: [
                { data: 'rsp_year', name: 'rsp_year', orderable: true },
                { data: 'rsp_target', name: 'rsp_target', orderable: true },
                { data: 'rsp_client', name: 'rsp_client', orderable: true},
                { data: 'rsp_job', name: 'rsp_job', orderable: true},
                { data: 'rsp_price', name: 'rsp_price', orderable: true},
                { data: 'rsp_status', name: 'rsp_status', orderable: true},
                { data: 'rsp_product', name: 'rsp_product', orderable: true},
                { data: 'rsp_trigger', name: 'rsp_trigger', orderable: true},
                { data: 'rsp_am', name: 'rsp_am', orderable: true},

                //{ data: 'action', name: 'action', orderable: false, searchable: false, className: 'center'}
            ],
            "order": [['0', 'asc']]
        });

        $('.select2').select2();

        $('.datepicker').datepicker({
            dateFormat : 'yy-mm-dd'
        });

        $('.form-check-input-styled').uniform();

        $('.dataTables_filter input[type=search]').attr('placeholder','Pencarian');

        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });

        //$('.dt-buttons').append('<button class="btn btn-light bg-white" onclick="add()"><i class="icon-add"></i> <span class="d-none d-lg-inline-block ml-2">Tambah Data</span></button>');
    });
</script>
@endsection