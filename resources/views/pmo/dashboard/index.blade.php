@extends('tpl.limitless.master_layout4')

@section('css_section')

@endsection

@section('page_title')
    Dashboard PMO
@endsection

@section('content')
    <div class="card">
        <table class="table table-striped" id="table">
            <thead>
            <tr>
                <th width="3%">#</th>
                <th width="7%">Category</th>
                <th width="80%">Document</th>
                <th width="10%">&nbsp;</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
@endsection

@section('js_section')
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/uploaders/dropzone.min.js')}}"></script>

<script>
    $(function(){

    });
</script>
@endsection