<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Dashboard PMO</title>

	<link rel="icon" type="image/png" href="{{url('/images/logobtp.png')}}" />

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/icons/icomoon/styles.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/icons/flag-icon/css/flag-icon.min.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/icons/fontawesome/styles.min.css')}}">

	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_3/css/bootstrap.min.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_3/css/bootstrap_limitless.min.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_3/css/layout.min.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_3/css/components.min.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/layout_3/css/colors.css')}}">
	<link type="text/css" rel="stylesheet" href="{{url('/themes/limitless/global/css/custom.css')}}">
	<!-- /global stylesheets -->
	<style>
		.stretched-link::after {
	position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1;
    pointer-events: auto;
    content: "";
    background-color: rgba(0,0,0,0);
}
		</style>
	@yield('css_section')
</head>

<body>

	<!-- Main navbar -->
	<div class="navbar navbar-expand-md navbar-dark bg-danger-800" style="background-image:url('{{ url('/themes/limitless/global/images/bg.png') }}')">
		<div class="navbar-brand wmin-200">
			<a href="index.html" class="d-inline-block">
				MyBTP
			</a>
		</div>

		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-tree5"></i>
			</button>
			<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
				<i class="icon-paragraph-justify3"></i>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="navbar-mobile">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
						<i class="icon-paragraph-justify3"></i>
					</a>
				</li>

				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown">
						<i class="icon-bell2"></i>
						<span class="d-md-none ml-2">Activity</span>
						<span class="badge badge-pill badge-mark bg-orange-400 border-orange-400 ml-auto ml-md-0"></span>
					</a>
					
					<div class="dropdown-menu dropdown-content wmin-md-350">
						<div class="dropdown-content-header">
							<span class="font-weight-semibold">Latest activity</span>
							<a href="#" class="text-default"><i class="icon-search4 font-size-base"></i></a>
						</div>

						<div class="dropdown-content-body dropdown-scrollable">
							<ul class="media-list">
								<li class="media">
									<div class="mr-3">
										<a href="#" class="btn bg-success-400 rounded-round btn-icon"><i class="icon-mention"></i></a>
									</div>

									<div class="media-body">
										<a href="#">Taylor Swift</a> mentioned you in a post "Angular JS. Tips and tricks"
										<div class="font-size-sm text-muted mt-1">4 minutes ago</div>
									</div>
								</li>

								<li class="media">
									<div class="mr-3">
										<a href="#" class="btn bg-pink-400 rounded-round btn-icon"><i class="icon-paperplane"></i></a>
									</div>
									
									<div class="media-body">
										Special offers have been sent to subscribed users by <a href="#">Donna Gordon</a>
										<div class="font-size-sm text-muted mt-1">36 minutes ago</div>
									</div>
								</li>

								<li class="media">
									<div class="mr-3">
										<a href="#" class="btn bg-blue rounded-round btn-icon"><i class="icon-plus3"></i></a>
									</div>
									
									<div class="media-body">
										<a href="#">Chris Arney</a> created a new <span class="font-weight-semibold">Design</span> branch in <span class="font-weight-semibold">Limitless</span> repository
										<div class="font-size-sm text-muted mt-1">2 hours ago</div>
									</div>
								</li>

								<li class="media">
									<div class="mr-3">
										<a href="#" class="btn bg-purple-300 rounded-round btn-icon"><i class="icon-truck"></i></a>
									</div>
									
									<div class="media-body">
										Shipping cost to the Netherlands has been reduced, database updated
										<div class="font-size-sm text-muted mt-1">Feb 8, 11:30</div>
									</div>
								</li>

								<li class="media">
									<div class="mr-3">
										<a href="#" class="btn bg-warning-400 rounded-round btn-icon"><i class="icon-comment"></i></a>
									</div>
									
									<div class="media-body">
										New review received on <a href="#">Server side integration</a> services
										<div class="font-size-sm text-muted mt-1">Feb 2, 10:20</div>
									</div>
								</li>

								<li class="media">
									<div class="mr-3">
										<a href="#" class="btn bg-teal-400 rounded-round btn-icon"><i class="icon-spinner11"></i></a>
									</div>
									
									<div class="media-body">
										<strong>January, 2018</strong> - 1320 new users, 3284 orders, $49,390 revenue
										<div class="font-size-sm text-muted mt-1">Feb 1, 05:46</div>
									</div>
								</li>
							</ul>
						</div>

						<div class="dropdown-content-footer bg-light">
							<a href="#" class="text-grey mr-auto">All activity</a>
							<div>
								<a href="#" class="text-grey" data-popup="tooltip" title="Clear list"><i class="icon-checkmark3"></i></a>
								<a href="#" class="text-grey ml-2" data-popup="tooltip" title="Settings"><i class="icon-gear"></i></a>
							</div>
						</div>
					</div>
				</li>
			</ul>

			<span class="badge bg-success-400 ml-md-auto mr-md-3">Active</span>

			<ul class="navbar-nav">
				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown">
						<i class="icon-bubbles5"></i>
						<span class="d-md-none ml-2">Messages</span>
						<span class="badge badge-pill badge-mark bg-orange-400 border-orange-400 ml-auto ml-md-0"></span>
					</a>
					
					<div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
						<div class="dropdown-content-header">
							<span class="font-weight-semibold">Messages</span>
							<a href="#" class="text-default"><i class="icon-compose"></i></a>
						</div>

						<div class="dropdown-content-body dropdown-scrollable">
							<ul class="media-list">
								<li class="media">
									<div class="mr-3 position-relative">
										<img src="../../../../global_assets/images/placeholders/placeholder.jpg" width="36" height="36" class="rounded-circle" alt="">
									</div>

									<div class="media-body">
										<div class="media-title">
											<a href="#">
												<span class="font-weight-semibold">James Alexander</span>
												<span class="text-muted float-right font-size-sm">04:58</span>
											</a>
										</div>

										<span class="text-muted">who knows, maybe that would be the best thing for me...</span>
									</div>
								</li>

								<li class="media">
									<div class="mr-3 position-relative">
										<img src="../../../../global_assets/images/placeholders/placeholder.jpg" width="36" height="36" class="rounded-circle" alt="">
									</div>

									<div class="media-body">
										<div class="media-title">
											<a href="#">
												<span class="font-weight-semibold">Margo Baker</span>
												<span class="text-muted float-right font-size-sm">12:16</span>
											</a>
										</div>

										<span class="text-muted">That was something he was unable to do because...</span>
									</div>
								</li>

								<li class="media">
									<div class="mr-3">
										<img src="../../../../global_assets/images/placeholders/placeholder.jpg" width="36" height="36" class="rounded-circle" alt="">
									</div>
									<div class="media-body">
										<div class="media-title">
											<a href="#">
												<span class="font-weight-semibold">Jeremy Victorino</span>
												<span class="text-muted float-right font-size-sm">22:48</span>
											</a>
										</div>

										<span class="text-muted">But that would be extremely strained and suspicious...</span>
									</div>
								</li>

								<li class="media">
									<div class="mr-3">
										<img src="../../../../global_assets/images/placeholders/placeholder.jpg" width="36" height="36" class="rounded-circle" alt="">
									</div>
									<div class="media-body">
										<div class="media-title">
											<a href="#">
												<span class="font-weight-semibold">Beatrix Diaz</span>
												<span class="text-muted float-right font-size-sm">Tue</span>
											</a>
										</div>

										<span class="text-muted">What a strenuous career it is that I've chosen...</span>
									</div>
								</li>

								<li class="media">
									<div class="mr-3">
										<img src="../../../../global_assets/images/placeholders/placeholder.jpg" width="36" height="36" class="rounded-circle" alt="">
									</div>
									<div class="media-body">
										<div class="media-title">
											<a href="#">
												<span class="font-weight-semibold">Richard Vango</span>
												<span class="text-muted float-right font-size-sm">Mon</span>
											</a>
										</div>
										
										<span class="text-muted">Other travelling salesmen live a life of luxury...</span>
									</div>
								</li>
							</ul>
						</div>

						<div class="dropdown-content-footer justify-content-center p-0">
							<a href="#" class="bg-light text-grey w-100 py-2" data-popup="tooltip" title="Load more"><i class="icon-menu7 d-block top-0"></i></a>
						</div>
					</div>
				</li>

				<li class="nav-item dropdown dropdown-user">
					<a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
						<img src="http://demo.interface.club/limitless/demo/Template/global_assets/images/demo/users/face11.jpg" class="rounded-circle mr-2" height="34" alt="">
						<span>Victoria</span>
					</a>
					
					<div class="dropdown-menu dropdown-menu-right">
						<a href="#" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
						<a href="#" class="dropdown-item"><i class="icon-coins"></i> My balance</a>
						<a href="#" class="dropdown-item"><i class="icon-comment-discussion"></i> Messages <span class="badge badge-pill bg-blue ml-auto">58</span></a>
						<div class="dropdown-divider"></div>
						<a href="#" class="dropdown-item"><i class="icon-cog5"></i> Account settings</a>
						<a href="#" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->

	<!-- Page header -->
	<div class="page-header">
		<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
			<div class="d-flex">
				<div class="breadcrumb">
					<a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
					<span class="breadcrumb-item active">Dashboard</span>
				</div>

				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>

			<div class="header-elements d-none">
				<div class="breadcrumb justify-content-center">
					<a href="#" class="breadcrumb-elements-item">
						<i class="icon-comment-discussion mr-2"></i>
						Support
					</a>

					<div class="breadcrumb-elements-item dropdown p-0">
						<a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
							<i class="icon-gear mr-2"></i>
							Settings
						</a>

						<div class="dropdown-menu dropdown-menu-right">
							<a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
							<a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
							<a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
							<div class="dropdown-divider"></div>
							<a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="page-header-content header-elements-md-inline">
			<div class="page-title d-flex">
				<h4> <span class="font-weight-semibold">Dashboard Projects</span></h4>
				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>

			<div class="header-elements d-none mb-3 mb-md-0">
				<div class="d-flex justify-content-center">
					<a href="#" class="btn btn-success"><i class="icon-upload mr-2"></i> <span>Post a Projects</span></a>
				</div>
			</div>
		</div>
	</div>
	<!-- /page header -->
		

	<!-- Page content -->
	<div class="page-content pt-0">

		<!-- Main sidebar -->
		<div class="sidebar sidebar-light sidebar-main sidebar-expand-md align-self-start">

			<!-- Sidebar mobile toggler -->
			<div class="sidebar-mobile-toggler text-center">
				<a href="#" class="sidebar-mobile-main-toggle">
					<i class="icon-arrow-left8"></i>
				</a>
				<span class="font-weight-semibold">Main sidebar</span>
				<a href="#" class="sidebar-mobile-expand">
					<i class="icon-screen-full"></i>
					<i class="icon-screen-normal"></i>
				</a>
			</div>
			<!-- /sidebar mobile toggler -->


			<!-- Sidebar content -->
			<div class="sidebar-content">
				<div class="card card-sidebar-mobile">

					<!-- Header -->
					<div class="card-header header-elements-inline">
						<h6 class="card-title">Navigation</h6>
						<div class="header-elements">
							<div class="list-icons">
								<a class="list-icons-item" data-action="collapse"></a>
							</div>
						</div>
					</div>
					
					<!-- User menu -->
					<div class="sidebar-user">
						<div class="card-body">
							<div class="media">
								<div class="mr-3">
									<a href="#"><img src="http://demo.interface.club/limitless/demo/Template/global_assets/images/demo/users/face11.jpg" width="38" height="38" class="rounded-circle" alt=""></a>
								</div>

								<div class="media-body">
									<div class="media-title font-weight-semibold">Victoria Baker</div>
									<div class="font-size-xs opacity-50">
										<i class="icon-pin font-size-sm"></i> &nbsp;Santa Ana, CA
									</div>
								</div>

								<div class="ml-3 align-self-center">
									<a href="#" class="text-white"><i class="icon-cog3"></i></a>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->

					
					<!-- Main navigation -->
					<div class="card-body p-0">
						<ul class="nav nav-sidebar" data-nav-type="accordion">

							<!-- Main -->
							<li class="nav-item-header mt-0"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
							<li class="nav-item">
								<a href="index.html" class="nav-link active">
									<i class="icon-home4"></i>
									<span>
										Dashboard Projects
									</span>
								</a>
							</li>
							<li class="nav-item nav-item-submenu">
								<a href="#" class="nav-link"><i class="icon-copy"></i> <span>Expert Management</span></a>

								<ul class="nav nav-group-sub" data-submenu-title="Layouts">
									<li class="nav-item"><a href="../../../../layout_1/LTR/default/full/index.html" class="nav-link">Master Data Client</a></li>
								</ul>
							</li>
							<li class="nav-item nav-item-submenu">
								<a href="#" class="nav-link"><i class="icon-color-sampler"></i> <span>Inkubasi Center</span></a>

								<ul class="nav nav-group-sub" data-submenu-title="Themes">
									<li class="nav-item"><a href="index.html" class="nav-link active">Default</a></li>
								</ul>
							</li>
							<li class="nav-item nav-item-submenu">
								<a href="#" class="nav-link"><i class="icon-stack"></i> <span>Kinerja Managerial</span></a>

								<ul class="nav nav-group-sub" data-submenu-title="Starter kit">
									<li class="nav-item"><a href="../seed/layout_nav_horizontal.html" class="nav-link">Horizontal navigation</a></li>
								</ul>
							</li>
							<li class="nav-item"><a href="../../../RTL/default/full/index.html" class="nav-link"><i class="icon-width"></i> <span>RTL version</span></a></li>
							<!-- /main -->

							<!-- Layout -->
							<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Master Data</div> <i class="icon-menu" title="Layout options"></i></li>
							<li class="nav-item nav-item-submenu">
								<a href="#" class="nav-link"><i class="icon-stack2"></i> <span>Page layouts</span></a>

								<ul class="nav nav-group-sub" data-submenu-title="Page layouts">
									<li class="nav-item"><a href="layout_navbar_fixed_main.html" class="nav-link">Fixed main navbar</a></li>
									<li class="nav-item"><a href="layout_navbar_sticky_secondary.html" class="nav-link">Sticky secondary navbar</a></li>
									<li class="nav-item"><a href="layout_navbar_hideable_main.html" class="nav-link">Hideable main navbar</a></li>
									<li class="nav-item"><a href="layout_navbar_hideable_secondary.html" class="nav-link">Hideable secondary navbar</a></li>
									<li class="nav-item"><a href="layout_footer_fixed.html" class="nav-link">Fixed footer</a></li>
									<li class="nav-item"><a href="layout_footer_hideable.html" class="nav-link">Hideable footer</a></li>
									<li class="nav-item"><a href="layout_sidebar_stretched.html" class="nav-link">Stretched sidebar</a></li>
									<li class="nav-item"><a href="layout_without_header.html" class="nav-link">Without page header</a></li>
									<li class="nav-item-divider"></li>
									<li class="nav-item"><a href="layout_boxed_default.html" class="nav-link">Boxed with default sidebar</a></li>
									<li class="nav-item"><a href="layout_boxed_mini.html" class="nav-link">Boxed with mini sidebar</a></li>
									<li class="nav-item"><a href="layout_boxed_full.html" class="nav-link">Boxed full width</a></li>
									<li class="nav-item"><a href="layout_boxed_content.html" class="nav-link">Boxed content</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /sidebar content -->
			
		</div>
		<!-- /main sidebar -->


		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content">
				<div class="row">
					<div class="col-md-12">
						<!-- Filter toolbar -->
						<div class="navbar navbar-expand-lg navbar-light navbar-component rounded">
							<div class="text-center d-lg-none w-100">
								<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-filter">
									<i class="icon-unfold mr-2"></i>
									Filters
								</button>
							</div>

							<div class="navbar-collapse collapse" id="navbar-filter">
								<span class="navbar-text font-weight-semibold mr-3">
									Filter:
								</span>

								<ul class="navbar-nav flex-wrap">
									<li class="nav-item dropdown">
										<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
											<i class="icon-sort-time-asc mr-2"></i>
											By date
										</a>

										<div class="dropdown-menu">
											<a href="#" class="dropdown-item">Show all</a>
											<div class="dropdown-divider"></div>
											<a href="#" class="dropdown-item">Today</a>
											<a href="#" class="dropdown-item">Yesterday</a>
											<a href="#" class="dropdown-item">This week</a>
											<a href="#" class="dropdown-item">This month</a>
											<a href="#" class="dropdown-item">This year</a>
										</div>
									</li>

									<li class="nav-item dropdown">
										<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
											<i class="icon-sort-amount-desc mr-2"></i>
											By status
										</a>

										<div class="dropdown-menu">
											<a href="#" class="dropdown-item">Show all</a>
											<div class="dropdown-divider"></div>
											<a href="#" class="dropdown-item">Open</a>
											<a href="#" class="dropdown-item">On hold</a>
											<a href="#" class="dropdown-item">Resolved</a>
											<a href="#" class="dropdown-item">Closed</a>
											<a href="#" class="dropdown-item">Duplicate</a>
											<a href="#" class="dropdown-item">Invalid</a>
											<a href="#" class="dropdown-item">Wontfix</a>
										</div>
									</li>

									<li class="nav-item dropdown">
										<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
											<i class="icon-sort-numeric-asc mr-2"></i>
											By priority
										</a>

										<div class="dropdown-menu">
											<a href="#" class="dropdown-item">Show all</a>
											<div class="dropdown-divider"></div>
											<a href="#" class="dropdown-item">Highest</a>
											<a href="#" class="dropdown-item">High</a>
											<a href="#" class="dropdown-item">Normal</a>
											<a href="#" class="dropdown-item">Low</a>
										</div>
									</li>
									
								</ul>
								{{-- <span>Search : </span> --}}
								<form class="mt-2 ml-auto" action="#">
									
									<div class="form-group form-group-feedback form-group-feedback-right">
									
										<input type="search" class="form-control" placeholder="Type and hit Enter">
										<div class="form-control-feedback">
											<i class="icon-search4 font-size-base text-muted"></i>
										</div>
									</div>
								</form>
								
								
							</div>
						</div>
						<!-- /filter toolbar -->
					</div>
				</div>

				

				
				
				@for ($i=0;$i<5;$i++)
					
				<!-- Basic card -->
				<div class="row">
					<div class="col-xl-12">
						<div class="card border-left-3 border-left-success-400 rounded-left-0">
							<div class="card-body">
								
								<div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
									<div>
										<h6 style="line-height:0">
											<a href="task_manager_detailed.html">XGracias IBIK Bogor</a>
										</h6>
										<span class="text-muted">28 January, 2015</span>
							
										<p class="mt-2">Produk Xgracias untuk Institut Bisnis dan Kesatuan Bogor Manage Service 3 Tahun</p>

										<span>Client: <span class="font-weight-semibold">IBIK Bogor</span></span><br>
										<span>Partner: <span class="font-weight-semibold">BUT</span></span>
										
									</div>

									<ul class="list list-unstyled mb-0 mt-3 mt-sm-0 ml-auto">
										<li><h3><span class="value-project"><span class="badge badge-danger">Rp. 250.000.000</span></span></h3></li>
										
										<li><i class="icon-user"></i> PM: <span class="font-weight-bold">Istikmal</span></li>
										<li><i class="icon-user"></i> PIC: <span class="font-weight-bold">Yusza Reditya Murti</span></li>
									</ul>
								</div>
						
							</div>
						

							<div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
								<div>
									<span class="mr-1">Progress: </span>
									<a type="button" class="badge bg-success-800" data-popup="tooltip" title="" data-original-title="Proposal">Proposal</a>
									<a type="button" class="badge bg-success-800" data-popup="tooltip" title="" data-original-title="Penawaran">Penawaran</a>
									<a type="button" class="badge bg-primary-800" data-popup="tooltip" title="" data-original-title="Pengerjaan">Pengerjaan</a>
									<a type="button" class="badge bg-grey-300" data-popup="tooltip" title="" data-original-title="BAST">BAST</a>
									<a type="button" class="badge bg-grey-300" data-popup="tooltip" title="" data-original-title="Financial">Financial</a>
									<a type="button" class="badge bg-grey-300" data-popup="tooltip" title="" data-original-title="Close">Closed</a>
									<span class="mr-1 ml-1">&gt;</span>
									<span class="badge badge-primary">100%</span>
								</div>
								<ul class="list-inline mb-0 mt-2 mt-sm-0">
									<li class="list-inline-item dropdown">
										<span>Kategori: </span>
										<span class="badge badge-success">Aplikasi</span>
									</li>
									<li class="list-inline-item dropdown">
										
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				
				@endfor

			
				<!-- /form layouts -->

			</div>
			<!-- /content area -->

		</div>
		<!-- /main content -->


		<!-- Right sidebar -->
		

	</div>
	<!-- /page content -->


<!-- Footer -->
<div class="navbar navbar-expand-lg navbar-light">
	<div class="text-center d-lg-none w-100">
		<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
			<i class="icon-unfold mr-2"></i>
			Footer
		</button>
	</div>

	<div class="navbar-collapse collapse" id="navbar-footer">
			<span class="navbar-text">
				&copy; {{ date('Y')-1 }} - {{ date('Y') }}. <span class="text-primary font-weight-semibold">PMO Application</span> by BTP
			</span>

		<ul class="navbar-nav ml-lg-auto">
			<li class="nav-item"><a href="" class="navbar-nav-link"><i class="icon-lifebuoy mr-2"></i> Support</a></li>
		</ul>
	</div>
</div>
<!-- /footer -->

<!-- Core JS files -->
<script type="text/javascript" src="{{url('/themes/limitless/global/js/main/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/main/bootstrap.bundle.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/loaders/blockui.min.js')}}"></script>
<!-- Core JS files -->

<script type="text/javascript" src="{{url('/themes/limitless/layout_4/js/app.js')}}"></script>

<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/forms/validation/validate.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/forms/validation/additional_methods.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/notifications/noty.min.js')}}"></script>
<script type="text/javascript" src="{{url('/themes/limitless/global/js/setting.js')}}"></script>

@yield('js_section')

</body>
</html>