@extends('tpl.limitless.master_layout4')

@section('css_section')

@endsection

@section('page_title')
Data Problem Pasar
@endsection

@section('content')
<div class="card">
    <table class="table table-striped" id="table">
        <thead>
        <tr>
            <th>Judul</th>
            <th>Kategori</th>
            <th>Deskripsi</th>
            <th>Requirement</th>
            <th>Tanggal Periode</th>
            <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>

<div class="modal fade" id="frmbox" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger-700" style="background-image:url({{url('/images/backgrounds/bg.png')}})">
                <h5 class="modal-title"><i class="fa fa-navicon"></i> &nbsp;Form</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form id="frm" class="form-validate">
                <input type="hidden" name="id" id="id">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Judul</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="inp[em_title]" id="em_title" data-rule-required="true">
                        </div>
                    </div>
                    {{-- <div class="form-group row">
                        <label class="col-form-label col-lg-3">Kategori</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="inp[em_cat]" id="em_cat" data-rule-required="true">
                        </div>
                    </div> --}}
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Kategori</label>
                        <div class="col-lg-9">
                            <div class="uniform-select fixedWidth"><span style="user-select: none;">Kategori</span><select class="form-control form-control-uniform" data-fouc="" name="inp[em_cat]" id="em_cat" data-rule-required="true">
                                @foreach ($errors as $category)
                                <option 
                                    value="{{$category->cp_id}}">{{$category->cp_name}}</option>     
                                @endforeach
                            </select></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Deskripsi</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="inp[em_desc]" id="em_desc" data-rule-required="true">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Requirement</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="inp[em_req]" id="em_req" data-rule-required="true">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Tanggal Periode</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="inp[em_date]" id="em_date" data-rule-required="true">
                        </div>
                    </div>
                </div>
                <div class="modal-footer bg-grey-100">
                    <button type="button" class="btn btn-danger btn-labeled btn-labeled-left btn-xs pull-left" data-dismiss="modal">
                        <b><i class="icon-switch"></i></b> Cancel
                    </button>
                    <button type="button" class="btn btn-success btn-labeled btn-labeled-left btn-xs" id="act-save" onclick="save('insert')">
                        <b><i class="icon-floppy-disk"></i></b> Simpan
                    </button>
                    <button type="button" class="btn btn-success btn-labeled btn-labeled-left btn-xs" id="act-update" onclick="save('update')">
                        <b><i class="icon-floppy-disk"></i></b> Update
                    </button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div class="modal fade" id="frmassign" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger-700" style="background-image:url({{url('/images/backgrounds/bg.png')}})">
                <h5 class="modal-title"><i class="fa fa-navicon"></i> &nbsp;Form</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form id="frm" class="form-validate">
                <input type="hidden" name="id" id="id">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="form-group">
                        <label>Assign to Expert</label>
                        <input type="text" value="" data-role="tagsinput" class="form-control tagsinput-typeahead" data-fouc>
                    </div>
                </div>
                
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<!-- /.modal -->
@endsection

@section('js_section')
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/interactions.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/widgets.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/effects.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/mousewheel.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/globalize/globalize.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/forms/tags/tagsinput.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/forms/tags/tokenfield.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/ui/prism.min.js')}}"></script>
<script>
    $(function(){
        dTable =  $('#table').DataTable({
            ajax: {
                url: '{{ url("em/problem_pasar/dt") }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: [
                { data: 'em_title', name: 'em_title', orderable: true },
                { data: 'em_cat', name: 'em_cat', orderable: true },
                { data: 'em_desc', name: 'em_desc', orderable: true },
                { data: 'em_req', name: 'em_req', orderable: true },
                { data: 'em_date', name: 'em_date', orderable: true },
                { data: 'action', name: 'action', orderable: false, searchable: false, className: 'center'}
            ],
            "order": [['0', 'asc']]
        });

        $('.select2').select2();

        $('.datepicker').datepicker({
            dateFormat : 'yy-mm-dd'
        });

        $('.form-check-input-styled').uniform();

        $('.dataTables_filter input[type=search]').attr('placeholder','Pencarian');

        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });

        $('.dt-buttons').append('<button class="btn btn-light bg-white" onclick="add()"><i class="icon-add"></i> <span class="d-none d-lg-inline-block ml-2">Tambah Data</span></button>');
    });

    function add()
    {
        _reset();
        $('.form-check-input-styled').prop('checked',false).uniform('refresh');
        $('#act-save').show();
        $('#act-update').hide();
        $('#frmbox').modal({keyboard: false, backdrop: 'static'});
    }

    function save(url)
    {
        if($("#frm").valid())
        {
            var formData = new FormData($('#frm')[0]);

            $.ajax({
                url:'{{ url("em/problem_pasar") }}'+'/'+url,
                type:'post',
                data: formData,
                contentType: false,//untuk upload image
                processData: false,//untuk upload image
                timeout: 300000, // sets timeout to 3 seconds
                dataType:'json',
                success : function(e) {
                    if(e.status == 'ok;') {
                        new Noty({
                            text: 'Berhasil Menyimpan Data',
                            type: 'success'
                        }).show();

                        dTable.draw();
                        $("#frmbox").modal('hide');
                    } else {
                        alert(e.text);
                    }
                }
            });
        }
    }

    function edit(id)
    {
        $.ajax({
            url:'{{ url("em/problem_pasar/edit") }}',
            type:'post',
            dataType:'json',
            data: ({ id : id }),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(e) {
                _reset();
                $('#act-save').hide();
                $('#act-update').show();
                $('#id').val(id);
                $.each(e, function(key, value) {
                    if($('#'+key).hasClass("select2")) {
                        $('#'+key).val(value).trigger('change');
                    }
                    else if($('input[type=radio]').hasClass(key)){
                        if(value!=""){
                            $("input[name='inp["+key+"]'][value='" + value + "']").prop('checked', true);
                            $.uniform.update();
                        }
                    }
                    else if($('input[type=checkbox]').hasClass(key)){
                        if(value!=null){
                            var temp = value.split('; ');
                            for (var i = 0; i < temp.length; i++) {
                                $("input[name='inp["+key+"][]'][value='" + temp[i] + "']").prop('checked', true);
                            }
                            $.uniform.update();
                        }
                    }
                    else  $('#'+key).val(value);
                });
                $('#frmbox').modal({keyboard: false, backdrop: 'static'});
            }
        });
    }

    function assign(id){
        _reset();
        $('#frmassign').modal({keyboard: false, backdrop: 'static'});
    }
    function del(id, txt)
    {
        if(confirm('Data: '+txt+'\n'+'Apakah anda yakin akan menghapus data ini?')) {
        $.ajax({
            url:'{{ url("pmo/cat_project/delete") }}',
            type:'delete',
            dataType:'json',
            data: ({id : id }),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(e) {
                if(e.status == 'ok;') {
                    new Noty({
                        text: 'Berhasil Menghapus Data',
                        type: 'success'
                    }).show();

                    dTable.draw();
                } else {
                    alert(e.text);
                }
            }
        });
    }
    }
</script>
@endsection