@extends('tpl.limitless.master_layout4')

@section('css_section')

@endsection

@section('page_title')
    Detail Expert
@endsection

@section('content')
<div class="content">

	<!-- Inner container -->
	<div class="d-md-flex align-items-md-start">

		<!-- Left sidebar component -->
		<div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-left wmin-300 border-0 shadow-0 sidebar-expand-md">

			<!-- Sidebar content -->
			<div class="sidebar-content">

				<!-- Navigation -->
				<div class="card">
					<div class="card-body bg-indigo-400 text-center card-img-top" style="background-color: #C63232; background-size: contain;">
						<div class="card-img-actions d-inline-block mb-3">
							<img class="img-fluid rounded-circle" src="\themes\limitless\global\images\user.png" width="170" height="170" alt="">
							
						</div>

						<h6 class="font-weight-semibold mb-0">Rahmadi Wijaya, S.Si., MT.</h6>
						<span class="d-block opacity-75">FAKULTAS ILMU TERAPAN</span>
					</div>
				</div>
				<!-- /navigation -->
			</div>
			<!-- /sidebar content -->

		</div>
		<!-- /left sidebar component -->


		<!-- Right content -->
		<div class="tab-content w-100 overflow-auto">
			<div class="tab-pane fade active show" id="profile">
				<div class="row">
					<div class="col-md-5">
						<div class="card border-left-danger rounded-left-0">
						
							<div class="card-header">
								<h5><b>Kepakaran :</b></h5>
								<h6>Teknologi Informatika</h6>
							</div>
							<div class="card-header">
								<h5><b>Pendidikan :</b></h5>
								<h6>S1 Ilmu Komputer, Unpad</h6>
								<h6>S2 Informatika, ITB</h6>
							</div>
						
							
						</div>
					</div>
					<div class="col-md-6">
						<div class="card border-left-danger rounded-left-0">
						
							<div class="card-header">
								<h5><b>Kontak :</b></h5>
								<h6>Nomer Telepon : </br> +62 813 455 *** *** *</h6>
								<h6>Email : </br> rahmadi@tass.telkomuniversity.ac.id</h6>
							</div>
						
							
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5">
						<div class="card border-left-danger rounded-left-0">
						
							<div class="card-header">
								<h5><b>Pendidikan: </b></h5>
								<h6>
									<ul>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
									</ul>
								</h6>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="card border-left-danger rounded-left-0">
						
							
							<div class="card-header">
								<h5><b>Riset :</b></h5>
								<h6>
									<ul>
										<li>Tes Kegunaan (Usabilty Testing) Pada Aplikasi Kepegawaian Dengan Menggunakan System Usabilty Scale (Studi Kasus: Dinas Pertanian Kabupaten Bandung)</li>
										<li>Penggunaan Sistem Pakar dalam Pengembangan portal Informasi untuk Spesifikasi Jenis Penyakit Infeksi</li>
										<li>Analisis model IT menggunakan balanced scorecard untuk pengembangan sistem teknologi informasi</li>
									</ul>
								</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5">
						<div class="card border-left-danger rounded-left-0">
						
							<div class="card-header">
								<h5><b>Publikasi :</b></h5>
								<h6>
									<ul>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
									</ul>
								</h6>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="card border-left-danger rounded-left-0">
						
							<div class="card-header">
								<h5><b>Kekayaan Intelektual: </b></h5>
								<h6>
									<ul>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
									</ul>
								</h6>
							</div>
						</div>
					</div>
				</div>


			
			</div>
		</div>
		<!-- /right content -->

	</div>
	<!-- /inner container -->

</div>
@endsection

@section('js_section')
    <script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/uploaders/dropzone.min.js')}}"></script>

    <script>
        $(function(){

        });
    </script>
@endsection