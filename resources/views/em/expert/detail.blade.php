@extends('tpl.limitless.master_layout4')

@section('css_section')

@endsection

@section('page_title')
    Detail Expert
@endsection

@section('content')
<div class="content">
<!-- Cover area -->
<div class="profile-cover">
    <div class="profile-cover-img" style="background-image:url({{ url('themes/limitless/global/images/cover.png') }})"></div>
    <div class="media align-items-center text-center text-md-left flex-column flex-md-row m-0">
        <div class="mr-md-3 mb-2 mb-md-0">
            <a href="#" class="profile-thumb">
                <img src="\themes\limitless\global\images\user.png" class="border-white rounded-circle" width="48" height="48" alt="">
            </a>
        </div>

        <div class="media-body text-white">
            <h1 class="mb-0">Rahmadi Wijaya, S.Si., MT.</h1>
            <span class="d-block">FAKULTAS ILMU TERAPAN</span>
        </div>

        <div class="ml-md-3 mt-2 mt-md-0">
            <ul class="list-inline list-inline-condensed mb-0">
                <li class="list-inline-item"><a href="#" class="btn btn-light border-transparent"><i class=""></i> Button</a></li>
                <li class="list-inline-item"><a href="#" class="btn btn-light border-transparent"><i class=""></i> Button</a></li>
            </ul>
        </div>
    </div>
    
</div>
<!-- /cover area -->

<!-- Kepakaran -->
<div class="navbar navbar-expand-lg navbar-light bg-light">
    <a  class="navbar-nav-link" >
        <b>Kepakaran :</b>
    </a>
    <button type="button" style="margin: 1mm" class="btn btn-outline bg-pink-400 text-pink-400 border-pink-400">Teknologi Informatika</button>
    <button type="button" style="margin: 1mm" class="btn btn-outline bg-pink-400 text-pink-400 border-pink-400">Teknologi Web</button>	                    	
</div>
<!-- /Kepakaran -->

<!-- Profile navigation -->
<div class="navbar navbar-expand-lg navbar-light bg-light">

    <div class="navbar-collapse collapse" id="navbar-second">
        <ul class="nav navbar-nav">
            <li class="nav-item">
                <a href="#pendidikan" class="navbar-nav-link active" data-toggle="tab">
                    <i class="icon-graduation"></i>
                    Pendidikan
                </a>
            </li>
            <li class="nav-item">
                <a href="#penelitian" class="navbar-nav-link" data-toggle="tab">
                    <i class="icon-search4"></i>
                    Penelitian
                </a>
            </li>
            <li class="nav-item">
                <a href="#publikasi" class="navbar-nav-link" data-toggle="tab">
                    <i class="icon-newspaper2"></i>
                    Publikasi
                </a>
            </li>
            <li class="nav-item">
                <a href="#hki" class="navbar-nav-link" data-toggle="tab">
                    <i class="icon-stack-check"></i>
                    HKI
                </a>
            </li>
            <li class="nav-item">
                <a href="#sertifikat" class="navbar-nav-link" data-toggle="tab">
                    <i class="icon-task"></i>
                    Sertifikat
                </a>
            </li>
            <li class="nav-item">
                <a href="#proyek" class="navbar-nav-link" data-toggle="tab">
                    <i class="icon-portfolio"></i>
                    Pengalaman Proyek
                </a>
            </li>
        </ul>

    </div>
</div>
<!-- /profile navigation -->

<!-- Content area -->
<div class="content">

    <!-- Inner container -->
    <div class="d-flex align-items-start flex-column flex-md-row">

        <!-- Left content -->
        <div class="tab-content w-100 order-2 order-md-1">
            {{-- pendidikan --}}
            <div class="tab-pane fade active show" id="pendidikan">

                <div style="margin-top: 2%">
                <!-- List layout -->
			             <div class="card">
							<div class="card-header bg-white header-elements-inline">
								<h6 class="card-title">Pendidikan</h6>
							</div>

							<ul class="media-list">
								<li class="media card-body flex-column flex-sm-row">
									<div class="mr-sm-3 mb-2 mb-sm-0">
										<a href="#">
											<img src="\themes\limitless\global\images\bg.jpg"  class="rounded" width="44" height="44" alt="">
										</a>
									</div>

									<div class="media-body">
										<h6 class="media-title font-weight-semibold">
											<a href="#">Institut Teknologi Telkom</a>
										</h6>

										<ul class="list-inline list-inline-dotted text-muted mb-2">
											<li class="list-inline-item"><a href="#" class="text-muted">S1</a></li>
											<li class="list-inline-item">Bandung, Indonesia</li>
										</ul>

									</div>
								</li>
								<li class="media card-body flex-column flex-sm-row">
									<div class="mr-sm-3 mb-2 mb-sm-0">
										<a href="#">
											<img src="\themes\limitless\global\images\bg.jpg"  class="rounded" width="44" height="44" alt="">
										</a>
									</div>

									<div class="media-body">
										<h6 class="media-title font-weight-semibold">
											<a href="#">Institut Teknologi Bandung</a>
										</h6>

										<ul class="list-inline list-inline-dotted text-muted mb-2">
											<li class="list-inline-item"><a href="#" class="text-muted">S2</a></li>
											<li class="list-inline-item">Bandung, Indonesia</li>
										</ul>

									</div>

									<div class="ml-sm-3 mt-2 mt-sm-0">
										<span class="badge bg-blue">New</span>
									</div>
								</li>
							</ul>
						</div>
                        <!-- /list layout -->
                </div>
            </div>

            {{-- penelitian --}}
            <div class="tab-pane fade" id="penelitian">
                <div style="margin-top: 1%">
                    <div class="card card-body">
                        <div class="media flex-column flex-sm-row">
                            <div class="mr-sm-3 mb-2 mb-sm-0">
                                <a href="#">
                                    <img src="\themes\limitless\global\images\bg.jpg"
                                        class="rounded" width="44" height="44" alt="">
                                </a>
                            </div>

                            <div class="media-body">
                                <h6 class="media-title font-weight-semibold">
                                    <a href="#">Sistem Optimasi Hidroponik NFT</a>
                                </h6>

                                <ul class="list-inline list-inline-dotted text-muted mb-2">
                                    <li class="list-inline-item"><a href="#" class="text-muted">2018</a></li>
                                    <li class="list-inline-item">Ketua Tim</li>
                                </ul>
                                Sistem Optimasi Hidroponik NFT (Nutrient Film Technique) dengan Smart Solar Power Plant Unit
                            </div>
                        </div>
                    </div>
                    <div class="card card-body">
                        <div class="media flex-column flex-sm-row">
                            <div class="mr-sm-3 mb-2 mb-sm-0">
                                <a href="#">
                                    <img src="\themes\limitless\global\images\bg.jpg"
                                        class="rounded" width="44" height="44" alt="">
                                </a>
                            </div>

                            <div class="media-body">
                                <h6 class="media-title font-weight-semibold">
                                    <a href="#">Software Defined Network (SDN)</a>
                                </h6>

                                <ul class="list-inline list-inline-dotted text-muted mb-2">
                                    <li class="list-inline-item"><a href="#" class="text-muted">2018</a></li>
                                    <li class="list-inline-item">Ketua Tim</li>
                                </ul>
                                Implementasi Protokol Routing pada Software Defined Network (SDN) menggunakan RYU Controller dan Open Vswitch
                            </div>

                            <div class="ml-sm-3 mt-2 mt-sm-0">
                                <span class="badge bg-blue">New</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- publikasi --}}
            <div class="tab-pane fade" id="publikasi">
                <div style="margin-top: 2%">
                    <!-- List layout -->
                             <div class="card">
                                <div class="card-header bg-white header-elements-inline">
                                    <h6 class="card-title">Publikasi</h6>
                                </div>
    
                                <ul class="media-list">
                                    <li class="media card-body flex-column flex-sm-row">
                                        <div class="mr-sm-3 mb-2 mb-sm-0">
                                            <a href="#">
                                                <img src="\themes\limitless\global\images\bg.jpg"  class="rounded" width="44" height="44" alt="">
                                            </a>
                                        </div>
    
                                        <div class="media-body">
                                            <h6 class="media-title font-weight-semibold">
                                                <a href="#">Tes Kegunaan (Usabilty Testing)</a>
                                            </h6>
    
                                            <ul class="list-inline list-inline-dotted text-muted mb-2">
                                                <li class="list-inline-item"><a href="#" class="text-muted">2018</a></li>
                                                <li class="list-inline-item">Vol. 20</li>
                                            </ul>
                                            Aplikasi Kepegawaian Dengan Menggunakan System Usabilty Scale
                                        </div>
                                    </li>
                                   
                                </ul>
                            </div>
                            <!-- /list layout -->
                </div>
            </div>

            {{-- HKI --}}
            <div class="tab-pane fade" id="hki">
                <div style="margin-top: 2%">
                    <!-- List layout -->
                             <div class="card">
                                <div class="card-header bg-white header-elements-inline">
                                    <h6 class="card-title">Hak Kekayaan Intelektual</h6>
                                </div>
    
                                <ul class="media-list">
                                    <li class="media card-body flex-column flex-sm-row">
                                        <div class="mr-sm-3 mb-2 mb-sm-0">
                                            <a href="#">
                                                <img src="\themes\limitless\global\images\bg.jpg"  class="rounded" width="44" height="44" alt="">
                                            </a>
                                        </div>
    
                                        <div class="media-body">
                                            <h6 class="media-title font-weight-semibold">
                                                <a href="#">STRUKTUR PENDUKUNG UNTUK MEMUNGKINKAN INTEGRASI TEKNOLOGI-TEKNOLOGI ANTI-SPAM</a>
                                            </h6>
    
                                            <ul class="list-inline list-inline-dotted text-muted mb-2">
                                                <li class="list-inline-item"><a href="#" class="text-muted">2009</a></li>
                                                <li class="list-inline-item"> No. IDP000023904</li>
                                            </ul>
                                        
                                        </div>
                                    </li>
                                   
                                </ul>
                            </div>
                            <!-- /list layout -->
                </div>
            </div>

            <div class="tab-pane fade" id="sertifikat">
                <div style="margin-top: 2%">
                    <!-- List layout -->
                             <div class="card">
                                <div class="card-header bg-white header-elements-inline">
                                    <h6 class="card-title">Sertifikat</h6>
                                </div>
    
                                <ul class="media-list">
                                    <li class="media card-body flex-column flex-sm-row">
                                        <div class="mr-sm-3 mb-2 mb-sm-0">
                                            <a href="#">
                                                <img src="\themes\limitless\global\images\bg.jpg"  class="rounded" width="44" height="44" alt="">
                                            </a>
                                        </div>
    
                                        <div class="media-body">
                                            <h6 class="media-title font-weight-semibold">
                                                <a href="#">Sertifikat Pendidik</a>
                                            </h6>
    
                                            <ul class="list-inline list-inline-dotted text-muted mb-2">
                                                <li class="list-inline-item"><a href="#" class="text-muted">2014</a></li>
                                                <li class="list-inline-item"> No. 0010273</li>
                                            </ul>
                                        
                                        </div>
                                    </li>
                                   
                                </ul>
                            </div>
                            <!-- /list layout -->
                </div>
            </div>

            <div class="tab-pane fade" id="proyek">
                <div style="margin-top: 1%">
                    <div class="card card-body">
                        <div class="media flex-column flex-sm-row">
                            <div class="mr-sm-3 mb-2 mb-sm-0">
                                <a href="#">
                                    <img src="\themes\limitless\global\images\bg.jpg"
                                        class="rounded" width="44" height="44" alt="">
                                </a>
                            </div>

                            <div class="media-body">
                                <h6 class="media-title font-weight-semibold">
                                    <a href="#">Sistem Optimasi Hidroponik NFT</a>
                                </h6>

                                <ul class="list-inline list-inline-dotted text-muted mb-2">
                                    <li class="list-inline-item"><a href="#" class="text-muted">2018</a></li>
                                    <li class="list-inline-item">Ketua Tim</li>
                                </ul>
                                Sistem Optimasi Hidroponik NFT (Nutrient Film Technique) dengan Smart Solar Power Plant Unit
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- /left content -->

    </div>
    <!-- /inner container -->

</div>
<!-- /content area -->



</div>
@endsection

@section('js_section')
    <script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/uploaders/dropzone.min.js')}}"></script>
    <script src="{{url('/themes/limitless/layout_1/js/app.js')}}"></script>
    <script src="{{url('/themes/limitless/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script>
        $(function(){

        });
    </script>
@endsection