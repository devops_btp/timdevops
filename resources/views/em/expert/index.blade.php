@extends('tpl.limitless.master_layout4')

@section('css_section')

@endsection

@section('page_title')
    Data Expert
@endsection

@section('content')
<div class="card">
    <div class="table-responsive">
        <table class="table text-nowrap">
            <thead>
                <tr>
                    <th style="width: 30%;">Nama Expert</th>
                    <th style="width: 30%;">Jabatan</th>
                    <th style="width: 40%;">Fakultas</th>
                    <th class="text-center" style="width: 20px;">
                    <a href="{{ url('em/expert/insert') }}" class="btn btn-success rounded-round"><i class="icon-plus2 mr-2"></i>Tambah Data</a>

                    </th>
                </tr>
            </thead>
            <tbody>


                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <div class="mr-3">
                                <a href="{{ url('em/expert/detail') }}" class="btn bg-teal-400 rounded-round btn-icon btn-sm">
                                    <span class="letter-icon"></span>
                                </a>
                            </div>
                            <div>
                                <a href="{{ url('em/expert/detail') }}" class="text-default font-weight-semibold letter-icon-title">Rahmadi Wijaya, S. SI., MT.</a>
                                {{-- <div class="text-muted font-size-sm"> Fakultas Ilmu Terapan</div> --}}
                            </div>
                        </div>
                    </td>
                    <td>
                        <a href="{{ url('em/expert/detail') }}" class="text-default">
                            <div class="font-weight-semibold">Kaprodi D3 Rekayasa Perangkat Lunak Aplikasi</div>
                        </a>
                    </td>
                    <td>
                        <a href="{{ url('em/expert/detail') }}" class="text-default">
                            <div class="font-weight-semibold">Fakultas Ilmu Terapan</div>
                        </a>
                    </td>
                </tr>
                

                
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('js_section')
    <script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/uploaders/dropzone.min.js')}}"></script>
>

	<!-- Theme JS files -->
	<script src="{{url('/themes/limitless/global/js/plugins/visualization/d3/d3.min.js')}}"></script>
	<script src="{{url('/themes/limitless/global/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
	<script src="{{url('/themes/limitless/global/js/plugins/forms/styling/switchery.min.js')}}"></script>
	<script src="{{url('/themes/limitless/global/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
	<script src="{{url('/themes/limitless/global/js/plugins/ui/moment/moment.min.js')}}"></script>
	<script src="{{url('/themes/limitless/global/js/plugins/pickers/daterangepicker.js')}}"></script>

    <script src="{{url('/themes/limitless/layout_1/js/app.js')}}"></script>
    <script src="{{url('/themes/limitless/global/js/demo_pages/dashboard.js')}}"></script>
    
    

    <script>
        $(function(){

        });
    </script>
@endsection