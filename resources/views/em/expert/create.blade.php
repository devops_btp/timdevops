@extends('tpl.limitless.master_layout4')

@section('css_section')

@endsection

@section('page_title')
    Insert Data Expert
@endsection

@section('content')
        
{{-- Data Pribadi --}}
<div class="card">
    <div class="card-body">
        <form action="#">
            <div class="row">
                <legend class="font-weight-semibold"><i class="icon-reading mr-2"></i>Data Pribadi</legend>
                <div class="col-md-6">
                    <fieldset>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Nama Lengkap</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Tanggal Lahir</label>
                            <div class="col-lg-9">
                                <input type="date" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Nomer Telepon</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control">
                            </div>
                        </div>

                    </fieldset>
                </div>

                <div class="col-md-6">
                    <fieldset>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Fakultas</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Jabatan</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Email</label>
                            <div class="col-lg-9">
                                <input type="email" class="form-control">
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
{{-- /Data Pribadi --}}

{{-- Pendidikan --}}
<div class="card">
    
    <div class="card-header header-elements-inline">
        
        <h5 class="card-title">Pendidikan</h5>
        
        <div class="header-elements">
            <button type="button" data-toggle="modal" data-target="#modal_pendidikan" class="btn btn-success rounded-round"><i class="icon-plus2 mr-2"></i>Tambah Data</button>
        </div>
    </div>
    <table class="table datatable-basic">
        <thead>
            <tr>
                <th>Peguruan Tinggi</th>
                <th>Program</th>
                <th>Konsentrasi Ilmu</th>
                <th>Tahun Lulus</th>
                <th>Judul Tugas Akhir (Skripsi/Tesis/Desertasi)</th>
                <th class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Universitas Telkom</td>
                <td>Sarjana</td>
                <td>Teknik Informatika</td>
                <td>2013</td>
                <td>Analisis model IT menggunakan balanced scorecard untuk pengembangan sistem teknologi informasi</td>
                <td class="text-center">
                    <div style="white-space: nowrap;">
                        <button type="button" class="btn btn-success rounded-round"><i class="icon-pencil5 mr-2"></i>Edit</button> 
                        <button type="button" class="btn btn-danger rounded-round"><i class="icon-trash mr-2"></i>Delete</button>
                    </div>
                    {{-- <div class="list-icons">
                        <div class="dropdown">
                            <a href="#" class="list-icons-item" data-toggle="dropdown">
                                <i class="icon-menu9"></i>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="#" class="dropdown-item"><i class="icon-pencil5"></i>Edit</a>
                                <a href="#" class="dropdown-item"><i class="icon-trash"></i>Delete</a>
                            </div>
                        </div>
                    </div> --}}
                </td>
            </tr>
            
           
        </tbody>
    </table>
</div>
    {{-- Modal Pendidikan --}}
    <div id="modal_pendidikan" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Data Pendidikan</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <form action="#">
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Peguruan Tinggi</label>
                                    <input type="text" placeholder="Universitas" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Program</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Konsentrasi Ilmu</label>
                                    <input type="text" class="form-control">
                                </div>

                                <div class="col-sm-6">
                                    <label>Tahun Lulus</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Judul Tugas Akhir</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn bg-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- /Modal Pendidikan --}}
{{-- /Pendidikan --}}

{{-- Penelitan --}}
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Pengalaman Riset</h5>
        <div class="header-elements">
            <button type="button" data-toggle="modal" data-target="#modal_riset" class="btn btn-success rounded-round"><i class="icon-plus2 mr-2"></i>Tambah Data</button>
        </div>
    </div>
    <table class="table datatable-basic">
        <thead>
            <tr>
                <th>Judul Riset</th>
                <th>Peran/Posisi</th>
                <th>Tahun Riset</th>
                <th>Nilai Pendanaan</th>
                <th>Sumber Pendanaan</th>
                <th>Mitra Riset</th>
                <th class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Penggunaan Sistem Pakar dalam Pengembangan portal Informasi untuk Spesifikasi Jenis Penyakit Infeksi</td>
                <td>Ketua Tim</td>
                <td>2018</td>
                <td>10.000.000</td>
                <td>Universitas</td>
                <td>Dinas</td>
                <td class="text-center">
                    <div style="white-space: nowrap;">
                        <button type="button" class="btn btn-success rounded-round"><i class="icon-pencil5 mr-2"></i>Edit</button> 
                        <button type="button" class="btn btn-danger rounded-round"><i class="icon-trash mr-2"></i>Delete</button>
                    </div>
                    {{-- <div class="list-icons">
                        <div class="dropdown">
                            <a href="#" class="list-icons-item" data-toggle="dropdown">
                                <i class="icon-menu9"></i>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="#" class="dropdown-item"><i class="icon-pencil5"></i>Edit</a>
                                <a href="#" class="dropdown-item"><i class="icon-trash"></i>Delete</a>
                            </div>
                        </div>
                    </div> --}}
                </td>
            </tr>
            
           
        </tbody>
    </table>
</div>
    {{-- Modal Penelitian --}}
    <div id="modal_riset" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Data Pengalaman Riset</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <form action="#">
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Judul Riset</label>
                                    <textarea type="text" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Peran/Posisi</label>
                                    <input type="text" class="form-control">
                                </div>

                                <div class="col-sm-6">
                                    <label>Tahun</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Nilai Pendanaan</label>
                                    <input type="text" class="form-control">
                                </div>

                                <div class="col-sm-6">
                                    <label>Sumber Pendanaan</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>

                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Mitra</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>

                       
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn bg-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- /Modal Penelitian --}}
{{-- /Penelitian --}}

{{-- Publikasi --}}
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Publikasi</h5>
        <div class="header-elements">
            <button type="button" data-toggle="modal" data-target="#modal_publikasi" class="btn btn-success rounded-round"><i class="icon-plus2 mr-2"></i>Tambah Data</button>
        </div>
    </div>
    <table class="table datatable-basic">
        <thead>
            <tr>
                <th>Judul</th>
                <th>Nama Jurnal</th>
                <th>Tahun</th>
                <th>Volume</th>
                <th>Url</th>
                <th class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Tes Kegunaan (Usabilty Testing)</td>
                <td>Aplikasi Kepegawaian Dengan Menggunakan System Usabilty Scale</td>
                <td>2018</td>
                <td>20</td>
                <td></td>
                <td class="text-center">
                    <div style="white-space: nowrap;">
                        <button type="button" class="btn btn-success rounded-round"><i class="icon-pencil5 mr-2"></i>Edit</button> 
                        <button type="button" class="btn btn-danger rounded-round"><i class="icon-trash mr-2"></i>Delete</button>
                    </div>
                    {{-- <div class="list-icons">
                        <div class="dropdown">
                            <a href="#" class="list-icons-item" data-toggle="dropdown">
                                <i class="icon-menu9"></i>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="#" class="dropdown-item"><i class="icon-pencil5"></i>Edit</a>
                                <a href="#" class="dropdown-item"><i class="icon-trash"></i>Delete</a>
                            </div>
                        </div>
                    </div> --}}
                </td>
            </tr>
            
           
        </tbody>
    </table>
</div>
    {{-- Modal Publikasi --}}
    <div id="modal_publikasi" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Data Publikasi</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <form action="#">
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Judul</label>
                                    <textarea type="text" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Nama Jurnal</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Tahun</label>
                                    <input type="text" class="form-control">
                                </div>

                                <div class="col-sm-6">
                                    <label>Volume</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Url</label>
                                    <input type="url" class="form-control">
                                </div>
                            </div>
                        </div>

                    
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn bg-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- /Modal Publikasi --}}
{{-- /Publikasi --}}

{{-- HKI --}}
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">HKI</h5>
        <div class="header-elements">
            <button type="button" data-toggle="modal" data-target="#modal_hki" class="btn btn-success rounded-round"><i class="icon-plus2 mr-2"></i>Tambah Data</button>
        </div>
    </div>
    <table class="table datatable-basic">
        <thead>
            <tr>
                <th>Judul</th>
                <th>Deskripsi</th>
                <th>Tahun</th>
                <th>No. HKI</th>
                <th class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Tes Kegunaan (Usabilty Testing)</td>
                <td>Aplikasi Kepegawaian Dengan Menggunakan System Usabilty Scale</td>
                <td>2018</td>
                <td>20</td>
                <td class="text-center">
                    <div style="white-space: nowrap;">
                        <button type="button" class="btn btn-success rounded-round"><i class="icon-pencil5 mr-2"></i>Edit</button> 
                        <button type="button" class="btn btn-danger rounded-round"><i class="icon-trash mr-2"></i>Delete</button>
                    </div>
                    {{-- <div class="list-icons">
                        <div class="dropdown">
                            <a href="#" class="list-icons-item" data-toggle="dropdown">
                                <i class="icon-menu9"></i>
                            </a>
                            
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="#" class="dropdown-item"><i class="icon-pencil5"></i>Edit</a>
                                <a href="#" class="dropdown-item"><i class="icon-trash"></i>Delete</a>
                            </div>
                        </div>
                    </div> --}}
                </td>
                <td></td>
            </tr>
            
           
        </tbody>
    </table>
</div>
    {{-- Modal HKI --}}
    <div id="modal_hki" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Data Publikasi</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <form action="#">
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Judul</label>
                                    <textarea type="text" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Deskripsi</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Tahun</label>
                                    <input type="text" class="form-control">
                                </div>

                                <div class="col-sm-6">
                                    <label>No. HKI</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                    
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn bg-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- /Modal HKI --}}
{{-- /HKI --}}


@endsection

@section('js_section')
    <script type="text/javascript" src="{{url('/themes/limitless/global/js/plugins/uploaders/dropzone.min.js')}}"></script>
    
<script src="{{url('/themes/limitless/layout_1/js/app.js')}}"></script>

	<!-- Theme JS files -->
	<script src="{{url('/themes/limitless/global/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script src="{{url('/themes/limitless/global/js/plugins/forms/styling/uniform.min.js')}}"></script>

	<script src="{{url('/themes/limitless/global/js/demo_pages/form_layouts.js')}}"></script>
    <script src="{{url('/themes/limitless/global/js/plugins/forms/inputs/inputmask.js')}}"></script>
    <script src="{{url('/themes/limitless/global/js/plugins/forms/validation/validate.min.js')}}"></script> 
    <script src="{{url('/themes/limitless/global/js/plugins/extensions/cookie.js')}}"></script>
    <!-- /theme JS files -->

    <script src="{{url('/themes/limitless/global/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{url('/themes/limitless/global/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script src="{{url('/themes/limitless/global/js/demo_pages/datatables_basic.js')}}"></script>
    <script src="{{url('/themes/limitless/global/js/demo_pages/components_modals.js')}}"></script>
	<script src="{{url('/themes/limitless/global/js/plugins/notifications/bootbox.min.js')}}"></script>
    <script>
        $(function(){

        });
    </script>

    
@endsection