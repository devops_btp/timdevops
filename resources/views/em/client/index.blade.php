@extends('tpl.limitless.master_layout4')

@section('css_section')

@endsection

@section('page_title')
Data Client
@endsection

@section('content')
<div class="card">
    <table class="table table-striped" id="table">
        <thead>
        <tr>
            <th>Nama Perusahaan</th>
            <th>No. Telp</th>
            <th>Alamat Perusahaan</th>
            <th>Email</th>
            <th>PIC</th>
            <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>

<div class="modal fade" id="frmbox" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger-700">
                <h5 class="modal-title"><i class="fa fa-xnavicon"></i> &nbsp;Form</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form id="frm" class="form-validate">
                <input type="hidden" name="id" id="id">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Nama Perusahaan</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="inp[em_name]" id="em_name" data-rule-required="true">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">No. Telp</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="inp[em_telp]" id="em_telp" data-rule-required="true">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Alamat</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="inp[em_address]" id="em_address" data-rule-required="true">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Email</label>
                        <div class="col-lg-9">
                            <input type="email" class="form-control" name="inp[em_email]" id="em_email" data-rule-required="true">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">PIC - Nama</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="inp[em_picname]" id="em_picname" data-rule-required="true">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">PIC - Email</label>
                        <div class="col-lg-9">
                            <input type="email" class="form-control" name="inp[em_picemail]" id="em_picemail" data-rule-required="true">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">PIC - No.Telp</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="inp[em_pictelp]" id="em_pictelp" data-rule-required="true">
                        </div>
                    </div>
                    {{-- <div class="form-group row">
                        <label class="col-form-label col-lg-3">Gambar</label>
                        <div class="col-lg-9">
                            <div class="uniform-uploader"><input type="file" class="form-control-uniform" name="inp[em_img]" id="em_img"data-fouc=""><span class="filename" style="user-select: none;">No file selected</span><span class="action btn btn-light" style="user-select: none;">Choose File</span></div>
                        </div>
                    </div> --}}
                   
                </div>
                <div class="modal-footer bg-grey-100">
                    <button type="button" class="btn btn-danger btn-labeled btn-labeled-left btn-xs pull-left" data-dismiss="modal">
                        <b><i class="icon-switch"></i></b> Cancel
                    </button>
                    <button type="button" class="btn btn-success btn-labeled btn-labeled-left btn-xs" id="act-save" onclick="save('insert')">
                        <b><i class="icon-floppy-disk"></i></b> Simpan
                    </button>
                    <button type="button" class="btn btn-success btn-labeled btn-labeled-left btn-xs" id="act-update" onclick="save('update')">
                        <b><i class="icon-floppy-disk"></i></b> Update
                    </button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('js_section')
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/interactions.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/widgets.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/effects.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/mousewheel.min.js')}}"></script>
<script src="{{url('/themes/limitless/global/js/plugins/extensions/jquery_ui/globalize/globalize.js')}}"></script>
<script src="{{url('/themes/limitless/layout_1/js/app.js')}}"></script>
<script>
    $(function(){
        dTable =  $('#table').DataTable({
            ajax: {
                url: '{{ url("em/client/dt") }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: [
                { data: 'em_name', name: 'em_name', orderable: true },
                { data: 'em_telp', name: 'em_telp', orderable: true },
                { data: 'em_address', name: 'em_address', orderable: true },
                { data: 'em_email', name: 'em_email', orderable: true },
                { data: 'em_picname', name: 'em_picname', orderable: true },
                // { data: 'em_picemail', name: 'em_picemail', orderable: true },
                // { data: 'em_pictelp', name: 'em_pictelp', orderable: true },
                { data: 'action', name: 'action', orderable: false, searchable: false, className: 'center'}
            ],
            "order": [['0', 'asc']]
        });

        $('.select2').select2();

        $('.datepicker').datepicker({
            dateFormat : 'yy-mm-dd'
        });

        $('.form-check-input-styled').uniform();

        $('.dataTables_filter input[type=search]').attr('placeholder','Pencarian');

        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });

        $('.dt-buttons').append('<button class="btn btn-light bg-white" onclick="add()"><i class="icon-add"></i> <span class="d-none d-lg-inline-block ml-2">Tambah Data</span></button>');
    });

    function add()
    {
        _reset();
        $('.form-check-input-styled').prop('checked',false).uniform('refresh');
        $('#act-save').show();
        $('#act-update').hide();
        $('#frmbox').modal({keyboard: false, backdrop: 'static'});
    }

    function save(url)
    {
        if($("#frm").valid())
        {
            var formData = new FormData($('#frm')[0]);

            $.ajax({
                url:'{{ url("em/client") }}'+'/'+url,
                type:'post',
                data: formData,
                contentType: false,//untuk upload image
                processData: false,//untuk upload image
                timeout: 300000, // sets timeout to 3 seconds
                dataType:'json',
                success : function(e) {
                    if(e.status == 'ok;') {
                        new Noty({
                            text: 'Berhasil Menyimpan Data',
                            type: 'success'
                        }).show();

                        dTable.draw();
                        $("#frmbox").modal('hide');
                    } else {
                        alert(e.text);
                    }
                }
            });
        }
    }

    function edit(id)
    {
        $.ajax({
            url:'{{ url("em/client/edit") }}',
            type:'post',
            dataType:'json',
            data: ({ id : id }),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(e) {
                _reset();
                $('#act-save').hide();
                $('#act-update').show();
                $('#id').val(id);
                $.each(e, function(key, value) {
                    if($('#'+key).hasClass("select2")) {
                        $('#'+key).val(value).trigger('change');
                    }
                    else if($('input[type=radio]').hasClass(key)){
                        if(value!=""){
                            $("input[name='inp["+key+"]'][value='" + value + "']").prop('checked', true);
                            $.uniform.update();
                        }
                    }
                    else if($('input[type=checkbox]').hasClass(key)){
                        if(value!=null){
                            var temp = value.split('; ');
                            for (var i = 0; i < temp.length; i++) {
                                $("input[name='inp["+key+"][]'][value='" + temp[i] + "']").prop('checked', true);
                            }
                            $.uniform.update();
                        }
                    }
                    else  $('#'+key).val(value);
                });
                $('#frmbox').modal({keyboard: false, backdrop: 'static'});
            }
        });
    }

    function del(id, txt)
    {
        if(confirm('Data: '+txt+'\n'+'Apakah anda yakin akan menghapus data ini?')) {
        $.ajax({
            url:'{{ url("em/client/delete") }}',
            type:'delete',
            dataType:'json',
            data: ({id : id }),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(e) {
                if(e.status == 'ok;') {
                    new Noty({
                        text: 'Berhasil Menghapus Data',
                        type: 'success'
                    }).show();

                    dTable.draw();
                } else {
                    alert(e.text);
                }
            }
        });
    }
    }
</script>
@endsection