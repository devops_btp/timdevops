FROM php:7.2-cli
RUN mkdir /usr/src/myapp
COPY . /usr/src/myapp
WORKDIR /usr/src/myapp
CMD ['php', '/usr/src/myapp']

