<?php

namespace App\Http\Controllers\Km;

use App\Models\Km;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;
use App\Models\soltekModel;
use App\Models\soltekHkiJenis;
use Yajra\Datatables\Datatables;
class DataKmController extends Controller
{
    public function index(Request $request)
    {
        if(empty($request->year))
            $year = date('Y');
        else
            $year = $request->year;

        if(empty($request->tw))
            $tw = ceil(date('m')/3);
        else
            $tw = $request->tw;

        $data['var_year'] = $year;
        $data['var_tw'] = $tw;

        $data['km'] = Km::where('km_year', $year)
            ->where('km_tw', $tw)
            ->first();

        $years = Km::select('km_year')->groupBy('km_year')->orderBy('km_year', 'desc')->get();
        $data['year'] = [];
        foreach($years as $year)
            $data['year'][$year->km_year] = $year->km_year;

        return view('km.view_km.index', $data);
    }

    public function dt()
    {
        $hki =  soltekModel::groupBy('rh_year', 'rh_faculty')
            ->selectRaw('sum(rh_qty) as sum, rh_year,rh_faculty')
            ->get();

        return datatables(soltekModel::query())
            ->addColumn('sum', function ($db) {
                return $db->sum;
            })
            ->rawColumns(['sum'])->toJson();
    }
    public function dump()
    {

        $data = soltekModel::groupBy('rh_status','rh_year','rh_faculty')
            ->selectRaw('rh_status,sum(rh_qty) as sum, rh_year,rh_faculty')
            ->orderBy('rh_year','asc')
            ->orderBy('rh_faculty', 'asc')
            ->get();
        return $data;
    }
}
