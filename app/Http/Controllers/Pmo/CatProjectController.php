<?php

namespace App\Http\Controllers\Pmo;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;
use App\Models\CatProject;

class CatProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('pmo.catproject.index');
    }

    public function dt()
    {
        /*$query = CatProject::query()
            ->when($ug = session()->get('defaultusergroup'), function($query) use ($ug) {
                if($ug == 'admin')
                    return $query;
                //return $query->where()
                else if($ug == 'com')
                    return $query->where('com_id', session()->get('userinfo')->com_id );
                else
                    return $query->where('com_id', '-1');
            });

        CatProject::leftJoin('project', 'pr_id', '=', 'cp_id_project')->where('','')->get();*/

        return datatables(CatProject::query())
            ->addColumn('action', function ($db) {
                    return '<a href="javascript:edit(\''.$db->cp_id.'\')" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="icon-database-edit2"></i></a> 
                        <a href="javascript:del(\''.$db->cp_id.'\', \''.$db->cp_name.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="icon-trash"></i></a>';
            })
            ->rawColumns(['action'])->toJson();
    }

    public function insert(Request $request)
    {
        $inp = $request->post('inp');

        if($inp) {
            $dbs = new CatProject();

            $dbs->cp_id = CatProject::getNextSequenceId();

            foreach($inp as $key => $row){
                if(is_array($inp[$key])) {
                    $inp[$key] = implode('; ',$inp[$key]); //untuk checkbox
                }
                else {
                    $dbs->$key = $inp[$key];
                }
            }

            if($dbs->save())
                return json_encode(array('status' => 'ok;', 'text' => ''));
            else
                return json_encode(array('status' => 'error;', 'text' => 'Gagal Menyimpan Data' ));
        }
        else return json_encode(array('status' => 'error;', 'text' => 'Gagal Menyimpan Data' ));
    }

    public function update(Request $request)
    {
        try {
            $dbs = CatProject::find($request->id);

            $inp = $request->post('inp');
            if($inp){
                foreach($inp as $key => $row){
                    if(is_array($inp[$key])) {
                        $dbs->$key = implode('; ',$inp[$key]); //untuk checkbox
                    } else {
                        $dbs->$key = $inp[$key];
                    }
                }
            }

            $dbs->save();

            return json_encode(array('status' => 'ok;', 'text' => ''));

        } catch (\Illuminate\Database\QueryException $e) {
            return json_encode(array('status' => 'error;', 'text' => 'Gagal Update Data' ));
        }
    }

    public function edit(Request $request)
    {
        return CatProject::find($request->id)->toJson();
    }

    public function delete(Request $request)
    {
        $deleted = CatProject::destroy($request->id);

        if($deleted)
            return json_encode(array('status' => 'ok;', 'text' => ''));
        else
            return json_encode(array('status' => 'error;', 'text' => 'Gagal Delete Data' ));
    }
}
