<?php

namespace App\Http\Controllers\Pmo;

//use App\Models\Items;
use App\Models\Projects;
use App\Models\Document;
use App\Models\Financial;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        //$data['action'] = 'login/exe';

        return view('pmo.dashboard.dashboardlimitless2');
    }

    public function limitless()
    {
        //$data['action'] = 'login/exe';

        return view('pmo.dashboard.dashboardlimitless');
    }
    // public function detail()
    // {
    //     //$data['action'] = 'login/exe';

    //     return view('pmo.dashboard.detailproject');
    // }
    public function detail($id)
    {
        //$data['action'] = 'login/exe';
        //$projects =  Projects::with('financial','document','mitra','client')->find($id);
        // dd($projects['pr_name']);
        // return $projects;
        return view('pmo.dashboard.detailproject');
     
        // return view('pmo.dashboard.coba',array("projects" => $projects));
    }


    public function dt()
    {
        return datatables(Projects::query())
            ->addColumn('pr_name', function ($db) {
               
                return '<div class="d-flex align-items-center">
                            <div class="mr-3">
                                <a href="#" class="btn btn-primary rounded-round btn-icon btn-sm">
                                    <span class="letter-icon"></span>
                                </a>
                            </div>
                            <div>
                                <a href="#" class="text-default font-weight-semibold">'.$db->pr_name.'</a>
                                <div class="text-muted font-size-sm">
                                    <span class="badge badge-mark border-blue mr-1"></span>
                                    '.\Carbon\Carbon::parse($db->pr_date_start)->format('d M Y').' - Juni 2019
                                </div>
                            </div>
                        </div>';
            })
            ->addColumn('currency', function ($db){
                return  number_format($db->pr_budget, 0, ',', '.');
               })
            ->addColumn('action', function ($db) {
                $ug = session()->get('defaultusergroup');
                if($ug == 'admin')
                {
                    return '<a href="javascript:edit(\''.$db->com_id.'\')" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="icon-database-edit2"></i></a> 
                        <a href="javascript:del(\''.$db->com_id.'\', \''.$db->com_name.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="icon-trash"></i></a>';
                }
                else
                {
                    return '<a href="javascript:;" title="Edit Data" class="btn btn-sm btn-icon bg-grey-400"><i class="icon-database-edit2"></i></a> 
                        <a href="javascript:;" title="Delete Data" class="btn btn-sm btn-icon bg-grey-400"><i class="icon-trash"></i></a>';
                }
            })
            ->rawColumns(['pr_name','action'])->toJson();
            
    }

public function dt_allprojects()
    {
      return datatables(Projects::query())
            ->addColumn('pr_name', function ($db) {
             
                     return '<div class="d-flex align-items-center">
                            <div class="mr-3">
                                <a href="#" class="btn btn-primary rounded-round btn-icon btn-sm">
                                    <span class="letter-icon"></span>
                                </a>
                            </div>
                            <div>
                                <a href="#" class="text-default font-weight-semibold">'.$db->pr_name.'</a>
                                <div class="text-muted font-size-sm">
                                    <span class="badge badge-mark border-blue mr-1"></span>
                                    '.\Carbon\Carbon::parse($db->pr_date_start)->format('d M Y').' - Juni 2019
                                </div>
                            </div>
                        </div>';
            })
            ->addColumn('currency', function ($db){
             return  number_format($db->pr_budget, 0, ',', '.');
            })
            
            ->addColumn('action', function ($db) {
                $ug = session()->get('defaultusergroup');
                if($ug == 'admin')
                {
                    return '<a href="javascript:edit(\''.$db->com_id.'\')" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="icon-database-edit2"></i></a> 
                        <a href="javascript:del(\''.$db->com_id.'\', \''.$db->com_name.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="icon-trash"></i></a>';
                }
                
                else
                {
                    return '
                    <div class="list-icons">
                        <div class="dropdown">
                            <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="#" class="dropdown-item"><i class="icon-file-stats"></i> View statement</a>
                                <a href="#" class="dropdown-item"><i class="icon-file-text2"></i> Edit campaign</a>
                                <a href="#" class="dropdown-item"><i class="icon-file-locked"></i> Disable campaign</a>
                                <div class="dropdown-divider"></div>
                                <a href="#" class="dropdown-item"><i class="icon-gear"></i> Settings</a>
                            </div>
                        </div>
                    </div>
               ';
                }
            })
            
            ->rawColumns(['pr_name','action'])->toJson();
        
            
    }

    
}
