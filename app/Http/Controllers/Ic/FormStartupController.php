<?php

namespace App\Http\Controllers\Ic;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;

use App\Models\Startup;

class FormStartupController extends Controller
{
    public function identity(Request $request)
    {
        $data['id'] = $request->id;
        $data['startup'] = Startup::where('ic_id' , $request->id)->first();
        return view('ic.form.identity', $data);
    }

    public function identityUpdate(Request $request)
    {
        try {
            $dbs = Startup::find($request->id);
            $inp = $request->post('inp');

            if($request->hasFile('file')) {
                $filename = $request->file('file')->getClientOriginalName();
                $file = 'ic/startup/so_'.$filename;

                \Storage::disk('public')->put($file, fopen($request->file('file'), 'r+'));

                $inp['ic_file_so'] = $file;
            }

            if($request->hasFile('logo')) {
                $filename = $request->file('logo')->getClientOriginalName();
                $file = 'ic/startup/logo_'.$filename;

                \Storage::disk('public')->put($file, fopen($request->file('logo'), 'r+'));

                $inp['ic_logo'] = $file;
            }

            if($inp){
                foreach($inp as $key => $row){
                    if(is_array($inp[$key])) {
                        $dbs->$key = implode('; ',$inp[$key]); //untuk checkbox
                    } else {
                        $dbs->$key = $inp[$key];
                    }
                }
            }

            $dbs->save();

            return json_encode(array('status' => 'ok;', 'text' => ''));

        } catch (\Illuminate\Database\QueryException $e) {
            return json_encode(array('status' => 'error;', 'text' => 'Gagal Update Data' ));
        }
    }
}
