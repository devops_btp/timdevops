<?php

namespace App\Http\Controllers\Ic;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;

use App\Models\Startup;

class StartupController extends Controller
{
    //


    public function index()
    {
        return view('ic.inkubasi.index');
        
    }
    
    // public function index($page , Request $request)
    // {
    //     if($page == 'update'){
    //         $startup = Startup::where('ic_id' , $request->id)->get();
            
    //         // return $startup; 
    //         return view('ic.inkubasi.insertstartup' , compact('startup'));
    //     }else{
    //         return view('ic.inkubasi.index');
    //     }
    // }

    public function dt()
    {
        # code...
        return datatables(Startup::query())
                ->addColumn('action', function ($db) {
                        return '<a href="startup/form/identity/'.$db->ic_id.'" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="icon-database-edit2"></i></a> 
                            <a href="javascript:del(\''.$db->ic_id.'\', \''.$db->ic_logo.'\', \''.$db->ic_nama_startup.'\',\''.$db->ic_alamat.'\', \''.$db->ic_visi.'\', \''.$db->ic_misi.'\', \''.$db->ri_status.'\', \''.$db->ic_data_medsos.'\', \''.$db->ic_website.'\', \''.$db->ic_jml_pegawai.'\', \''.$db->ic_file_so.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="icon-trash"></i></a>';
                })
                ->rawColumns(['action'])->toJson();
    }


    public function insert(Request $request)
{
    # code...
    $inp = $request->post('inp');
        if($inp) {
            $dbs = new Startup();

            $dbs->ic_id = Startup::getNextSequenceId();

            foreach($inp as $key => $row){
                if(is_array($inp[$key])) {
                    $inp[$key] = implode('; ',$inp[$key]); //untuk checkbox
                    
                    $request->hasFile($inp[$key]);
                    $filename = $request->file($inp[$key])->getClientOriginalName();
                    $file = 'cdn/ic/'.date('Y').'/'.$filename;
                    \Storage::disk('public/images')->put($file, fopen($request->file($inp[$key]), 'r+'));
                    $dbs->ic_logo = $file;

                } else{
                    $dbs->$key = $inp[$key];
                }
            }

            if($dbs->save())
                return json_encode(array('status' => 'ok;', 'text' => ''));
            else
                return json_encode(array('status' => 'error;', 'text' => 'Gagal Menyimpan Data' ));
        }
        else return json_encode(array('status' => 'error;', 'text' => 'Gagal Menyimpan Data' ));
}

public function update(Request $request)
    {
        try {
            $dbs = Startup::find($request->id);

            $inp = $request->post('inp');
            if($inp){
                foreach($inp as $key => $row){
                    if(is_array($inp[$key])) {
                        $dbs->$key = implode('; ',$inp[$key]); //untuk checkbox
                    } else {
                        $dbs->$key = $inp[$key];
                    }
                }
            }

            $dbs->save();

            return json_encode(array('status' => 'ok;', 'text' => ''));

        } catch (\Illuminate\Database\QueryException $e) {
            return json_encode(array('status' => 'error;', 'text' => 'Gagal Update Data' ));
        }
    }

    public function edit(Request $request)
    {
        return Startup::find($request->id)->toJson();
    }

    public function delete(Request $request)
    {
        $deleted = Startup::destroy($request->id);

        if($deleted)
            return json_encode(array('status' => 'ok;', 'text' => ''));
        else
            return json_encode(array('status' => 'error;', 'text' => 'Gagal Delete Data' ));
    }
}
