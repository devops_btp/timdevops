<?php

namespace App\Http\Controllers\Hki;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\DataTables\DataTables;

use Illuminate\Support\Facades\DB;

use App\Models\Hki;
use App\Models\hkiMember;

class DataHkiController extends Controller
{
     //

    public function index()
    {
        return view('hki.index');
    }

    public function form(Request $request)
    {
        $id = $request->id;
        $data['data'] = Hki::where('hki_id', $id)->first();

        return view('hki.form', $data);
    }

    public function dt() {
        //  $data = DB::table('mybtp.hki')
        //  ->join('mybtp.hki_member', 'mybtp.hki.ki_id', '=', 'mybtp.hki_member.kim_id_ki')
        //  ->get();

        // return DB::table('mybtp.hki')
        // -> select("hki.*", "hki_member.kim_person") 
        // -> join('mybtp.hki_member', 'mybtp.hki.ki_id', '=', 'mybtp.hki_member.kim_id_ki')
        // -> limit(10)->get();

        $post = Hki::join('mybtp.hki_member', 'mybtp.hki.ki_id', '=', 'mybtp.hki_member.kim_id_ki')
        -> select(["hki.*", "hki_member.*"])
        -> groupBy(["hki.ki_id","hki_member.kim_id", "hki_member.kim_id_ki", "hki_member.kim_person", "hki_member.kim_status"])
        -> orderBy("kim_id_ki", "DESC");




        return datatables::of($post)
            ->editColumn('ki_title', function($db) {
                $type = strtolower($db->ki_type);
                if($type == 'merek')
                {
                    $stype = 'M';
                    $ctype = 'bg-indigo';
                }
                else if($type == 'hak cipta')
                {
                    $stype = 'HC';
                    $ctype = 'bg-blue';
                }
                else if($type == 'paten')
                {
                    $stype = 'P';
                    $ctype = 'bg-green';
                }
                else if($type == 'desain industri')
                {
                    $stype = 'DI';
                    $ctype = 'bg-pink';
                }
                else
                {
                    $stype = 'NN';
                    $ctype = 'bg-grey';
                }

                return '<div class="d-flex align-items-center">
                            <div class="mr-3">
                                <a href="#" class="btn '.$ctype.' rounded-round btn-icon btn-sm">
                                    <span class="letter-icon">'.$stype.'</span>
                                </a>
                            </div>
                            <div>
                                <a href="#" class="text-default font-weight-semibold">'.$db->ki_title.'</a>
                                <div>
                                    <span class="text-muted">'.$db->kim_person.'</span>
                                </div>
                            </div>
                        </div>';
            })
            ->editColumn('ki_year', function($db) {
                return '<td class="text-center">
                            <h6 class="mb-0">'.$db->ki_year.'</h6>
                            <div class="font-size-sm text-muted line-height-1">januari</div>
                        </td>';
            })
            ->addColumn('action', function ($db) {
                    return '<a href="'.url('hki/form').'/'.$db->ki_id.'" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="icon-database-edit2"></i></a>';
            })
            ->rawColumns(['action','ki_title','ki_year'])->toJson();
     
           
            
        // -> join('mybtp.hki_member', 'mybtp.hki.ki_id', '=', 'mybtp.hki_member.kim_id_ki')
        // -> limit(10)->get();
        

        # code...
        // return datatables(Hki::with('hki_member')->get())
        //         ->addColumn('action', function ($db) {
        //                 return '<a href="javascript:edit(\''.$db->ki_id.'\')" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="icon-database-edit2"></i></a> 
        //                     <a href="javascript:del(\''.$db->ki_year.'\',\''.$db->ki_type.'\',\''.$db->ki_status.'\',\''.$db->ki_title.'\',\''.$db->ki_desc.'\',\''.$db->ki_published_date.'\',\''.$db->ki_published_country.'\',\''.$db->ki_person_city.'\',\''.$db->ki_file_attachment.'\',\''.$db->ki_file_statement.'\',\''.$db->ki_file_transfer.'\',\''.$db->ki_file_ktp.'\',\''.$db->ki_finance.'\',\''.$db->ki_finance_other.'\',\''.$db->ki_submit_date.'\',\''.$db->hki_member->kim_person.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="icon-trash"></i></a>';
        //         })
        //         ->rawColumns(['action'])->toJson();


        

        // $data = Hki::with('hki_member')->get();
        // // dd($data->hki_member()->get());
        // return $data;

        
    }

    public function dtmember() {

        $post = Hki::join('mybtp.hki_member', 'mybtp.hki.ki_id', '=', 'mybtp.hki_member.kim_id_ki')
        -> select(["hki.ki_id", "hki_member.*"])
        -> groupBy(["hki.ki_id", "hki_member.kim_id"])
        -> orderBy("kim_id_ki", "DESC");


        return datatables::of($post)
            ->addColumn('action', function ($db) {
                    return '<a href="javascript:editmember(\''.$db->kim_id_ki.'\')" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="icon-database-edit2"></i></a> 
                            <a href="javascript:del(\''.$db->kim_status.'\',\''.$db->kim_person.'\',\''.$db->kim_prodi.'\',\''.$db->kim_faculty.'\',\''.$db->kim_kk.'\',\''.$db->kim_notelp.'\',\''.$db->kim_email.'\',\''.$db->kim_alamat.'\',\''.$db->kim_kodepos.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="icon-trash"></i></a>';
            })
            ->rawColumns(['action'])->toJson();
     
        
    }
    public function insertmember(Request $request)
    {
        # code...
        $inp_member = $request->post('inp_member');
            if($inp_member) {
                $dbs = new hkiMember();
    
                $dbs->kim_id = hkiMember::getNextSequenceId();
    
                foreach($inp_member as $key => $row){
                    if(is_array($inp_member[$key])) {
                        $inp_member[$key] = implode('; ',$inp_member[$key]); //untuk checkbox
                    }
                    else {
                        $dbs->$key = $inp_member[$key];
                    }
                }
    
                if($dbs->save())
                    return json_encode(array('status' => 'ok;', 'text' => ''));
                else
                    return json_encode(array('status' => 'error;', 'text' => 'Gagal Menyimpan Data' ));
            }
            else return json_encode(array('status' => 'error;', 'text' => 'Gagal Menyimpan Data' ));
    }
    public function insert(Request $request)
    {
        $inp = $request->post('inp');

        if($inp) {
            $dbs = new HKI();

            $dbs->cp_id = HKI::getNextSequenceId();

            foreach($inp as $key => $row){
                if(is_array($inp[$key])) {
                    $inp[$key] = implode('; ',$inp[$key]); //untuk checkbox
                }
                else {
                    $dbs->$key = $inp[$key];
                }
            }

            if($dbs->save())
                return json_encode(array('status' => 'ok;', 'text' => ''));
            else
                return json_encode(array('status' => 'error;', 'text' => 'Gagal Menyimpan Data' ));
        }
        else return json_encode(array('status' => 'error;', 'text' => 'Gagal Menyimpan Data' ));
    }

    public function update(Request $request)
    {
        try {
            $dbs = HKI::find($request->id);

            $inp = $request->post('inp');
            if($inp){
                foreach($inp as $key => $row){
                    if(is_array($inp[$key])) {
                        $dbs->$key = implode('; ',$inp[$key]); //untuk checkbox
                    } else {
                        $dbs->$key = $inp[$key];
                    }
                }
            }

            $dbs->save();

            return json_encode(array('status' => 'ok;', 'text' => ''));

        } catch (\Illuminate\Database\QueryException $e) {
            return json_encode(array('status' => 'error;', 'text' => 'Gagal Update Data' ));
        }
    }

    public function edit(Request $request)
    {   
        return HKI::find($request->id)->toJson();
    }
    public function editmember(Request $request)
    {
        return hki::find($request->id)->toJson();
    }

    public function delete(Request $request)
    {
        $deleted = HKI::destroy($request->id);

        if($deleted)
            return json_encode(array('status' => 'ok;', 'text' => ''));
        else
            return json_encode(array('status' => 'error;', 'text' => 'Gagal Delete Data' ));
    }


    
}
