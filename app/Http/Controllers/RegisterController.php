<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller; 
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File; 
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session; 
use App\Referral;

class RegisterController extends Controller
{
    //
    public function index() {
        $data['action'] = 'register/exe';
        $data['login']  = 'Application';
        if(Session::get('login')){
            return redirect('home');
        }
        return view('tpl.limitless.register',$data);
    } 

    public function check_username(Request $request)
    {
        $inp = $request->post('inp'); 

        $data = Referral::whereRaw("ref_username='".strtolower($inp['ref_username'])."'")->first(); 
		if ($data) echo "false";
		else echo "true";
    }  

    public function exe(Request $request){
        $inp                    = $request->post('inp'); 
		$password               = $request->post('password');
        $inp['ref_password']   = '$AFFILIATE$'.substr(sha1(md5(md5($password))), 0, 50);
 
        $dbs = new Referral(); 
        foreach($inp as $key => $row){
            if(is_array($inp[$key])) {
                $inp[$key] = implode('; ',$inp[$key]); //untuk checkbox
            } 
            else {
                $dbs->$key = $inp[$key];
            }
        } 

        $dbs->ref_id = Referral::getNextSequenceId();
        $dbs->ref_active = 1;

        $dbs->save(); 

        return redirect()->back()->with('alert','Register Berhasil');
    }
 
}