<?php

namespace App\Http\Controllers\inkubasi;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;

use App\Models\Inkubasi;

class inkubasiController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

     //Ramdan
        public function show(){
            return view('ic.inkubasi.data');
        }
        
        public function create(){
            return view('ic.inkubasi.insert');
        }
        
        public function detail(){
            return view('ic.inkubasi.detailStartup');
        }
     //

    public function index()
    {
        //$data['action'] = 'login/exe';
        // $inkubasi =  Inkubasi::all();
        // with('financial','document','mitra','client')->find($id);
        // dd($hki);
        // return $inkubasi;

        return view('ic.datexpert.index');

        // return view('pmo.dashboard.detailproject',array("projects" => $projects));
     
        // return view('pmo.dashboard.coba',array("projects" => $projects));
    }

    public function index()
    {
        //$data['action'] = 'login/exe';
        // $inkubasi =  Inkubasi::all();
        // with('financial','document','mitra','client')->find($id);
        // dd($hki);
        // return $inkubasi;

        return view('ic.datexpert.index');

        // return view('pmo.dashboard.detailproject',array("projects" => $projects));

        // return view('pmo.dashboard.coba',array("projects" => $projects));
    }

    public function dt()
{
    # code...
    return datatables(Inkubasi::query())
            ->addColumn('action', function ($db) {
                    return '<a href="javascript:del(\''.$db->ri_id.'\', \''.$db->ri_year.'\', \''.$db->ri_product.'\',\''.$db->ri_team.'\', \''.$db->ri_desc.'\', \''.$db->ri_team_faculty.'\', \''.$db->ri_status.'\', \''.$db->ri_category.'\', \''.$db->ri_founding.'\')"';
            })
            ->rawColumns(['action'])->toJson();
}

// public function insert(Request $request)
// {
//     # code...
//     $inp = $request->post('inp');

//         if($inp) {
//             $dbs = new Inkubasi();

//             $dbs->ri_id = Inkubasi::getNextSequenceId();

//             foreach($inp as $key => $row){
//                 if(is_array($inp[$key])) {
//                     $inp[$key] = implode('; ',$inp[$key]); //untuk checkbox
//                 }
//                 else {
//                     $dbs->$key = $inp[$key];
//                 }
//             }

//             if($dbs->save())
//                 return json_encode(array('status' => 'ok;', 'text' => ''));
//             else
//                 return json_encode(array('status' => 'error;', 'text' => 'Gagal Menyimpan Data' ));
//         }
//         else return json_encode(array('status' => 'error;', 'text' => 'Gagal Menyimpan Data' ));
// }

// public function update(Request $request)
// {
//     # code...
//     try {
//         $dbs = Inkubasi::find($request->id);

//         $inp = $request->post('inp');
//         if($inp){
//             foreach($inp as $key => $row){
//                 if(is_array($inp[$key])) {
//                     $dbs->$key = implode('; ',$inp[$key]); //untuk checkbox
//                 } else {
//                     $dbs->$key = $inp[$key];
//                 }
//             }
//         }

//         $dbs->save();

//         return json_encode(array('status' => 'ok;', 'text' => ''));

//     } catch (\Illuminate\Database\QueryException $e) {
//         return json_encode(array('status' => 'error;', 'text' => 'Gagal Update Data' ));
//     }
// }

// public function edit(Request $request)
// {
//     # code...
//     return Inkubasi::find($request->id)->toJson();
// }

// public function delete(Request $request)
// {
//     $deleted = Inkubasi::destroy($request->id);
//     if($deleted)
//         return json_encode(array('status' => 'ok;', 'text' => ''));
//     else
//         return json_encode(array('status' => 'error;', 'text' => 'Gagal Delete Data' ));
// }
}
