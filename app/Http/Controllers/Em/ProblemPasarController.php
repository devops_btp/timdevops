<?php

namespace App\Http\Controllers\Em;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;
use App\Models\ProblemPasar;
use App\Models\CatProject;

class ProblemPasarController extends Controller
{
    public function index()
    {
        return view('em.problempasar.index');
        
    }

    public function dt()
    {

        return datatables(ProblemPasar::query())
            ->addColumn('action', function ($db) {
                    return '<a href="javascript:edit(\''.$db->pp_id.'\')" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="icon-database-edit2"></i></a> 
                        <a href="javascript:del(\''.$db->pp_id.'\', \''.$db->em_title.'\', \''.$db->em_cat.'\', \''.$db->em_desc.'\', \''.$db->pp_req.'\', \''.$db->pp_date.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="icon-trash"></i></a>
                        <a href="javascript:assign(\''.$db->pp_id.'\')" title="Assign" class="btn btn-sm btn-icon btn-success"><i class="icon-user-check"></i></a>';
            })
            ->rawColumns(['action'])->toJson();
    }

    public function insert(Request $request)
    {
        $categories = CatProject::with("catproject")->get();
        return view ('em\problempasar\index', compact('categories'));
        // return View::make('em.problempasar.index')->with(compact('categories'));

        $inp = $request->post('inp');

        if($inp) {
            $dbs = new ProblemPasar();

            $dbs->pp_id = ProblemPasar::getNextSequenceId();

            foreach($inp as $key => $row){
                if(is_array($inp[$key])) {
                    $inp[$key] = implode('; ',$inp[$key]); //untuk checkbox
                }
                else {
                    $dbs->$key = $inp[$key];
                }
            }

            if($dbs->save())
                return json_encode(array('status' => 'ok;', 'text' => ''));
            else
                return json_encode(array('status' => 'error;', 'text' => 'Gagal Menyimpan Data' ));
        }
        else return json_encode(array('status' => 'error;', 'text' => 'Gagal Menyimpan Data' ));
    }

    public function update(Request $request)
    {
        try {
            $dbs = ProblemPasar::find($request->id);

            $inp = $request->post('inp');
            if($inp){
                foreach($inp as $key => $row){
                    if(is_array($inp[$key])) {
                        $dbs->$key = implode('; ',$inp[$key]); //untuk checkbox
                    } else {
                        $dbs->$key = $inp[$key];
                    }
                }
            }

            $dbs->save();

            return json_encode(array('status' => 'ok;', 'text' => ''));

        } catch (\Illuminate\Database\QueryException $e) {
            return json_encode(array('status' => 'error;', 'text' => 'Gagal Update Data' ));
        }
    }

    public function edit(Request $request)
    {
        return ProblemPasar::find($request->id)->toJson();
    }

    public function delete(Request $request)
    {
        $deleted = ProblemPasar::destroy($request->id);

        if($deleted)
            return json_encode(array('status' => 'ok;', 'text' => ''));
        else
            return json_encode(array('status' => 'error;', 'text' => 'Gagal Delete Data' ));
    }

}
