<?php

namespace App\Http\Controllers\Em;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;
use App\Models\ClientEM;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('em.client.index');
    }

    public function dt()
    {

        return datatables(ClientEM::query())
            ->addColumn('action', function ($db) {
                    return '<a href="javascript:edit(\''.$db->cl_id.'\')" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="icon-database-edit2"></i></a> 
                        <a href="javascript:del(\''.$db->cl_id.'\', \''.$db->em_name.'\', \''.$db->telp.'\', \''.$db->em_address.'\', \''.$db->em_email.'\', \''.$db->em_picname.'\', \''.$db->em_picemail.'\', \''.$db->em_telp.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="icon-trash"></i></a>';
            })
            ->rawColumns(['action'])->toJson();
    }

    public function insert(Request $request)
    {
        $inp = $request->post('inp');

        if($inp) {
            $dbs = new ClientEM();

            $dbs->cl_id = ClientEM::getNextSequenceId();

            foreach($inp as $key => $row){
                if(is_array($inp[$key])) {
                    $inp[$key] = implode('; ',$inp[$key]); //untuk checkbox
                }
                else {
                    $dbs->$key = $inp[$key];
                }
            }

            if($dbs->save())
                return json_encode(array('status' => 'ok;', 'text' => ''));
            else
                return json_encode(array('status' => 'error;', 'text' => 'Gagal Menyimpan Data' ));
        }
        else return json_encode(array('status' => 'error;', 'text' => 'Gagal Menyimpan Data' ));
    }

    public function update(Request $request)
    {
        try {
            $dbs = ClientEM::find($request->id);

            $inp = $request->post('inp');
            if($inp){
                foreach($inp as $key => $row){
                    if(is_array($inp[$key])) {
                        $dbs->$key = implode('; ',$inp[$key]); //untuk checkbox
                    } else {
                        $dbs->$key = $inp[$key];
                    }
                }
            }

            $dbs->save();

            return json_encode(array('status' => 'ok;', 'text' => ''));

        } catch (\Illuminate\Database\QueryException $e) {
            return json_encode(array('status' => 'error;', 'text' => 'Gagal Update Data' ));
        }
    }

    public function edit(Request $request)
    {
        return ClientEM::find($request->id)->toJson();
    }

    public function delete(Request $request)
    {
        $deleted = ClientEM::destroy($request->id);

        if($deleted)
            return json_encode(array('status' => 'ok;', 'text' => ''));
        else
            return json_encode(array('status' => 'error;', 'text' => 'Gagal Delete Data' ));
    }
}
