<?php

namespace App\Http\Controllers\Login;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Referral;

class IgraciasLogin extends Controller
{
    //
    public function index()
    {
        $data['action'] = 'sso/login';
        $data['login']  = 'Application';
        // if (Session::get('login')) {
        //     // $module = Session::get('module');
        //     // return redirect($module[0]);
        // }
        return view('tpl.limitless.login_igracias', $data);
    }
    public function PostLogin(Request $request)
    {
        $client = new \GuzzleHttp\Client([
                'http_errors' => false
        ]);

        $username = $request->post('username');
        $password = $request->post('password');
        $url = "https://dev-gateway.telkomuniversity.ac.id/issueauth";

      $requestLogin = $client->request('POST', $url, [
            'query' => [
                'username' =>  $username,
                'password' => html_entity_decode($password, ENT_QUOTES, 'UTF-8')
            ]
        ]);
    
    //    dd($requestLogin->getStatusCode());
      if($requestLogin->getStatusCode() != 200){
        return redirect('sso')->with('errors','Gagal Login!');
      } else {
        $response = $requestLogin->getBody()->getContents();
      
        $token = json_decode($response, true);

        $headers = [
            'Authorization' =>  'Bearer ' . $token['token'],
            'Accept'        => 'application/json'
        ];

        $url2 = 'https://dev-gateway.telkomuniversity.ac.id/a277b3eca77891ca5d2f96ab73e98c06';

        $requestProfile = $client->request('GET', $url2, [

            'headers' => $headers

        ]);

        $responseProfile = $requestProfile->getBody()->getContents();

        $profil = json_decode($responseProfile, true);

        $emailUser = $profil['data'][0]['email'];
        $splitEmail = strtok($emailUser, '@');

        if ($username == $splitEmail) {
            Session::put('login', TRUE);
            return redirect('/');
        } else return redirect()->back()->with('alert', 'Username atau Password anda salah. Silahkan coba kembali.');
    }
}
}
