<?php
namespace App\Http\ControllersApp\Http\Controllers\Login;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session; 
use App\Referral;

class LoginController extends Controller
{
    //
    public function index() {
        $data['action'] = 'login/exe';
        $data['login']  = 'Application';
        if(Session::get('login')) {
            // $module = Session::get('module');
            // return redirect($module[0]);
        }
        return view('tpl.limitless.login',$data);
    }


    public function exe(Request $request){
        $username = $request->post('username');
        $password = $request->post('password');
        $pass 	  = '$AFFILIATE$'.substr(sha1(md5(md5($password))), 0, 50);

        $data = Referral::where('ref_username',$username)->where('ref_password',$pass)->where('ref_active','1')->first();

        if($username="btpoke" && $password="btpoke"){
            //   Session::put('userid',$data->ref_id);
            // Session::put('username',$data->ref_username);
            //   Session::put('fullname',$data->ref_fullname);
            Session::put('login',TRUE);

            //    if(isset($allusergroup[7]))
            //       Session::put('emp_id',$data->ref_table_id);

            return redirect('/');
        }
        else return redirect()->back()->with('alert','Username atau Password anda salah. Silahkan coba kembali.');
    }

    public function exe_ori(Request $request){
        $username = $request->post('username');
        $password = $request->post('password');
        $pass 	  = '$XGRACIAS$'.substr(sha1(md5(md5($password))), 0, 50);
        $data = Referral::leftJoin('xgpanel.usergroups', function($join) {
            $join->on('usergroups.usergroups_id', '=', 'users.ref_defaultusergroup');
        })
            ->where('ref_username',$username)->where('ref_password',$pass)->first();

        if($data){
            $ug = json_decode($data->ref_usergroup,true);
            if(!$ug) return redirect('/login')->with('alert','Anda belum Tergabung dengan Usergroup');
            $id = "";
            if($data->ref_defaultusergroup!='1') $id = $data->ref_defaultusergroup;
            else {
                foreach($ug as $row){
                    if($row!=1) { $id = $row; break;
                    }
                }
            }
            if($id!="") {
                $dt = DB::select("select * from (SELECT unnest(string_to_array(REPLACE(REPLACE(REPLACE(usergroups_module,'\"', ' '),'[',''),']',''), ',')) as token  FROM xgpanel.usergroups where usergroups_id='$id')a
                left join xgpanel.modules on modules_id=CAST(token AS INT) where  modules_slug is not null");


                $group  = Usergroups::where('usergroups_id',$id)->first();

                $allusergroup = array();
                foreach($ug as $row){
                    // if($row!=1 and $row!=$id) {
                    if($row!=1) {
                        $usergroup = Usergroups::where('usergroups_id',$row)->first();
                        $allusergroup[$usergroup->usergroups_id] = $usergroup->usergroups_name;
                    }

                }

                asort($allusergroup);

                if($dt){
                    $temp = array();
                    foreach($dt as $row){
                        $temp[] = str_replace('/', '_', $row->modules_slug);
                    }
                    Session::forget(['module', 'username', 'fullname', 'defaultusergroup_id','defaultusergroup_name', 'allusergroup', 'login', 'cname', 'cpart', 'navJson']);
                    Session::flush();
                    Session::put('module',$temp);
                    Session::put('userid',$data->ref_id);
                    Session::put('empid',$data->ref_table_id);
                    Session::put('nip',$data->ref_nip);
                    Session::put('username',$data->ref_username);
                    Session::put('fullname',$data->ref_fullname);
                    Session::put('defaultusergroup_id',$data->ref_defaultusergroup);
                    Session::put('defaultusergroup_name',$group->usergroups_name);
                    Session::put('allusergroup',$allusergroup);
                    Session::put('login',TRUE);

                    return redirect('/');
                }
                else return redirect()->back()->with('alert','Usergroup '.$group->usergroups_name.' belum mempunyai modul');
            }
            else return redirect()->back()->with('alert','Username atau Password anda salah. Silahkan coba kembali.');
        }
        else return redirect()->back()->with('alert','Username atau Password anda salah. Silahkan coba kembali.');
    }

    public function logout(Request $request){
        Session::forget(['module', 'username', 'fullname', 'defaultusergroup_id','defaultusergroup_name', 'allusergroup', 'login', 'cname', 'cpart', 'navJson']);
        Session::flush();
        // \Cache::flush();

        return redirect('/login');
    }
    

}
