<?php

namespace App\Http\Controllers\Data;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;

use App\Models\DataSegmen;

class SegmenController extends Controller
{
    public function index()
    {
        $data["grafik"] = DB::table('mybtp.report_segment_pasar')->select(DB::raw("rsp_target as name, sum(rsp_price)/1000000000 as y"))->groupBy("rsp_target")->get();
    //    dd($data['grafik']);
        return view('data.segmen.index', $data);
    }

    public function dt()
    {
        # code...
        return datatables(DataSegmen::query())
            ->addColumn('action', function ($db) {
                return '
                        <a href="javascript:del(\'' . $db->rp_id . '\', \'' . $db->rp_year . '\', \'' . $db->rp_number . '\',\'' . $db->rp_client . '\', \'' . $db->rp_contract . '\', \'' . $db->rp_project . '\', \'' . $db->rp_date_start . '\', \'' . $db->rp_date_end . '\', \'' . $db->rp_price . '\', \'' . $db->rp_ppn . '\', \'' . $db->rp_price_total . '\')"';
            })
            ->rawColumns(['action'])->toJson();
    }
}
