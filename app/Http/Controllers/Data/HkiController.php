<?php

namespace App\Http\Controllers\Data;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;
use App\Models\soltekModel;
use App\Models\soltekHkiJenis;
use Yajra\Datatables\Datatables;
class HkiController extends Controller
{
    public function index()
    {
        $rekap = soltekModel::selectRaw('rh_status,sum(rh_qty) as sum, rh_year,rh_faculty')
            ->orderBy('rh_year','asc')
            ->orderBy('rh_faculty', 'asc')
            ->groupBy('rh_status','rh_year','rh_faculty')
            ->get();

        $rekapJenis = soltekHkiJenis::selectRaw('rhj_name,rhj_qty,rhj_year,sum(rhj_qty) as sum')
        ->orderBy('rhj_year','asc')
        ->orderBy('rhj_name', 'asc')
        ->groupBy('rhj_name','rhj_qty','rhj_year')
        ->get();
      

        $data['rekap_tahun'] = [];
        $data['rekap_unit'] = [];
        $data['rj_tahun']=[];
        $data['rj_unit']=[];
        $rekap_data = [];
        $rekap_faculty = [];
        $rekap_jenis= [];
        $rekap_data_jenis= [];
        foreach($rekap as $r)
        {
            if(!in_array($r->rh_year, $data['rekap_tahun']))
                $data['rekap_tahun'][] = $r->rh_year;

            if(!in_array($r->rh_faculty, $data['rekap_unit']))
                $data['rekap_unit'][] = $r->rh_faculty;

            if(empty($rekap_faculty[$r->rh_year][$r->rh_faculty]))
                $rekap_faculty[$r->rh_year][$r->rh_faculty] = $r->sum;
            else
                $rekap_faculty[$r->rh_year][$r->rh_faculty] += $r->sum;

            $rekap_data[$r->rh_year][$r->rh_faculty][$r->rh_status] = $r->sum;
        }

        foreach($rekapJenis as $rj)
        {
       
            if(!in_array($rj->rhj_year, $data['rj_tahun']))
                $data['rj_tahun'][] = $rj->rhj_year;

            if(!in_array($rj->rhj_name, $data['rj_unit']))
            $data['rj_unit'][] = $rj->rhj_name;

            if(empty($rekap_jenis[$rj->rhj_year][$rj->rhj_name]))
            $rekap_jenis[$rj->rhj_year][$rj->rhj_name] = $rj->rhj_qty;
            else
            $rekap_jenis[$rj->rhj_year][$r->rhj_name] += $rj->rhj_qty;

        }

       
       
        $json = [];
        $json2 = [];
        $jsonJenis = [];
        foreach($rekap_faculty as $th => $faculty)
        {
            $json[] = [
                'name' => $th,
                'data' => array_values($faculty)
            ];

            $json2[] = array_sum($faculty);
        }

        foreach($rekap_jenis as $rj => $jenis)
        {
            $jsonJenis[] = [
                'name' => $rj,
                'data' => array_values($jenis)
            ];
        }
       

        $data['chart_jenis'] = json_encode($jsonJenis);
        $data['chart_unit'] = json_encode($json);
        $data['chart_year'] = json_encode($json2);
        $data['rekap'] = $rekap_data;
        $data['rekap_jenis'] = $rekap_jenis;
        //dd($data['rekap_jenis']);
        return view('data.hki.index', $data);
    }

    public function dt()
    {
        $hki =  soltekModel::groupBy('rh_year', 'rh_faculty')
            ->selectRaw('sum(rh_qty) as sum, rh_year,rh_faculty')
            ->get();

        return datatables(soltekModel::query())
            ->addColumn('sum', function ($db) {
                return $db->sum;
            })
            ->rawColumns(['sum'])->toJson();
    }
    public function dump()
    {

        $data = soltekModel::groupBy('rh_status','rh_year','rh_faculty')
            ->selectRaw('rh_status,sum(rh_qty) as sum, rh_year,rh_faculty')
            ->orderBy('rh_year','asc')
            ->orderBy('rh_faculty', 'asc')
            ->get();
        return $data;
    }
}
