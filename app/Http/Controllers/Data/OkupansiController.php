<?php

namespace App\Http\Controllers\Data;

use App\Models\MsRoom;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;
use App\Models\CatProject;
use App\Models\Okupansi;

class OkupansiController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('data.okupansi.index');
    }

    public function dt()
    {
        $q = MsRoom::select(\DB::raw("mr_room, max(mr_seat) as total_seat, sum(COALESCE(rgo_seat,0)) as terisi_seat, string_agg(rgo_company, '||') as company"))
            ->leftJoin('mybtp.report_ga_okupansi', 'rgo_room', '=', 'mr_room')
            ->groupBy('mr_room');

        return datatables($q)
            ->editColumn('company', function ($db) {
                return str_replace('||', '<br>', ($db->company ?? '-'));
            })
            ->addColumn('action', function ($db) {
                    return '<a href="javascript:edit(\''.$db->rgo_id.'\')" title="Edit Data" class="btn btn-sm btn-icon btn-primary"><i class="icon-database-edit2"></i></a> 
                        <a href="javascript:del(\''.$db->rgo_id.'\', \''.$db->no_ruang.'\')" title="Delete Data" class="btn btn-sm btn-icon btn-danger"><i class="icon-trash"></i></a>';
            })
            ->rawColumns(['action','company'])->toJson();
    }

    public function insert(Request $request)
    {
        $inp = $request->post('inp');

        if($inp) {
            $dbs = new CatProject();

            $dbs->cp_id = CatProject::getNextSequenceId();

            foreach($inp as $key => $row){
                if(is_array($inp[$key])) {
                    $inp[$key] = implode('; ',$inp[$key]); //untuk checkbox
                }
                else {
                    $dbs->$key = $inp[$key];
                }
            }

            if($dbs->save())
                return json_encode(array('status' => 'ok;', 'text' => ''));
            else
                return json_encode(array('status' => 'error;', 'text' => 'Gagal Menyimpan Data' ));
        }
        else return json_encode(array('status' => 'error;', 'text' => 'Gagal Menyimpan Data' ));
    }

    public function update(Request $request)
    {
        try {
            $dbs = CatProject::find($request->id);

            $inp = $request->post('inp');
            if($inp){
                foreach($inp as $key => $row){
                    if(is_array($inp[$key])) {
                        $dbs->$key = implode('; ',$inp[$key]); //untuk checkbox
                    } else {
                        $dbs->$key = $inp[$key];
                    }
                }
            }

            $dbs->save();

            return json_encode(array('status' => 'ok;', 'text' => ''));

        } catch (\Illuminate\Database\QueryException $e) {
            return json_encode(array('status' => 'error;', 'text' => 'Gagal Update Data' ));
        }
    }

    public function edit(Request $request)
    {
        return CatProject::find($request->id)->toJson();
    }

    public function delete(Request $request)
    {
        $deleted = CatProject::destroy($request->id);

        if($deleted)
            return json_encode(array('status' => 'ok;', 'text' => ''));
        else
            return json_encode(array('status' => 'error;', 'text' => 'Gagal Delete Data' ));
    }
}
