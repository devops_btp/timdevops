<?php

namespace App\Http\Controllers\Data;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;


use Illuminate\Support\Facades\DB;

use App\Models\DocumentLegal;


class DocumentLegalController extends Controller
{
    public function index()
    {
        $types = DocumentLegal::select('rdl_type')->groupBy('rdl_type')->orderBy('rdl_type', 'asc')->get();
        $data['type'][''] = 'Semua Jenis Dokumen';
        foreach($types as $type)
            $data['type'][$type->rdl_type] = $type->rdl_type;

        $data['year'][''] = 'Semua Tahun';
        for($y=2015; $y<= date('Y')+1; $y++)
            $data['year'][$y] = $y;

        return view('data.documentLegal.index', $data);
    }

    public function dt(Request $request)
    {
        $type = $request->get('type');
        $year = $request->get('year');
        $db = DocumentLegal::select('*')
            ->when($type, function($query) use ($type) {
                if(!empty($type)) {
                    return $query->where('rdl_type', $type);
                }
            })
            ->when($year, function($query) use ($year) {
                if(!empty($year)) {
                    return $query->where('rdl_year', $year);
                }
            })
            ->orderBy('rdl_year', 'desc');

        return datatables($db)
            ->editColumn('rdl_desc', function($db) {
                if(!empty($db->rdl_file))
                    $doc = url('/storage/docs_legal/'.$db->rdl_file);
                else
                    $doc = '#';
                return '<div class="d-flex align-items-center">
                            <div class="mr-3">
                                <a href="'.$doc.'" class="btn bg-primary rounded-round btn-icon btn-sm text-bold">
                                    <i class="icon-file-pdf text-bold"></i>
                                </a>
                            </div>
                            <div>
                                <a href="'.$doc.'" class="text-default font-weight-semibold">'.$db->rdl_desc.'</a>
                                <div class="text-muted font-size-sm">
                                    <span class="badge badge-mark border-blue mr-1"></span> '.$db->rdl_no.'
                                </div>
                            </div>
                        </div>';
            })
            ->editColumn('rdl_mitra', function($db) {
                return '<div>'.$db->rdl_mitra.'</div>
                        <span class="text-muted">'.$db->rdl_mitra_status.'</span>';
            })
            ->rawColumns(['rdl_desc','rdl_mitra'])->toJson();
    }
}
