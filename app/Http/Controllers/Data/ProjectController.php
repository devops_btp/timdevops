<?php

namespace App\Http\Controllers\Data;

use App\Models\Projects;
use App\Models\ReportProject;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;

use App\Models\DataMark;

class ProjectController extends Controller
{
    public function index()
    {
        /*$chartmarketing['cm'] = DB::select("select
        (select count(rp_project) from mybtp.report_project where rp_date_start between '2019-01-01' and '2019-03-30') as TW1,
        (select count(rp_project) from mybtp.report_project where rp_date_start between '2019-04-01' and '2019-06-30') as TW2,
        (select count(rp_project) from mybtp.report_project where rp_date_start between '2019-07-01' and '2019-09-30') as TW3,
        (select count(rp_project) from mybtp.report_project where rp_date_start between '2019-10-01' and '2019-12-30') as TW4,
        (select count(rp_project) from mybtp.report_project where rp_date_start between '2020-01-01' and '2020-03-30') as TW11,
        (select count(rp_project) from mybtp.report_project where rp_date_start between '2020-04-01' and '2020-06-30') as TW21,
        (select count(rp_project) from mybtp.report_project where rp_date_start between '2020-07-01' and '2020-09-30') as TW31,
        (select count(rp_project) from mybtp.report_project where rp_date_start between '2020-10-01' and '2020-12-30') as TW41")[0];*/

        $q = ReportProject::select(DB::raw('extract(quarter from rp_date_start) as q, extract(year from rp_date_start) as th, sum(rp_price) as p, count(rp_id) as c'))
            ->groupBy(DB::raw('1,2'))
            ->get();

        $count = [];
        $price = [];
        foreach ($q as $qq)
        {
            $count[$qq->th][$qq->q] = $qq->c;
            $price[$qq->th][$qq->q] = (int) $qq->p;
        }

        $json = [];
        foreach($count as $th => $quarter)
        {
            $json[] = [
                'name' => $th,
                'data' => array_values($quarter)
            ];
        }
        $chartmarketing['chart_count'] = json_encode($json);

        $json = [];
        foreach($price as $th => $quarter)
        {
            $json[] = [
                'name' => $th,
                'data' => array_values($quarter)
            ];
        }
        $chartmarketing['chart_price'] = json_encode($json);

        //dd($chartmarketing['chart_count']);

        return view('data.project.index', $chartmarketing);
    }

    public function dt()
{
    # code...
    return datatables(DataMark::query())
            ->addColumn('action', function ($db) {
                    return '
                        <a href="javascript:del(\''.$db->rp_id.'\', \''.$db->rp_year.'\', \''.$db->rp_number.'\',\''.$db->rp_client.'\', \''.$db->rp_contract.'\', \''.$db->rp_project.'\', \''.$db->rp_date_start.'\', \''.$db->rp_date_end.'\', \''.$db->rp_price.'\', \''.$db->rp_ppn.'\', \''.$db->rp_price_total.'\')"';
            })
            ->rawColumns(['action'])->toJson();
}

    // public function chart()
    // {
    //     $chartmarketing = DB::select("select (select count(rp_project) from mybtp.report_project where rp_date_start between '2020-01-01' and '2020-03-01') as TW1,
    //     (select count(rp_project) from mybtp.report_project where rp_date_start between '2020-04-01' and '2020-07-01') as TW2,
    //     (select count(rp_project) from mybtp.report_project where rp_date_start between '2020-08-01' and '2020-11-01') as TW3");
    //     return $chartmarketing; 
        
    // }

}
