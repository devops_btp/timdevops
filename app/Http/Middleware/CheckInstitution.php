<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class CheckInstitution
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::get('switcher_institution')) {
            return $next($request);
        } else {
            if($request->has('institution')) {
                Session::put('switcher_institution', $request->input('institution'));
                return $next($request);
            } else {
                return redirect('/');
                //return $next($request);
            }
        } /*else {
            if($request->has('institution')) {
                Session::put('switcher_institution', $request->input('institution'));
                return $next($request);
            } else {
                return redirect('/');
            }
        }*/

        //return $next($request);
    }
}
