<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    protected $table = 'mybtp.client';
    protected $primaryKey = 'id_client';
    // protected $dates = ['deleted_at'];
    // public $sequence = 'iclia.ms_spk_id_seq';

    //protected $fillable = [];

    //protected static $logName = 'iclia.iclia_ms_spk';
    //protected static $logAttributes = ['*'];
    public function project(){
    	return $this->belongsTo('App\Models\Projects');
    }
}
