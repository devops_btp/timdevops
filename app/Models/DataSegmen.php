<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataSegmen extends Model
{
    //
    protected $table = 'mybtp.report_segment_pasar';
    protected $primaryKey = 'rsp_id';
}
