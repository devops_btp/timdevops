<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

class Startup extends Model
{
    //
    protected $table = 'mybtp.ic_data_startup';
    protected $primaryKey = 'ic_id';
    protected $fillable = [
        'ic_logo' , 'ic_nama_startup' , 'ic_alamat' , 'ic_visi' , 'ic_misi' , 'ic_data_medsos' ,
        'ic_website' , 'ic_jml_pegawai' , 'ic_file_so'
    ];

    public static function getNextSequenceId()
    {
        $next_id = DB::select("select nextval('mybtp.ic_id_seq')");
        return intval($next_id['0']->nextval);
    }
}
