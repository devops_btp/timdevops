<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
//use Spatie\Activitylog\Traits\LogsActivity;
use DB;

class ProblemPasar extends Model
{
    use SoftDeletes;

    protected $table = 'mybtp.em_problempasar';
    protected $primaryKey = 'pp_id';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'em_title', 'em_cat', 'em_desc', 'em_req', 'em_date'
    ];

    public static function getNextSequenceId()
    {
        $next_id = DB::select("select nextval('mybtp.pp_id_seq')");
        return intval($next_id['0']->nextval);
    }

    public function catproject()
    {
        return $this->hasOne('App\Models\CatProject');
    }
}
