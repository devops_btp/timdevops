<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Spatie\Activitylog\Traits\LogsActivity;

use DB;

class Inovasi extends Model
{

    protected $table = 'mybtp.report_inovasi';
    protected $primaryKey = 'ri_id';
    /*protected $fillable = [
        'ri_year', 'ri_product', 'ri_team', 'ri desc', 'ri_team_faculty', 'ri_status', 'ri_category', 'ri_founding'
    ];*/

    public static function getNextSequenceId()
    {
        $next_id = DB::select("select nextval('mybtp.ri_id_seq')");
        return intval($next_id['0']->nextval);
    }
}
