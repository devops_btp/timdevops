<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;
//use Spatie\Activitylog\Traits\LogsActivity;
use DB;

class ClientEM extends Model
{
    // use SoftDeletes;
    //use LogsActivity;

    protected $table = 'mybtp.em_client';
    protected $primaryKey = 'cl_id';
    protected $fillable = [
        'em_name', 'em_telp', 'em_address', 'em_email', 'ri_team_faculty', 'em_picname', 'em_picemail', 'em_pictelp'
    ];
    
    public static function getNextSequenceId()
    {
        $next_id = DB::select("select nextval('mybtp.cl_id_seq')");
        return intval($next_id['0']->nextval);
    }
}
//cobaan insert ayeuna
