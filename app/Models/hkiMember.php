<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class hkiMember extends Model
{
    //
    protected $table = 'mybtp.hki_member';
    protected $primaryKey = 'kim_id';

    public function hki()
    {
        return $this->belongsTo('App\Models\Hki','kim_id_ki','ki_id');
    }
    public static function getNextSequenceId()
    {
        $next_id = DB::select("select nextval('mybtp.hki_member_seq')");
        return intval($next_id['0']->nextval);
    }

}
