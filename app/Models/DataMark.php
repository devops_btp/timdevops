<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataMark extends Model
{
    //
    protected $table = 'mybtp.report_project';
    protected $primaryKey = 'rp_id';
}
