<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hki extends Model
{
    //
    protected $table = 'mybtp.hki';
    protected $primaryKey = 'ki_id';

    public function hki_member()
    {
        return $this->hasMany('App\Models\hkiMember','kim_id_ki') ;
    }
  
}
