<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
//use Spatie\Activitylog\Traits\LogsActivity;
use DB;

class Financial extends Model
{
    use SoftDeletes;
    //use LogsActivity;

    protected $table = 'mybtp.p_financial';
    protected $primaryKey = 'pf_id';

    public function project() {
    	return $this->belongsTo('App\Models\Projects','project','project_pr__id');
        
    }
    // protected $dates = ['deleted_at'];
    // public $sequence = 'iclia.ms_spk_id_seq';

    //protected $fillable = [];

    //protected static $logName = 'iclia.iclia_ms_spk';
    //protected static $logAttributes = ['*'];

    public static function getNextSequenceId()
    {
        $next_id = DB::select("select nextval('pmo.pr_id_seq')");
        return intval($next_id['0']->nextval);
    }
}
