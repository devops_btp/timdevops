<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
//use Spatie\Activitylog\Traits\LogsActivity;

use DB;

class DocumentLegal extends Model
{

    protected $table = 'mybtp.report_docs_legal';
    protected $primaryKey = 'rdl_id';
    protected $fillable = [
        'rdl_year', 'rdl_type', 'rdl_mitra', 'rdl_mitra_status','rdl_no','rdl_desc','rdl_file'
    ];

    public static function getNextSequenceId()
    {
        $next_id = DB::select("select nextval('mybtp.rdl_id_seq')");
        return intval($next_id['0']->nextval);
    }
}
