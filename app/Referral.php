<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 
use DB;

class Referral extends Model
{
    use SoftDeletes; 

    //protected $connection = 'masterdata';
    protected $table = 'affiliate.referral';
    protected $primaryKey = 'ref_id';
    protected $dates = ['deleted_at']; 
    public $timestamps = false;
	//protected $fillable = []; 

      
    public static function getNextSequenceId()
    {
        $next_id = DB::select("select nextval('affiliate.ref_id_seq')");
        return intval($next_id['0']->nextval);
    }
}
