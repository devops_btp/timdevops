<?php

return [
    'jns_ki' => [
        'HC APL' => 'Hak Cipta Aplikasi',
        'HC N-APL' => 'Hak Cipta Non-Aplikasi',
        'PB' => 'Paten Biasa',
        'PS' => 'Paten Sederhana',
        'DI' => 'Desain Industri',
        'DTLST' => 'Desain Tata Letak Sirkuit Terpadu',
        'M' => 'Merek',
        'RD' => 'Rahasia Dagang'
    ]
];
